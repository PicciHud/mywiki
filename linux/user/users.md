# Gestione degli utenti

## Aggiungere un utente 

```bash
sudo useradd -s /bin/bash -m -u 1050 -c "Mary Quinn" -G sudo,lpadmin,sambashare maryq
```
Il comando è composto da:

- `-s /bin/bash`: selezione della shell predefinita
- `-m`: l'opzione crea una directory nella directory `/home/`, con lo stesso nome del nuovo utente;
- `-c "Mary Quinn"`: il nome completo del nuovo utente. Facoltativo;
- `-G`: gruppi già esistenti a cui verrà aggiunto l'utente;
- `-u`: permette di specificare l'UID dell'utente;
- maryq : il nome del nuovo utente.

Questo comando crea il nuovo utente, compresa la sua home directory.

### Aggiungere un utente esistente a dei gruppi

```bash
usermod -aG group1,group2 username

usermod -aG adm,cdrom,floppy,sudo,audio,dip,video,plugdev,netdev,bluetooth,lpadmin,scanner davide
```

### Impostare una password

Il nuovo utente non potrà accedere finché non sarà impostata una password. 

```bash
sudo passwd maryq
```

 È consigliabile che venga richiesto di cambiare la password al primo accesso.

 ```bash
sudo passwd --expire maryq
```

### chage

Il comando chage viene utilizzato per il monitoraggio e la modifica della data di scadenza della password di un utente.

```bash
chage -l username

chage -l davide 
Ultimo cambio della password                            : gen 29, 2023
Scadenza della password                                 : mai
Inattività della password                               : mai
Scadenza dell'account                                   : mai
Numero minimo di giorni tra i cambi di password         : 0
Numero massimo di giorni tra i cambi di password        : 99999
Giorni di preavviso prima della scadenza della password : 7
```
Per utilizzare il comando `chage` in modalità interattiva al fine di modificare uno o più campi, digitare:

```bash
sudo chage username
```

#### Bloccare l'account di un utente

```bash
sudo -E 0 username
```
In questo modo l'utente non potrà più accedere al sistema. 

L'opzione `-E` viene utilizzata per impostare una data di scadenza. Quindi, una volta eseguito il comando, l'account verrà bloccato immediatamente.

#### Disabilitare l'account

```bash
sudo usermod -L testuser

# or

sudo passwd -l testuser

# or

sudo usermod -s /sbin/nologin [username]
```

#### Forzare il cambio password

```bash
sudo chage --lastday 0 username
```
#### Impostare una data di scadenza dell'account

```bash
sudo chage -E 2023-01-11 sagar
```

#### Impostare una data di scadenza della password

```bash
sudo chage -d YYYY-MM-GG nome_utente
```

dove YYYY-MM-GG rappresenta la data in cui si vuole far scadere la password e nome_utente rappresenta il nome dell'utente di cui si vuole modificare la password.

## Eliminare un utente

Per prima cosa, uccidere tutti i processi relativi all'utente da eliminare:

```bash
sudo pkill -KILL -u eric
```
Rimuovere eventuali `cronjob`:

```bash
sudo crontab -r -u eric
```
Infine, eliminare l'utente e la sua home col comando (Debian):

```bash
sudo deluser --remove-home eric
```

## Collegamenti

- [https://www.howtogeek.com/806104/add-a-user-to-linux/](https://www.howtogeek.com/806104/add-a-user-to-linux/)
- [https://www.howtogeek.com/656549/how-to-delete-a-user-on-linux-and-remove-every-trace/](https://www.howtogeek.com/656549/how-to-delete-a-user-on-linux-and-remove-every-trace/)
- [https://linuxhandbook.com/chage-command/](https://linuxhandbook.com/chage-command/)
- [https://www.howtogeek.com/50787/add-a-user-to-a-group-or-second-group-on-linux/](https://www.howtogeek.com/50787/add-a-user-to-a-group-or-second-group-on-linux/)
- [https://www.thegeekdiary.com/unix-linux-how-to-lock-or-disable-an-user-account/](https://www.thegeekdiary.com/unix-linux-how-to-lock-or-disable-an-user-account/)

# Differenza tra sudo e su

## su

Il comando `su` (*switch users*) serve praticamente per *diventare* un altro utente ereditandone identificativo, identificativo del gruppo, privilegi, permessi 
d’esecuzione e di navigazione ed in alcuni casi anche variabili d’ambiente, alias, percorsi predefiniti e quant’altro.
Richiede la password dell'utente di destinazione.

La sua sintassi di base è:

```bash
su <parametro_opzionale> nomeutente
```
Se invocato senza parametri il nomeutente viene impostato automaticamente a *root*.

### sudo -

Il comando `su -` è simile al comando su, ma cambia anche l'ambiente dell'utente corrente in quello dell'utente root. Ciò significa che le variabili 
d'ambiente dell'utente corrente vengono sostituite con quelle dell'utente root.

## sudo

Il comando `sudo` prende come input un comando – non un utente – e permette di eseguire il comando passato in input con i privilegi definiti nel file `/etc/sudoers`.

In sostanza il comando sudo permette di fare ciò che farebbe un utente *privilegiato* senza il bisogno di possedere la sua password.

```bash
sudo <parametro_opzionale> comando_da_eseguire
```
Mentre con il comando su si cambia utente fino al termine della sessione del terminale, sudo assegna i privilegi dell'utente target al solo processo che viene con esso avviato. 

L'utente `target` non deve essere necessariamente l'amministratore, ma può essere un qualsiasi utente del sistema. Per scegliere l'utente target, usare l'opzione -u:

```bash
sudo -u target comando
```

Una volta digitato il comando, il sistema chiederà la password dell'utente attuale e non la password dell'utente target (a meno che non si configuri sudo in modo diverso). 
La password viene chiesta la prima volta e memorizzata per un certo lasso di tempo, quindi è possibile usare il comando sudo più volte consecutive senza dover inserire ogni volta la password. 

### Parametri alla riga di comando

sudo può essere lanciato con una serie di parametri che ne modificano il funzionamento temporaneamente. Quelli che seguono sono una parte delle opzioni disponibili:

| Opzione | Risultato                                                                                                                                                                                                                                                 |
|---------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| -H      | imposta la variabile di sistema *\$HOME* con la cartella Home dell'utente target, cioè quello che eseguirà il comando, normalmente root; in maniera predefinita sudo lascia inalterata la *\$HOME*                                                        |
| -b      | esegue il comando in background                                                                                                                                                                                                                           |
| -k      | serve a eliminare il salvataggio della password per l'utente; al successivo utilizzo di sudo verrà nuovamente richiesta la password                                                                                                                       |
| -l      | visualizza i comandi che si è autorizzati a usare o non usare                                                                                                                                                                                             |
| -i      | consente di entrare nella riga di comando come utente target simulando l'accesso per il nuovo utente e impostando le variabili d'ambiente; in pratica consente all'utente di diventare root e quindi non dover utilizzare sudo ad ogni successivo comando |
| -s      | consente di entrare nella riga di comando come utente target; questa opzione è quasi identica a -i, ma non imposta le variabili d'ambiente dell'utente target                                                                                             |

## Collegamenti

- [https://qastack.it/ubuntu/70534/what-are-the-differences-between-su-sudo-s-sudo-i-sudo-su](https://qastack.it/ubuntu/70534/what-are-the-differences-between-su-sudo-s-sudo-i-sudo-su)
- [https://www.chimerarevo.com/guide/differenza-sudo-su-linux-145298/](https://www.chimerarevo.com/guide/differenza-sudo-su-linux-145298/)
- [https://wiki.ubuntu-it.org/AmministrazioneSistema/PrivilegiDiAmministrazione/Sudo](https://wiki.ubuntu-it.org/AmministrazioneSistema/PrivilegiDiAmministrazione/Sudo)

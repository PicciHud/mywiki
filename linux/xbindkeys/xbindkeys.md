# xbindkeys

## Rimappare tastiera e mouse

Installare i seguenti pacchetti:

```bash
sudo apt install xbindkeys xvkbd
```

Quindi creare il seguente file nella propria home:

```bash
cd
touch .xbindkeysrc
```

### Rimappare i pulsanti del mouse

Dare il seguente comando:

```bash
xev -event button
```
Quindi, all'interno del riquadro della finestra che si apre, cliccare i pulsanti del mouse. Vengono sempre intercettati due eventi, la pressione e il rilascio del pulsante.

Nel esempio sottostante, il **pulsante 1** corrisponde al click sinistro del mouse

```md
xev -event button

Outer window is 0x7400001, inner window is 0x7400002

ButtonPress event, serial 25, synthetic NO, window 0x7400001,
    root 0x6be, subw 0x7400002, time 24648844, (40,43), root:(1006,608),
    state 0x0, button 1, same_screen YES

ButtonRelease event, serial 25, synthetic NO, window 0x7400001,
    root 0x6be, subw 0x7400002, time 24648916, (40,43), root:(1006,608),
    state 0x100, button 1, same_screen YES
```

In questo modo è possibile assegnare dei comandi ai vari bottoni e pulsanti del mouse.

Per il Logitech MX Ergo questa è la mappatura:

| Numero | Pulsante                             |
|--------|--------------------------------------|
| 1      | click sinistro                       |
| 2      | pulsante centrale (click su rotella) |
| 3      | click destro                         |
| 4      | rotella su                           |
| 5      | rotella giù                          |
| 6      | rotella a sinistra                   |
| 7      | rotella a destra                     |
| 8      | pulsante indietro                    |
| 9      | pulsante avanti                      |

[Qui](https://www.alternativalinux.it/assegnare-bottoni-mouse-in-linux-mint/) maggiori informazioni su come rimappare i pulsanti del mouse nel file `.xbindkeys`.

Ecco alcuni semplici esempi:

```bash
# Lanciare firefox con il bottone 9
"firefox"
m:0x0 + b:9

# Scrivere formula di chiusura con B9
"xvkbd -text "Cordiali saluti\rDavide.""
m:0x0 + b:9
```

Dopo aver rimappato i pulsanti nel file `.xbindkeys`, uccidere e riavviare il processo:

```bash
killall xbindkeys

xbindkeys
```

### Rimappare la tastiera

Per visualizzare come specificare la combinazione di tasti scelta per avviare un comando si può utilizzare:

```bash
xbindkeys -k
```
Digitando una combinazione di tasti, ad esempio `control+shift+t`, appariranno a terminale uno o due modi per indicare correttamente la combinazione di tasti all’interno del file `.xbindkeysrc`.
 
```bash
xbindkeys -k 

Press combination of keys or/and click under the window.
You can use one of the two lines after "NoCommand"
in $HOME/.xbindkeysrc to bind a key.
"NoCommand"
    m:0x5 + c:28
    Control+Shift + t
```

Nella prima riga *NoCommand* andrà precisato il comando da avviare.
La seconda riga e anche la terza, sicuramente più intellegibile, presentano la combinazione usata. 

## Esempio di configurazione

```bash
cat .xbindkeysrc 

# print pipe
"xvkbd -xsendevent -text "|""
    m:0x5 + c:30
#   Control+Shift + u

# stamp
"flameshot gui"
    m:0x5 + c:33
    Control+Shift + p

#\r per Invio
#\t per tasto TAB
#\b per backspace cioè l’eliminazione dell’ultimo carattere digitato
#\e per Esc
#\d per Canc o Del
#\S per Maiuscolo o Shift
#\C per CTRL
#\A per Alt
#\M per il tasto Meta
#\W per il tasto Windows
#\[F1] per simulare un tasto funzione
#\[End] per simulare il tasto fine
#\[Prior] simula il tasto Pag Su
#\[Next] simula il tasto Pag Giù
#\[Print] per il tasto stamp o PrintScreen (esegue uno screenshot)
```

## Collegamenti

- [https://www.alternativalinux.it/assegnare-bottoni-mouse-in-linux-mint/](https://www.alternativalinux.it/assegnare-bottoni-mouse-in-linux-mint/)
- [https://medium.com/@Aenon/bind-mouse-buttons-to-keys-or-scripts-under-linux-with-xbindkeys-and-xvkbd-7e6e6fcf4cba](https://medium.com/@Aenon/bind-mouse-buttons-to-keys-or-scripts-under-linux-with-xbindkeys-and-xvkbd-7e6e6fcf4cba)
- [https://mauriziosiagri.wordpress.com/tag/xbindkeys-config/](https://mauriziosiagri.wordpress.com/tag/xbindkeys-config/)

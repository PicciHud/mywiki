# Script Bash

Qui un elenco di semplici funzioni o script in Bash.

## Rinominare immagini

Ecco come rinominare una lunga serie di file in maniera progressiva, nel formato `imgXXX`:

```bash
j=1 ; for i in * ; do mv "$i" ${i//*./img$(printf "%03d\n" $j).} ; let j=j+1 ; done
```

Per ogni parametro, questo viene rinominato nel formato `imgXXX`, mantenendo inalterata l'estensione del file.

In pratica questo comando permette di rinominare tutti i file di una directory in un formato specifico "img001, img002, img003, ecc."

## Crontab

```bash
#.---------------- [m]inute: minuto (0 - 59)
#|  .------------- [h]our: ora (0 - 23)
#|  |  .---------- [d]ay [o]f [m]onth: giorno del mese (1 - 31)
#|  |  |  .------- [mon]th: mese (1 - 12) OPPURE jan,feb,mar,apr...
#|  |  |  |  .---- [d]ay [o]f [w]eek: giorno della settimana (0 - 7) (la domenica vale 0 o 7)  OPPURE sun,mon,tue,wed,thu,fri,sat
#|  |  |  |  |
#
#*  *  *  *  *  comando da eseguire

0 21,22 * * * pg_dump cinema > ~/Databases/filmdb.sql
0 9,15,20 * * * nice -n 19 clamscan -ir /$HOME --log=clamavlog.txt --move=/$HOME/quarantine/ 2>&1
0 9,15,20,22 * * * /usr/bin/rsync -az --delete --exclude=Nextcloud ~/ /media/davide/Backup/Desktop/ && date >> ~/backup.log
```

## SQL to Markdown

```bash
awk -F "\t" '{print $1"\t"$2"\t"$4"\t"$3}' file.sql | sort --field-separator='|' -s -k 4 >> file.md
```

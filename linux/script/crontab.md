## Crontab

Nei sistemi operativi GNU-Linux, il comando `crontab` consente di pianificare l'esecuzione automatica e periodica di attività o script.

Ogni attività è chiamata `cron job`.

## Sintassi

Ogni riga presenta una sequenza di campi, divisi da uno spazio, come in questo esempio

```bash
0 23 * * * /root/mybackup.sh
```

Questo il significato: al minuto 0 delle ore 23, tutti i giorni del mese, tutti i mesi, tutti i giorni della settimana, esegui lo script "/root/mybackup.sh" .
In poche parole, esegue lo script tutti i giorni alle ore 23.00. 

Ecco il dettaglio tutti i possibili valori di questi campi, in ordine di inserimento:

|Campo|Valore|
|---|---|
|Minuti|0 - 59|
|Ore|0 - 23|
|Giorno del mese|1 - 31|
|Mese|1 - 12|
|Giorno della settimana|0 - 7|

```txt
.---------------- [m]inute: minuto (0 - 59) 
|  .------------- [h]our: ora (0 - 23)
|  |  .---------- [d]ay [o]f [m]onth: giorno del mese (1 - 31)
|  |  |  .------- [mon]th: mese (1 - 12) OPPURE jan,feb,mar,apr... 
|  |  |  |  .---- [d]ay [o]f [w]eek: giorno della settimana (0 - 7) (la domenica vale 0 o 7)  OPPURE sun,mon,tue,wed,thu,fri,sat 
|  |  |  |  |

*  *  *  *  *  comando da eseguire
```

- `*` l'operatore asterisco indica qualsiasi valore. Se il simbolo asterisco è presente nel campo Hour, significa che l'attività verrà eseguita ogni ora;
- `,` l'operatore virgola consente di specificare un elenco di valori. Ad esempio, se si dispone 1,3 nel campo Hour, l'attività verrà eseguita all'1:00 e alle 03:00;
- `-` l'operatore trattino consente di specificare un intervallo di valori. Se è presente 1-5 nel campo Day of a week, l'attività verrà eseguita tutti i giorni feriali (dal lunedì al venerdì).
- `/` l'operatore backslash consente di specificare valori che verranno ripetuti in un determinato intervallo. Ad esempio, se è presente `*/4` nel campo Hour, significa che l'azione verrà eseguita ogni quattro ore. È lo stesso che specificare 0,4,8,12,16,20. 

## Impostare un'attività

Per aggiungere/modificare/eliminare le attività, dare il seguente comando:

```bash
crontab -e
```

Digitare quindi il carattere `i` (insert) per editare il file di crontab.

Per visualizzare le attività nel file crontab: 

```bash
crontab -l
```
Per rimuovere il file crontab corrente:

```bash
crontab -r 
```

## Variabili d'ambiente

Le variabili ambientali utilizzano la seguente forma:

`NOME = valore`

Per motivi di leggibilità, dovrebbero essere inserite in testa al file generato dal comando crontab.

Le variabili a cui è possibile assegnare un valore sono:

- `SHELL`: se non assegnata viene impostata automaticamente da Cron a /bin/sh. Altrimenti impostare la variabile ambientale SHELL in maniera conforme all'interprete utilizzato, ad esempio: `SHELL=/bin/bash`

Questa modifica potrebbe essere necessaria nel caso in cui si voglia eseguire attraverso Cron uno script Bash.

- `MAILTO`: se questa variabile è impostata, il demone Cron invierà un'email, con l'output del/dei comando/comandi specificati in crontab, all'utente specificato. È possibile specificare più utenti separandoli con una virgola. Se la variabile è impostata a `""`, allora non verranno inviate email.

Ovviamente, affinché Cron possa inviare con successo le email, bisogna avere un MTA installato e funzionante sulla propria macchina.

## Alcuni esempi pratici

```bash
*/20 * * * * /root/mybackup.sh
#Lo script viene eseguito ogni 20 minuti 

0 23 * * * /root/mybackup.sh
#Lo script viene eseguito alle ore 23.00 tutti i giorni

0 9,23 * * * /root/mybackup.sh
#Lo script viene eseguito alle ore 09.00 e alle ore 23.00 tutti i giorni

0 7 * * 0 /root/mybackup.sh
#Lo script viene eseguito alle ore 07.00 della domenica

0 7 * 5 6 /root/mybackup.sh
#Lo script viene eseguito alle ore 07.00 ogni sabato del mese di maggio

15 7 * 5 6 /root/mybackup.sh
#Lo script viene eseguito alle ore 07.15, ogni sabato di maggio

* * * * * /root/mybackup.sh
#Lo script viene eseguito ogni minuto

*/5 * * * * /root/mybackup.sh
#Lo script viene eseguito ogni 5 minuti

0 17 * * 1,5 /root/mybackup.sh
#Lo script viene eseguito alle ore 17.00 di lunedì e domenica

0 */5 * * * /root/mybackup.sh
#Lo script viene eseguito ogni 5 ore (05.00, 10.00, ...)

* * * * * sleep 30; /root/mybackup.sh
#Lo script viene eseguito ogni 30 secondi
```

**Alias**

```bash
0 * * * * /root/mybackup.sh
#oppure
@hourly /root/mybackup.sh
#Lo script viene eseguito ogni ora

0 0 * * * /root/mybackup.sh
#oppure
@daily /root/mybackup.sh
#Lo script viene eseguito ogni giorno alle 00.00

0 0 * 0 0 /root/mybackup.sh
#oppure
@weekly /root/mybackup.sh
#Lo script viene eseguito ogni settimana (alle 00.00 di ogni domenica)

0 0 1 * * /root/mybackup.sh
#oppure
@monthly /root/mybackup.sh
#Lo script viene eseguito il primo di ogni mese (alle 00.00)
```

**Lanciare più script**

È possibile lanciare più script contemporaneamente, separandoli con il ";"

```bash
0 0 * * * /root/mybackup.sh; /home/myscript.sh
#I due script vengono eseguiti ogni giorno alle ore 00.00
```

**Lanciare uno script all'avvio del sistema**

```bash
@reboot /root/backup.sh
```

**Verificare l'esecuzione**

Per verificare se i crontab sono stati eseguiti, leggere i log di cron:

```bash
$ tail /var/log/cron
Nov 30 23:30:00 mioserver crond[18340]: (root) CMD (/bin/sh /root/mybackup.sh &)
Dec  1 00:03:00 mioserver crond[20399]: (pippo) CMD (/bin/sh /root/myscript.sh &)
```

Il log contiene le seguenti informazioni:

- *Timestamp*: la data di esecuzione del cron job;

- *Hostname*: il nome host del server su cui è eseguito il cron;

- Il *demone cron* e, tra parensi quadre, il *PID* del processo;

- *Username*: il nome utente che esegue il cron job, tra parentesi tonde;

- *CMD*: il comando/script da eseguire.

## Backup e ripristino

### Esportare un file crontab

Se si volesse salvare il proprio crontab per esportarlo su un'altra macchina o semplicemente per farne una copia di backup, la procedura è semplicissima:

```bash
crontab -l > mycrontab 
```

in questo modo il file crontab verrà salvato nel file mycrontab.

### Importare un file crontab

Questa procedura è utile per importare un file in cui si è precedentemente salvato il contenuto di un file crontab oppure per importare un file editato a mano attraverso un qualsiasi altro editor, testuale o grafico:

```bash
crontab mycrontab
```

**Attenzione**: il file crontab, se presente, verrà sovrascritto! 

## Collegamenti

- [https://noviello.it/come-pianificare-scheduling-cron-jobs-con-crontab-su-linux/](https://noviello.it/come-pianificare-scheduling-cron-jobs-con-crontab-su-linux/)

- [https://howto.webarea.it/linux/utilizzo-di-crontab-per-schedulare-processi-con-esempi-sotto-linux_1](https://howto.webarea.it/linux/utilizzo-di-crontab-per-schedulare-processi-con-esempi-sotto-linux_1)

- [https://guide.debianizzati.org/index.php/Utilizzo_del_servizio_di_scheduling_Cron](https://guide.debianizzati.org/index.php/Utilizzo_del_servizio_di_scheduling_Cron)

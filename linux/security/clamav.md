# ClamAV e scansioni automatiche

ClamAV è un antivirus multipiattaforma.

## Installazione

```bash
sudo apt install clamav
```

## Utilizzo

### Aggiornare il database dei virus

Per aggiornare il database dei virus digitare il seguente comando in un terminale:

```bash
sudo freshclam
```

Nel caso si riscontrasse il seguente errore:

```bash
ERROR: /var/log/clamav/freshclam.log is locked by another process
ERROR: Problem with internal logger (UpdateLogFile = /var/log/clamav/freshclam.log).
ERROR: initialize: libfreshclam init failed.
ERROR: Initialization error!
```
bisogna uccidere il processo che utilizza il file freshclam.org

```bash
sudo lsof /var/log/clamav/freshclam.log
lsof: WARNING: can't stat() fuse.portal file system /run/user/19201007/doc
Output information may be incomplete.
COMMAND   PID    USER   FD  TYPE DEVICE SIZE/OFF NODE NAME
freshclam 217908 clamav 3wW REG 253,0 5565 1444631 /var/log/clamav/freshclam.log
```

Quindi uccidere il processo col comando:

```bash
sudo kill -9 ${PID}
```

Ora è possibile aggiornare il database:

```bash
sudo freshclam
Sat Dec 31 16:14:18 2022 -> ClamAV update process started at Sat Dec 31 16:14:18 2022
Sat Dec 31 16:14:18 2022 -> daily.cld database is up-to-date (version: 26767, sigs: 2014726, f-level: 90, builder: raynman)
Sat Dec 31 16:14:18 2022 -> main.cvd database is up-to-date (version: 62, sigs: 6647427, f-level: 90, builder: sigmgr)
Sat Dec 31 16:14:18 2022 -> bytecode.cvd database is up-to-date (version: 333, sigs: 92, f-level: 63, builder: awillia2)
```

Altre possibili soluzioni:

```bash
sudo /etc/init.d/clamav-freshclam stop
sudo freshclam
sudo /etc/init.d/clamav-freshclam start

sudo service clamav-freshclam stop
sudo freshclam

sudo service clamav-freshclam start
sudo systemctl stop clamav-freshclam.service
```

In ogni caso, riavviare il servizio:

```bash
sudo systemctl start clamav-freshclam.service
```

### Aggiornamento automatico

Per impostare l'aggiornamento automatico ogni giorno del database delle definizioni si può usare il comando:

```bash
sudo freshclam -d -c 1
```

## Scansione da Terminale

La sintassi predefinita per utilizzare la scansione di file e cartelle è la seguente:

```bash
clamscan <opzione> /cartella/da/scansionare
```

Pertanto per scansionare l'intera home dell'utente (tutti i file, cartelle e sotto cartelle presenti) digitare in un terminale:

```bash
clamscan -r /home
```

Questo l'output finale:

### Alcune opzioni utili:

- `-i`: Mostra nel terminale solo i file risultati positivi alla scansione;
- `--log=clamavlog.txt`: Crea il file clamavlog.txt nella propria home contenente il resoconto della scansione. I file risultati positivi alla scansione saranno indicati con la scritta FOUND;
- `--exclude-dir='/percorso/da/escludere'`: Esclude una cartella dalla scansione;
- `-r`: Scansione ricorsiva;
- `--move=/patch/to/folder`: Sposta/Copia tutti i file infetti della cartella scansionata in una directory dedicata per poterli controllare successivamente.

Ecco un esempio di comando:

```bash
clamscan -ir /$HOME --log=clamavlog.txt --move=$HOME/quarantine/
```

Creare la directory `~/quarantine`.

## Creazione cronjob

```bash
crontab -e
```

ed inserire il seguenti job:

```bash
0 9,13,18 * * * nice -n 19 clamscan -ir /$HOME --log=clamavlog.txt --move=/$HOME/quarantine/ 2>&1
```

Ogni giorno, alle ore 9, 13 e alle ore 18, viene eseguita la scansione.

`nice`: il comando nice serve per eseguire un processo con una priorità differente da quella standard. La priorità può essere impostata con valori fra -20 e 19, dove -20 è la priorità più alta e 19 la più bassa. Il valore di nice è un “suggerimento” al sistema sulla priorità del processo, che il sistema può prendere in considerazione o ignorare.

### Verificare l'esecuzione

Per verificare se i crontab sono stati eseguiti, leggere i log di cron:

```bash
tail /var/log/cron
Nov 30 23:30:00 mioserver crond[18340]: (root) CMD (/bin/sh /root/mybackup.sh &)
Dec 1 00:03:00 mioserver crond[20399]: (pippo) CMD (/bin/sh /root/myscript.sh &)
```

Il log contiene le seguenti informazioni:

- `Timestam`: la data di esecuzione del cron job;
- `Hostname`: il nome host del server su cui è eseguito il cron;
- Il demone cron e, tra parensi quadre, il PID del processo;
- `Username`: il nome utente che esegue il cron job, tra parentesi tonde;
- `CMD`: il comando/script da eseguire.

Oppure guardando il file di log nella home directory:

```bash
cat clamavlog.txt
-------------------------------------------------------------------------------
----------- SCAN SUMMARY -----------
Known viruses: 8646692
Engine version: 0.103.7
Scanned directories: 3043
Scanned files: 50608
Infected files: 0
Total errors: 4
Data scanned: 4794.27 MB
Data read: 3542.08 MB (ratio 1.35:1)
Time: 524.361 sec (8 m 44 s)
Start Date: 2022:12:31 16:34:03
End Date: 2022:12:31 16:42:48
-------------------------------------------------------------------------------
----------- SCAN SUMMARY -----------
Known viruses: 8646692
Engine version: 0.103.7
Scanned directories: 3070
Scanned files: 50963
Infected files: 0
Data scanned: 4831.48 MB
Data read: 3560.17 MB (ratio 1.36:1)
Time: 510.803 sec (8 m 30 s)
Start Date: 2022:12:31 17:36:01
End Date: 2022:12:31 17:44:32
```

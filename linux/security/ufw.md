# ufw

*Uncomplicated Firewall* (ufw) è un firewall ideato per gestire la configurazione appunto del firewall in maniera semplificata, attraverso un'interfaccia a riga di comando.

Sviluppato per semplificare la configurazione di iptables, ufw offre un modo semplice per creare un firewall basato su protocolli IPv4 e IPv6. È inizialmente disabilitato. 

## Installare ufw

```bash
sudo apt-get install ufw
```

## Abilitare ufw

Per abilitare ufw digitare il comando:

```bash
sudo ufw enable
```

Per disabilitare ufw digitare il comando:

```bash
sudo ufw disable
```

## Stato

Per visualizzare lo stato del firewall, digitare il seguente comando in una finestra di terminale:

```bash
sudo ufw status
```

Se abilitato, un possibile output sarà il seguente:

```bash
Firewall attivato

 To                         Action  From
 --                         ------  ----
 22:tcp                     DENY    192.168.0.1
 22:udp                     DENY    192.168.0.1
 22:tcp                     DENY    192.168.0.7
 22:udp                     DENY    192.168.0.7
 22:tcp                     ALLOW   192.168.0.0/24
 22:udp                     ALLOW   192.168.0.0/24
```
Il comando mostra la lista completa delle eventuali regole firewall già impostate.

### Lista delle regole firewall configurabili automaticamente 

Per conoscere la lista delle applicazioni installate sulla macchina Linux e riconosciute da ufw, è sufficiente digitare:

```bash
sudo ufw app list
```

## Impostare le regole di default

In generale, la sintassi è la seguente:

```bash
sudo ufw allow/deny porta/protocollo (facoltativo)
```

Il comando sottostante può essere utilizzato per impostare la risposta predefinita alle connessioni in entrata e in uscita. 

Per negare tutto il traffico in entrata e consentire tutte le connessioni in uscita, eseguire:

```bash
sudo ufw default allow outgoing
sudo ufw default deny incoming
```

## Aggiungire regole

Le regole possono essere aggiunte in due modi: specificando il numero della porta o utilizzando il nome del servizio.

Ad esempio, per consentire sia le connessioni in entrata che quelle in uscita sulla porta 22 per SSH, è possibile eseguire:

`sudo ufw allow ssh`

oppure

`sudo ufw allow 22`

Allo stesso modo, per negare il traffico su una determinata porta (in questo esempio, 111) si dovrebbe solo eseguire:

`sudo ufw deny 111`

Per perfezionare ulteriormente le regole, è possibile permettere il traffico dei pacchetti basati su TCP o UDP. Quanto segue consente i pacchetti TCP sulla porta 80:

```bash
sudo ufw allow 80/tcp
sudo ufw allow http/tcp
```

### Regole avanzate

Oltre a consentire o negare basandosi esclusivamente sulla porta, ufw permette di consentire/bloccare il traffico da indirizzi IP specifici, sottoreti e combinazioni di indirizzo IP/subnet/porta.

Per consentire le connessioni da un indirizzo IP:

```bash
sudo ufw allow from 198.51.100.0
```

Per consentire le connessioni da una sottorete specifica:

```bash
sudo ufw allow from 198.51.100.0/24
```

Per rimuovere una regola, aggiungere `delete` prima dell'attuazione della regola. Se non si desidera più consentire il traffico HTTP, è possibile eseguire:

```bash
sudo ufw delete allow 80
```

## Collegamenti

- [https://www.ilsoftware.it/articoli.asp?tag=Firewall-come-configurarlo-con-ufw-su-Ubuntu_21508](https://www.ilsoftware.it/articoli.asp?tag=Firewall-come-configurarlo-con-ufw-su-Ubuntu_21508)
- [https://wiki.ubuntu-it.org/Sicurezza/Ufw](https://wiki.ubuntu-it.org/Sicurezza/Ufw)

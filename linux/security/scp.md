# scp

`scp` (*secure copy*) è un'utilità della riga di comando che consente di copiare file e directory in modo sicuro.

Con scp, è possibile copiare un file o una directory:

- Dal sistema locale a un sistema remoto;
- Da un sistema remoto al sistema locale;
- Tra server remoti diversi

Quando si trasferiscono i dati con scp, sia i file che la password sono crittografati.

## Sintassi

```bash
scp [OPTION] user@SRC_HOST:path/to/file1 user@DEST_HOST:path/to/file2
```
Tra le principali opzioni:

- `-P` specifica la porta ssh dell'host remoto;
- `-p` preserva la modifica dei file e i tempi di accesso;
- `-C` questa opzione forzerà scp a comprimere i dati;
- `-r` questa opzione permetterà a scp di copiare ricorsivamente le directory;
- `-q` copia i file in modalità silenziosa, non mostra i messaggi di avanzamento o di errore

### Copiare un file locale su un sistema remoto 

```bash
scp file.txt remote_username@10.10.0.2:/remote/directory
```

### Copiare un file remoto su un sistema locale

```bash
scp remote_username@10.10.0.2:/remote/file.txt /local/directory
```
### Copiare un file tra due sistemi remoti

```bash
scp user1@host1.com:/files/file.txt user2@host2.com:/files
```

## Collegamenti

- [https://noviello.it/come-usare-il-comando-scp-per-trasferire-file-in-modo-sicuro-su-linux/](https://noviello.it/come-usare-il-comando-scp-per-trasferire-file-in-modo-sicuro-su-linux/)
- [https://www.freecodecamp.org/italian/news/il-comando-linux-scp-come-trasferire-file-via-ssh-da-remoto-a-locale/](https://www.freecodecamp.org/italian/news/il-comando-linux-scp-come-trasferire-file-via-ssh-da-remoto-a-locale/)
- [https://www.howtogeek.com/804179/scp-command-linux/](https://www.howtogeek.com/804179/scp-command-linux/)

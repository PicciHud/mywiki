# Vim

Vim è un editor di testo molto potente, che può essere ampliato ulteriormente tramite **plugin** vari.

## Plugin Manager

Per poter installare dei plugin è necessario installare prima un plugin-manager.

### Vundle

```bash
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```

Dopo aver clonato Vundle sulla propria macchina, inserire le seguenti righe di testo nel file `.vimrc`

```vimrc
" PLUGINS ---------------------------------------------------------------- {{{

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
"
" All of your Plugins must be added before the following line
" 

call vundle#end()            " required

" }}}
```

## Installazione plugin

Installare un plugin è molto semplice, basta aggiungerlo nel file `.vimrc`, come da codice sottostante:

```vim
" PLUGINS ---------------------------------------------------------------- {{{

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
"
" All of your Plugins must be added before the following line
" 

Plugin 'jasonccox/vim-wayland-clipboard'
Plugin 'gabrielelana/vim-markdown'
Plugin 'itchyny/vim-cursorword'
Plugin 'preservim/nerdtree'
Plugin 'iamcco/markdown-preview.nvim'
Plugin 'iamcco/mathjax-support-for-mkdp'
Plugin 'alvan/vim-closetag'
Plugin 'mattn/emmet-vim'
Plugin 'jiangmiao/auto-pairs'
Plugin 'ryanoasis/vim-devicons'
Plugin 'frazrepo/vim-rainbow'
Plugin 'mg979/vim-visual-multi', {'branch': 'master'}
Plugin 'itchyny/lightline.vim'
Plugin 'voldikss/vim-floaterm'
Plugin 'mracos/mermaid.vim'
Plugin 'mzlogin/vim-markdown-toc'
Plugin 'bbrtj/vim-vorg-md'
Plugin 'sheerun/vim-polyglot'
Plugin 'ervandew/supertab'
Plugin 'Valloric/YouCompleteMe'

call vundle#end()            " required

" }}}zo

```

Una volta salvato il file, digitare `:PluginInstall`.

Quando l'installazione è completata, è buona norma cancellare la cache del buffer digitando il comando `:bdelete`.

È possibile ricercare plugin nel Web oppure tramite il comando `:PluginSearch`. Per aggiornare l'elenco locale dei plugin dal sito vimscripts digitare:
`:PluginSearch!`

### Alcuni plugin utili

- [https://github.com/jasonccox/vim-wayland-clipboard](https://github.com/jasonccox/vim-wayland-clipboard)
- [https://github.com/preservim/nerdtree](https://github.com/preservim/nerdtree)
- [https://github.com/VundleVim/Vundle.vim](https://github.com/VundleVim/Vundle.vim)
- [https://github.com/sheerun/vim-polyglot](https://github.com/sheerun/vim-polyglot)
- [https://github.com/alvan/vim-closetag](https://github.com/alvan/vim-closetag)
- [https://github.com/mattn/emmet-vim](https://github.com/mattn/emmet-vim)
- [https://github.com/ryanoasis/vim-devicons](https://github.com/ryanoasis/vim-devicons)
- [https://github.com/nopecha-ai/nopecha-extension](https://github.com/nopecha-ai/nopecha-extension)
- [https://github.com/thameera/vimv](https://github.com/thameera/vimv)
- [https://github.com/voldikss/vim-floaterm#requirements](https://github.com/voldikss/vim-floaterm#requirements)
- [https://github.com/akrawchyk/awesome-vim](https://github.com/akrawchyk/awesome-vim)
- [https://github.com/neoclide/coc.nvim](https://github.com/neoclide/coc.nvim)
- [https://github.com/fannheyward/coc-marketplace](https://github.com/fannheyward/coc-marketplace)
- [https://github.com/OmniSharp/omnisharp-vim](https://github.com/OmniSharp/omnisharp-vim)
- [https://github.com/ncm2/ncm2](https://github.com/ncm2/ncm2)
- [https://github.com/prashanthellina/follow-markdown-links](https://github.com/prashanthellina/follow-markdown-links)
- [https://github.com/itchyny/calendar.vim](https://github.com/itchyny/calendar.vim)
- [https://github.com/mracos/mermaid.vim](https://github.com/mracos/mermaid.vim)
- [https://github.com/bbrtj/vim-vorg-md](https://github.com/bbrtj/vim-vorg-md)
- [https://github.com/rderik/vim-markdown-toc](https://github.com/rderik/vim-markdown-toc)
- [https://github.com/tpope/vim-commentary](https://github.com/tpope/vim-commentary)
- [https://clay-atlas.com/us/blog/2021/07/04/vim-en-you-complete-me-plugin-auto-code/](https://clay-atlas.com/us/blog/2021/07/04/vim-en-you-complete-me-plugin-auto-code/)
- [https://github.com/ycm-core/YouCompleteMe](https://github.com/ycm-core/YouCompleteMe)
- [https://github.com/primitivorm/vim-predictive](https://github.com/primitivorm/vim-predictive)
- [https://github.com/itchyny/vim-cursorword](https://github.com/itchyny/vim-cursorword)

Nel caso di `neoclide`, un plugin che abilita l'autocompletamento, da installare i seguenti moduli:

```vim
CocInstall coc-cmake coc-css coc-cssmodules coc-docker coc-emmet coc-flutter coc-highlight coc-html coc-htmlhint coc-html-css-support coc-json coc-ltex coc-markdown-preview-enhanced coc-markdownlint coc-omnisharp coc-prettier coc-pytho
n coc-sh coc-sql coc-vimlsp coc-xml coc-yaml coc-markdown-preview-enhanced coc-webview
```

Quindi eseguire:

```bash
cd ~/.vim/bundle/coc.nvim && git checkout release && yarn install && yarn build
```

![vim](./asset/vim/vim.png)


## Comandi utili

| Comando       | Funzione                                                                                                                                       |
|---------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| `gx`          | Apre il link sotto al cursore nel browser                                                                                                      |
| `:m +/-n`     | Sposta la riga sopra o sotto di *n* posizioni                                                                                                  |
| `"+y`         | In Visual Mode, copia il testo selezionato nella clipboard del SO                                                                              |
| `vimv`        | Da terminale, permette di rinominare cartelle e file coi comandi di Vim ([https://github.com/thameera/vimv](https://github.com/thameera/vimv)) |
| `:args`       | Seleziona dei file come argomenti. È possibile utilizzare i caratteri jolly                                                                    |
| `:argdo`      | Permette di eseguire un comando su n files                                                                                                     |
| `gg=G`        | Identazione automatica del codice                                                                                                              |
| `dG`          | Cancella l'intero documento                                                                                                                    |
| `^w`          | In *insert mode*, cancella la parola precedente, dalla posizione del cursore                                                                   |
| `^h`          | In *insert mode*, cancella la parola precedente, dalla posizione del cursore                                                                   |
| `^u`          | In *insert mode*, cancella dalla posizione del cursore a inizio riga                                                                           |
| `^o`          | In *insert mode*, permette di digitare un comando                                                                                              |
| `das`         | Cancella un'intera frase (sentence)                                                                                                            |
| `dap`         | Cancella un intero paragrafo                                                                                                                   |
| `gUU`         | Tutta la riga in maiuscolo                                                                                                                     |
| `guu`         | Tutta la riga in minuscolo                                                                                                                     |
| `(`           | Sposta il cursore all'inizio della frase                                                                                                       |
| `)`           | Sposta il cursore alla fine della frase                                                                                                        |
| `{`           | Sposta il cursore all'inizio del paragrafo                                                                                                     |
| `}`           | Sposta il cursore alla fine del paragrafo                                                                                                      |
| `H`, `M`, `L` | Rispettivamente, sposta il cursore alla prima riga, a metà o all'ultima riga del viewport                                                      |
| `^i`, `^o`    | In *normal mode*, sposta il cursore alla posizione precedente/successiva                                                                       |

Ad esempio:

```vim
:argdo %s/stringToBeReplaced/replacementString/g | update
```
`update` serve per salvare le modifiche automaticamente.

## Collegamenti

- [https://irian.to/blogs/how-to-search-and-replace-string-across-multiple-files-in-vim/](https://irian.to/blogs/how-to-search-and-replace-string-across-multiple-files-in-vim/)
- [https://linuxhint.com/vim-copy-text-system-clipboard/](https://linuxhint.com/vim-copy-text-system-clipboard/)
- [https://vim.fandom.com/wiki/Accessing_the_system_clipboard](https://vim.fandom.com/wiki/Accessing_the_system_clipboard)
- [https://youtu.be/13gNtgqzzmM](https://youtu.be/13gNtgqzzmM)

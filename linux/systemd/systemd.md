# Systemd

## Sistema di init

**init**, in un sistema operativo Unix, è il primo processo che il kernel manda in esecuzione dopo che il computer ha terminato la fase di bootstrap. Esso ha il compito di 
portare il sistema in uno stato operativo, avviando i programmi e servizi necessari.

Dato che init è sempre il primo processo eseguito, esso ha tipicamente il `PID 1`. 

## Systemd

systemd è una suite di demoni, librerie e utilità di amministrazione progettate con lo scopo di centralizzare la gestione e la configurazione dei sistemi operativi Unix-like.
Nacque per Red Hat Linux e fu adottato poi in Debian con lo scopo di rimpiazzare l'init discendente dallo Unix System V 

Le attività di systemd sono organizzate in `unità`. Le unità più comuni sono servizi (**.service**), punti di montaggio (**.mount**), device (**.device**), socket (**.socket**)
o temporizzatori (**.timer**). Per esempio l'avvio del demone SSH viene fatto dall'unità ssh.service.

systemd è preinstallato in molte distribuzioni GNU/Linux. Per verificarne la presenza digitare in un terminale:

```bash
ps --no-headers -o comm 1
```

### Uso base

systemctl è lo strumento principale utilizzato per l'introspezione e il controllo dello stato dei servizi gestiti da systemd. 
Si può, ad esempio, usare systemctl per abilitare o disabilitare servizi in modo permanente o solo per la sessione corrente.

### Ottenere informazioni sullo stato del sistema

- Visualizzare lo stato del sistema:

`systemctl status`

- Elencare le unità che hanno fallito:

`systemctl --failed`

- Elencare i file di unità installati:

`systemctl list-unit-files`

- Elencare tutte le unità:

`systemctl list-units --all`

### Gestire servizi

- Elencare tutti i servizi in esecuzione:

`systemctl`

- Avviare il servizio "<nome_servizio>.service" immediatamente:

`systemctl start <nome_servizio>.service`

- Fermare il servizio "<nome_servizio>.service" immediatamente:

`systemctl stop <nome_servizio>.service`

- Riavviare il servizio "<nome_servizio>.service" immediatamente:

`systemctl restart <nome_servizio>.service`

- Per ricaricare la configurazione di un servizio digitare:

`systemctl reload <nome_servizio>.service`

- Abilitare l'avvio di "<nome_servizio>.service" all'avvio di sistema:

`systemctl enable <nome_servizio>.service`

- Disabilitare l'avvio di "<nome_servizio>.service" all'avvio di sistema:

`systemctl disable <nome_servizio>.service`

- Per rendere un servizio non avviabile sia manualmente che automaticamente digitare:

`systemctl mask <nome_servizio>.service`

- Per riabilitare un servizio a essere avviabile manualmente o automaticamente digitare:

`systemctl unmask <nome_servizio>.service`

#### Controllo

- Visualizzare lo stato del servizio "<nome_servizio>.service":

`systemctl status <nome_servizio>.service`

- Per verificare se un determinato servizio è attivo digitare:

`systemctl is-active <nome_servizio>.service`

-Per verificare se un determinato servizio viene attivato all'avvio del sistema digitare:

`systemctl is-enabled <nome_servizio>.service`

- Per verificare se un servizio ha fallito l'avvio digitare:

`systemctl is-failed <nome_servizio>.service`


#### Informazioni

- Per elencare tutti i servizi disponibili digitare:

`systemctl list-units --type=service`

- Per elencare i servizi attivi nel sistema digitare:

`systemctl list-units --type=service --state=running`

#### Un esempio con apache2

```bash
sudo systemctl start apache2
sudo systemctl stop apache2
systemctl status apache2
sudo systemctl enable apache2
sudo systemctl disable apache2
```

Abilitare un servizio all’avvio implica che systemctl crearà un link simbolico allo stesso nella cartella utilizzata da Systemd per l’autostart, di solito `/etc/systemd/system/`. 
Viceversa disabilitare il servizio rimuoverà questo link.

### Creare o alterare servizi

Le unità sono definite da singoli file di configurazione chiamati file di unità. I file di unità forniti da Debian sono posizionati nella directory `/lib/systemd/system`. 
Se esiste un file di unità locale con un nome identico nella directory `/etc/systemd/system`, questo avrà la precedenza e systemd ignorerà il file nella directory /lib/systemd/system.

Gli amministratori di sistema dovrebbero mettere i file di unità nuovi o quelli altamente personalizzati in /etc/systemd/system.


```bash
sudo systemctl edit <nome_servizio>.service
```

## Collegamenti

- [https://www.lffl.org/2020/03/guida-systemctl-systemd.html](https://www.lffl.org/2020/03/guida-systemctl-systemd.html)
- [https://wiki.ubuntu-it.org/AmministrazioneSistema/Systemd](https://wiki.ubuntu-it.org/AmministrazioneSistema/Systemd)
- [https://wiki.debian.org/it/systemd](https://wiki.debian.org/it/systemd)
- [https://wiki.archlinux.org/title/Systemd_(Italiano)/User_(Italiano)](https://wiki.archlinux.org/title/Systemd_(Italiano)/User_(Italiano))
- [https://it.wikipedia.org/wiki/Boot](https://it.wikipedia.org/wiki/Boot)
- [https://it.wikipedia.org/wiki/Systemd](https://it.wikipedia.org/wiki/Systemd)


# systemd-analyze 

systemd-analyze è un comando da terminale che permette di analizzare il boot del sistema, alla ricerca di problemi che rallentano questa fase o suggerire ottimizzazioni per velocizzarla.
Il primo, semplice, comando è:

```bash
systemd-analyze time

Startup finished in 17.954s (kernel) + 8.477s (userspace) = 26.432s
graphical.target reached after 8.430s in userspace.
```
Per approfondire la conoscenza del processo di avvio utilizzare il comando:

```bash
systemd-analyze blame
```
che mostra la durata di ogni servizio in successione. In realtà la lista non tiene conto dei rapporti fra i servizi.

Per una lista gerarchica delle unità:

```bash
systemd-analyze critical-chain

The time when unit became active or started is printed after the "@" character.
The time the unit took to start is printed after the "+" character.

graphical.target @8.430s
└─multi-user.target @8.430s
  └─exim4.service @8.184s +245ms
    └─network-online.target @8.010s
      └─NetworkManager-wait-online.service @1.635s +6.374s
        └─NetworkManager.service @1.592s +38ms
          └─dbus.service @1.482s +107ms
            └─basic.target @1.465s
              └─sockets.target @1.448s
                └─uuidd.socket @1.446s
                  └─sysinit.target @1.400s
                    └─systemd-binfmt.service @403ms +996ms
                      └─proc-sys-fs-binfmt_misc.mount @1.385s +12ms
                        └─systemd-journald.socket @335ms
                          └─system.slice @294ms
                            └─-.slice @294ms
```

Un altro comando è:

```bash
systemd-analyze plot > bootup.svg
```
che indirizza l'output, simile a quello precedente, in una immagine svg.

Infine, un ultimo comando che può tornare utile:

```bash
systemd-analyze dot ‘unit_name.*’ | dot -Tsvg > unit_name.svg
```
che permette di creare un grafico svg in cui sono mostrate tutte le relazioni fra un unità e l'altra.

## Collegamenti

- [https://www.marcosbox.org/2021/10/usare-systemd-analyze.html](https://www.marcosbox.org/2021/10/usare-systemd-analyze.html)
- [https://www.freedesktop.org/software/systemd/man/systemd-analyze.html](https://www.freedesktop.org/software/systemd/man/systemd-analyze.html)

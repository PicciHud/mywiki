# unattended-upgrades

Il pacchetto `unattended-upgrades` viene utilizzato per installare automaticamente i pacchetti aggiornati e può essere configurato per installare automaticamente solo gli aggiornamenti di sicurezza.
Può essere installarlo con il seguente comando:

```bash
sudo apt install unattended-upgrades
```

Abilitare quindi il servizio:

```bash
sudo systemctl enable --now unattended-upgrades.service

Synchronizing state of unattended-upgrades.service with SysV service script with /lib/systemd/systemd-sysv-install.
Executing: /lib/systemd/systemd-sysv-install enable unattended-upgrades

sudo systemctl status unattended-upgrades.service
● unattended-upgrades.service - Unattended Upgrades Shutdown
     Loaded: loaded (/lib/systemd/system/unattended-upgrades.service; enabled; preset: enabled)
     Active: active (running) since Sat 2023-04-15 19:21:21 CEST; 4s ago
       Docs: man:unattended-upgrade(8)
   Main PID: 33300 (unattended-upgr)
      Tasks: 2 (limit: 16595)
     Memory: 11.8M
        CPU: 59ms
     CGroup: /system.slice/unattended-upgrades.service
             └─33300 /usr/bin/python3 /usr/share/unattended-upgrades/unattended-upgrade-shutdown --wait-for-signal

apr 15 19:21:21 desktop systemd[1]: Started unattended-upgrades.service - Unattended Upgrades Shutdown.
```

Possiamo configurare le impostazioni del pacchetto modificando il file `/etc/apt/apt.conf.d/50unattended-upgrades`. 

Il pacchetto unattended-upgrades può essere configurato per aggiornare tutti i pacchetti o solo gli aggiornamenti di sicurezza. 
La prima sezione definisce quali tipi di pacchetti verranno automaticamente aggiornati. Per impostazione predefinita, installerà solo gli aggiornamenti di sicurezza, 
se si desidera abilitare gli aggiornamenti dagli altri repository, è possibile decommentare il repository appropriato. 

```bash
sudo nano /etc/apt/apt.conf.d/50unattended-upgrades

[...]

Unattended-Upgrade::Origins-Pattern {
        // Codename based matching:
        // This will follow the migration of a release through different
        // archives (e.g. from testing to stable and later oldstable).
        // Software will be the latest available for the named release,
        // but the Debian release itself will not be automatically upgraded.
        //"origin=Debian,codename=${distro_codename}-updates";
        //"origin=Debian,codename=${distro_codename}-proposed-updates";
        //"origin=Debian,codename=${distro_codename},label=Debian";
        "origin=Debian,codename=${distro_codename},label=Debian-Security";
        "origin=Debian,codename=${distro_codename}-security,label=Debian-Security";

        // Archive or Suite based matching:
        // Note that this will silently match a different release after
        // migration to the specified archive (e.g. testing becomes the
        // new stable).
//      "o=Debian,a=stable";
//      "o=Debian,a=stable-updates";
//      "o=Debian,a=proposed-updates";
//      "o=Debian Backports,a=${distro_codename}-backports,l=Debian Backports";
};
```

## Abilitare gli aggiornamenti automatici di sicurezza

Per abilitare l'aggiornamento automatico, è necessario assicurarsi che il file di configurazione `/etc/apt/apt.conf.d/20auto-upgrades` contenga almeno le seguenti due righe, 
che dovrebbero essere incluse per impostazione predefinita:

```bash
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Unattended-Upgrade "1";
```
La configurazione precedente aggiorna l'elenco dei pacchetti e installa gli aggiornamenti disponibili ogni giorno.

È possibile anche aggiungere la seguente riga che pulirà l'archivio dei download locale ogni 7 giorni:

```bash
APT::Periodic::AutocleanInterval "7";
```

Quindi dare il seguente comando:

```bash
sudo dpkg-reconfigure --priority -low ined upgrades
```

### Verificare che siano correttamente abilitati

Per verificare se gli aggiornamenti automatici funzionano correttamente, procedere nel seguente modo:

```bash
sudo unattended-upgrades --dry-run --debug
```

L'output dovrebbe assomigliare a questo:

```bash
...
pkgs that look like they should be upgraded:
Fetched 0 B in 0s (0 B/s)
fetch.run() result: 0
blacklist: []
whitelist: []
No packages found that can be upgraded unattended and no pending auto-removals
```

La cronologia degli aggiornamenti automatici viene salvata nel file `/var/log/unattended-upgrades/unattended-upgrades.log`.

## Collegamenti

- [https://linuxhint.com/configure-automatic-security-updates-debian/](https://linuxhint.com/configure-automatic-security-updates-debian/)
- [https://noviello.it/come-abilitare-e-configurare-aggiornamenti-automatici-su-ubuntu-18-04/](https://noviello.it/come-abilitare-e-configurare-aggiornamenti-automatici-su-ubuntu-18-04/)

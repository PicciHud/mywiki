## Personalizzare KDE

Ecco alcuni temi molto interessanti compatibili in toto con KDE Plasma:

- [https://github.com/catppuccin/catppuccin](https://github.com/catppuccin/catppuccin)

- [https://draculatheme.com](https://draculatheme.com)

1. Clonare questo repository:

```bash
git clone https://github.com/catppuccin/KDE && cd KDE/kde-store-archives/global-theme
```

2. Installare il tema con questo comando:    

```bash
kpackagetool5 -i catppuccin.tar.gz
```

![KDE](./asset/kde/kde.png 'KDE')

Un tema molto bello per le icone della barra di stato è **Materia Theme**, installabile con questo comando:

```bash
wget -qO- https://raw.githubusercontent.com/PapirusDevelopmentTeam/materia-kde/master/install.sh | sh
```

Qui maggiori informazioni:

[https://github.com/PapirusDevelopmentTeam/materia-kde](https://github.com/PapirusDevelopmentTeam/materia-kde)

![Status](./asset/kde/statusbar.png 'Barra di stato') 

![Material](./asset/kde/material.png 'Material Theme') 

Nelle impostazioni di KDE, impostare `#1f2430` come colore di sfondo delle finestre:

![1f2430](./asset/kde/1f2430.png '1f2430')

![Material](./asset/kde/material2.png 'Material Theme')

## Vim

[https://github.com/catppuccin/vim](https://github.com/catppuccin/vim)

Ecco delle impostazioni base per Vim:

```bash
davide@piccihud:~$ cat .vimrc 
set number
colorscheme catppuccin_mocha
set spell spelllang=it
syntax enable
set termguicolors
```

Il tema viene installato in questo modo:

```bash
cd ~
git clone https://github.com/catppuccin/vim.git
mv colors/ .vim/
```
Inserire la seguente riga nel file `.vimrc`:

```vim
colorscheme catppuccin_mocha
```

## Firefox

Qui si trovano moltissimi temi CSS per Firefox:

[https://firefoxcss-store.github.io/](https://firefoxcss-store.github.io/)

Uno dei migliori:

[https://github.com/crambaud/waterfall](https://github.com/crambaud/waterfall)

![Firefox](./asset/kde/firefox.png 'Waterfall Theme')

Oppure: [https://github.com/aadilayub/firefox-i3wm-theme](https://github.com/aadilayub/firefox-i3wm-theme)

### Dark Reader

[https://github.com/catppuccin/dark-reader](https://github.com/catppuccin/dark-reader)

> Mocha

```css	
Background 	#1e1e2e

Text 		#cdd6f4

Selection 	#585b70
```

![Mocha](./asset/kde/reader.png 'Tema')

![Menù](./asset/kde/reader-menu.png 'Menù')

## Cambiare Display manager

[https://github.com/fairyglade/ly](https://github.com/fairyglade/ly)

Per prima cosa, bisogna installare le dipendenze:

```bash
sudo -i

apt install build-essential libpam0g-dev libxcb-xkb-dev
```

Quindi clonare il repository Git e seguire le istruzioni per l'installazione: 

```bash
git clone --recurse-submodules https://github.com/fairyglade/ly
cd ly
make
make install installsystemd
systemctl enable ly.service
```

Maggiori informazioni alla pagina sopra elencata. Di seguito il risultato ottenuto:

![ly](./asset/kde/ly.png 'Display Manager')

Il file di configurazione si trova in `/etc/ly/config.ini`.


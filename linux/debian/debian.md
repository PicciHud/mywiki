# Debian KDE

Come prima cosa, occorre scaricare la .iso della  distro in questione:

[https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/11.6.0-live+nonfree/amd64/iso-hybrid/](https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/11.6.0-live+nonfree/amd64/iso-hybrid/) 

Nell'esempio, `Debian 11 KDE`.

Al termine del download, creare la chiavetta con la live, al fine di poter intraprendere l'installazione.

## Balena Etcher

Come prima cosa, importare i repository:

```bash
curl -1sLf 'https://dl.cloudsmith.io/public/balena/etcher/setup.deb.sh' | sudo -E bash
```

Quindi procedere con l'installazione:

```bash
sudo apt update ; sudo apt install balena-etcher-electron -y
```

Adesso è possibile avviare il programma, seleziore l'.iso scaricata precedentemente, il supporto corretto ed avviare la procedura.

## Installazione di Debian

Avviare la live e da qui procedere con l'installazione guidata, cliccando sull'icona. Rispetto al 'graphic installer' del menù, è decisamente più semplice.

## Modificare i repository

Al fine di avere una distribuzione con pacchetti più aggiornati, occorre modificare il file `/etc/apt/source.list`.

[https://guide.debianizzati.org/index.php/Repository_ufficiali](https://guide.debianizzati.org/index.php/Repository_ufficiali)

Se si vuole restare sempre con la versione testing di Debian, a prescindere dai rilasci di Debian, scegliere questa configurazione:
 
```bash
# Debian testing
deb http://deb.debian.org/debian/ testing main
deb-src http://deb.debian.org/debian/ testing main
 
# Aggiornamenti di sicurezza
deb http://security.debian.org/debian-security testing-security main
deb-src http://security.debian.org/debian-security testing-security main

# Gli aggiornamenti raccomandati per testing (testing-updates) sono disabilitati
```

Quindi procedere con l'aggiornamento dei pacchetti:

```bash
sudo apt update ; sudo apt -y upgrade ; sudo apt autoremove -y  ; sudo apt autoclean -y ; pkcon refresh ; pkcon update -y
```

## Bismuth

```bash
echo "deb http://deb.volian.org/volian/ scar main" | sudo tee /etc/apt/sources.list.d/volian-archive-scar-unstable.list > /dev/null
wget -qO - https://deb.volian.org/volian/scar.key | sudo tee /etc/apt/trusted.gpg.d/volian-archive-scar-unstable.gpg > /dev/null
sudo apt update && sudo apt install kwin-bismuth
```
Tramite `Bismuth`, uno script per KDE, è possibile avere semplicemente un tiling window manager.

[https://github.com/Bismuth-Forge/bismuth](https://github.com/Bismuth-Forge/bismuth)

![bismuth](./asset/debian/debian/bismuth.png 'Bismuth')

## Polonium

Per KDE 5.27 e superiore: [https://zeroxoneafour.github.io/polonium/](https://zeroxoneafour.github.io/polonium/)

## Papirus Icon

[https://github.com/PapirusDevelopmentTeam/papirus-icon-theme](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme)

Il miglior icon pack per Gnu/Linux!

```bash
sudo apt update ; sudo apt install papirus-icon-theme -y
```

![papirus](./asset/debian/papirus.png 'Papirus icon pack')

### Papirus Theme for LibreOffice

[https://sourcedigit.com/23703-install-papirus-theme-for-libreoffice-papirus-libreoffice-theme/](https://sourcedigit.com/23703-install-papirus-theme-for-libreoffice-papirus-libreoffice-theme/)

```bash
wget -qO- https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-libreoffice-theme/master/install-papirus-root.sh | sh
```

In questo modo si installa il tema per LibreOffice, indipendentemente dalla distro.

![Papirus Theme](./asset/debian/libreoffice1.png 'Opzioni')
![Papirus Theme](./asset/debian/libreoffice2.png 'Vista')

## Rofi

Installare Rofi e poi procedere con le sue configurazioni: ![Rofi](https://gitea.it/PicciHud/Appunti/wiki/Rofi)

```bash
sudo apt update && sudo apt install -y rofi
```

## .NET

[https://learn.microsoft.com/it-it/dotnet/core/install/linux-debian](https://learn.microsoft.com/it-it/dotnet/core/install/linux-debian)

## Brave Browser

[https://brave.com/linux/#nightly-channel-installation](https://brave.com/linux/#nightly-channel-installation)

## Newsboat : RSS/Atom feedreader

Si tratta di un lettore RSS testuale. Si installa direttamente dai repo ufficiali di Debian:

```bash
sudo apt update ; sudo apt install newsboat
```

Per le varie configurazioni, seguire la documentazione ufficiale:

[https://newsboat.org/releases/2.29/docs/newsboat.html](https://newsboat.org/releases/2.29/docs/newsboat.html)

Per aggiungere un feed, basta inserirle URL in questo file:

```bash
~/.newsboat/urls 
```

Oppure è possibile importare un file OPML con il seguente comando:

```bash
newsboat -i file.opml
```

I comandi base:

- `Shift+R`: download degli articoli dai feed inseriti;

- `/`: ricerca all'interno di tutti i files;

- `A`: marca il feed come letto;

- `Q`: chiude Newsboat;

- `O`: apre l'articolo nel  browser;

- `Q`: permette di ritornare nel terminale.

### Fluent Reader

Una valida alternativa:

[https://hyliu.me/fluent-reader/](https://hyliu.me/fluent-reader/)

## Collegamenti

- [https://wiki.debian.org/it/SourcesList](https://wiki.debian.org/it/SourcesList)

- [https://www.debian.org/devel/testing](https://www.debian.org/devel/testing)

- [https://it.linux-console.net/?p=348#gsc.tab=0](https://it.linux-console.net/?p=348#gsc.tab=0)

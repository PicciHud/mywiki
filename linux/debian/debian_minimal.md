# Debian minimale

La iso può essere scaricata da qui: [https://cdimage.debian.org/cdimage/unofficial/non-free/images-including-firmware/current/amd64/iso-cd/](https://cdimage.debian.org/cdimage/unofficial/non-free/images-including-firmware/current/amd64/iso-cd/)

Oppure esiste anche la mini.iso, dal peso di circa 50 MB: [https://deb.debian.org/debian/dists/bullseye/main/installer-amd64/current/images/netboot/](https://deb.debian.org/debian/dists/bullseye/main/installer-amd64/current/images/netboot/)

Selezionare `Advance options ...`

![debian](./asset/minidebian/debian1.png)

quindi `Expert install`

![debian](./asset/minidebian/debian2.png)

Impostare la lingua desiderata. Qui sarà scelta la lingua *italiana*. Lo stesso vale per il layout della tastiera.

Il punto successivo, *Access software for a blind person using a braille display* può essere eluso

![debian](./asset/minidebian/debian3.png)

Rilevare e montare i supporti di installazione

![debian](./asset/minidebian/debian4.png)

Caricare i seguenti moduli aggiuntivi, che tuttavia, come specificato nella descrizione, non sono necessari

![debian](./asset/minidebian/debian5.png)

Configurare la rete automaticamente, tramite il DHCP

![debian](./asset/minidebian/debian6.png)

![debian](./asset/minidebian/debian7.png)

Stabilire il nome dell'host

![debian](./asset/minidebian/debian8.png)

Abilitare le `shadow password`

![debian](./asset/minidebian/debian9.png)

Non permettere l'accesso a root

![debian](./asset/minidebian/debian10.png)

Stabilire il nome dell'utente e assegnargli una password

![debian](./asset/minidebian/debian11.png)

### Partizionamento manuale

![debian](./asset/minidebian/debian12.png)

Selezionare il disco corretto

![debian](./asset/minidebian/debian13.png)

Creare una nuova tabella delle partizioni di tipo `GPT`

![debian](./asset/minidebian/debian14.png)

![debian](./asset/minidebian/debian15.png)

Selezionare nuovamente il disco e creare la partizione `/boot/efi`

![debian](./asset/minidebian/debian16.png)

![debian](./asset/minidebian/debian17.png)

100 MB sono sufficienti

![debian](./asset/minidebian/debian18.png)

![debian](./asset/minidebian/debian19.png)

Completare il partizionamento. Importante che sia avviabile

![debian](./asset/minidebian/debian20.png)

Procedere allo stesso modo per la partizione `/boot`, sempre di 100 MB. Usare come file-system `ext2`

![debian](./asset/minidebian/debian21.png)

Quindi creare la partizione di root `/`. Volendo è possibile creare una partizione `/home` separata.

Come file-system va benissimo sia `ext4` che `btrfs`

![debian](./asset/minidebian/debian22.png)

#### Cifrare la partizione root

![debian](./asset/minidebian/debian23.png)

Selezionare la partizione `/`

![debian](./asset/minidebian/debian24.png)

![debian](./asset/minidebian/debian25.png)

Dopo aver completato la cifratura, selezionare il volume e assegnargli il punto di mount corretto e pure il tipo di file-system

![debian](./asset/minidebian/debian26.png)

![debian](./asset/minidebian/debian27.png)

Saltare la creazione della partizione di swap

![debian](./asset/minidebian/debian28.png)

Proseguire con l'installazione del sistema di base

![debian](./asset/minidebian/debian29.png)

![debian](./asset/minidebian/debian30.png)

Per evitare problemi di compatibilità, installare tutti di driver disponibili

![debian](./asset/minidebian/debian31.png)

![debian](./asset/minidebian/debian32.png)

### Partizionamento manuale con LVM

Qualora si desiderasse utilizzare LVM, dopo aver creato una nuova tabella delle partizioni GPT

![debian](./asset/minidebian/debian45.png)

Selezionare partizionamento manuale

![debian](./asset/minidebian/debian46.png)

![debian](./asset/minidebian/debian47.png)

Dopo aver creato la partizione `boot/efi` e la partizione di `/boot` di almeno 150 MB,
procedere con la creazione del LVM. Per maggiori informazioni leggere qui: [LVM](https://it.wikipedia.org/wiki/Gestore_logico_dei_volumi)

![debian](./asset/minidebian/debian48.png)

Creare il gruppo di volumi (nell'esempio composto da un solo disco) con nome a piacere

![debian](./asset/minidebian/debian49.png)

![debian](./asset/minidebian/debian50.png)

Creare quindi il *volume logico*, assegnando tutto la spazio a disposizione

![debian](./asset/minidebian/debian51.png)

#### Cifrare partizione di root-LVM

![debian](./asset/minidebian/debian52.png)

![debian](./asset/minidebian/debian53.png)

![debian](./asset/minidebian/debian54.png)

![debian](./asset/minidebian/debian55.png)

![debian](./asset/minidebian/debian56.png)

Questa la situazione finale. Se si volesse, è sempre possibile creare una partizione `/home` separata

![debian](./asset/minidebian/debian57.png)

### Configurare il gestore pachetti

![debian](./asset/minidebian/debian33.png)

![debian](./asset/minidebian/debian34.png)

Utilizzare il *software non libero* a scelta, in base anche al proprio hardware

![debian](./asset/minidebian/debian35.png)

Abilitare anche i software *backported*

![debian](./asset/minidebian/debian36.png)

Installare automaticamente gli aggiornamenti di sicurezza

![debian](./asset/minidebian/debian37.png)

Per un installazione minimale, selezionare solo `Utilità di sistema standard`. Il DE verrà installato successivamente

![debian](./asset/minidebian/debian38.png)

### Installazione di GRUB

![debian](./asset/minidebian/debian39.png)

![debian](./asset/minidebian/debian40.png)

Terminare quindi l'installazione

![debian](./asset/minidebian/debian41.png)

#### Configurazioni di GRUB

Dopo aver avviato il pc, modificare il file `/etc/default/grub` in questo modo:

```bash

# If you change this file, run 'update-grub' afterwards to update
# /boot/grub/grub.cfg.
# For full documentation of the options in this file, see:
#   info -f grub -n 'Simple configuration'

GRUB_DEFAULT=0
GRUB_TIMEOUT_STYLE=hidden
GRUB_TIMEOUT=0
GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
GRUB_CMDLINE_LINUX="systemd.show_status=1"
GRUB_GFXMODE=1920x1080
GRUB_GFXPAYLOAD_LINUX=keep
```

![debian](./asset/minidebian/debian42.png)

Dare quindi il comando `sudo update-grub` e riavviare la macchina.

In questo caso GRUB non verrà mostrato all'avvio e ci si troverà davanti immediatamente la schermata per de-crittografare la partizione `/`

![debian](./asset/minidebian/debian43.png)

Questa la situazione finale dei dischi

![debian](./asset/minidebian/debian44.png)

### Debian Testing
 
Per Debian testing, modificare il file `/etc/apt/sources.list` nel seguente modo:

```bash
# Debian testing
deb http://deb.debian.org/debian/ testing main contrib non-free non-free-firmware
deb-src http://deb.debian.org/debian/ testing main contrib non-free non-free-firmware
 
# Aggiornamenti di sicurezza
deb http://security.debian.org/debian-security testing-security main contrib non-free non-free-firmware
deb-src http://security.debian.org/debian-security testing-security main contrib non-free non-free-firmware

# Gli aggiornamenti raccomandati per testing (testing-updates) sono disabilitati
```

## Installazione minimale di KDE

Quindi, dopo aver modificato i repository, aggiornare la distribuzione:

```bash
sudo apt update && sudo apt upgrade --no-install-recommends
sudo apt autoremove
sudo apt dist-upgrade --no-install-recommends --no-install-suggests
sudo apt install git nala --no-install-recommends
```

Infine installare i programmi necessari:

```bash
sudo apt install kde-plasma-desktop plasma-nm sddm xserver-xorg kwin-x11 kde-config-sddm pipewire neofetch \
kde-gtk-config systemsettings kscreen khotkeys kmenuedit kwalletmanager qml-module-org-kde-pipewire \
print-manager bluedevil phonon4qt5settings kmix powerdevil vim kate \
librsvg2-dev gwenview ark unrar-free --no-install-recommends

sudo systemctl enable --now upower.service

sudo purge debian-reference-* doc-debian
```

## Collegamenti

- [https://ubuntuhandbook.org/index.php/2020/06/hide-grub-boot-menu-ubuntu-20-04-lts/](https://ubuntuhandbook.org/index.php/2020/06/hide-grub-boot-menu-ubuntu-20-04-lts/)
- [https://unix.stackexchange.com/questions/577379/how-can-i-install-debian-with-full-disk-encryption-and-a-custom-sized-swapfile](https://unix.stackexchange.com/questions/577379/how-can-i-install-debian-with-full-disk-encryption-and-a-custom-sized-swapfile)
- [https://it.linux-console.net/?p=2525](https://it.linux-console.net/?p=2525)
- [https://debian-handbook.info/browse/it-IT/stable/sect.installation-steps.html](https://debian-handbook.info/browse/it-IT/stable/sect.installation-steps.html)
- [https://www.blakehartshorn.com/installing-debian-on-existing-encrypted-lvm/](https://www.blakehartshorn.com/installing-debian-on-existing-encrypted-lvm/)
- [https://www.paritybit.ca/blog/debian-with-btrfs](https://www.paritybit.ca/blog/debian-with-btrfs)
- [https://guide.debianizzati.org/index.php/Installare_Debian:_configurazione_LVM](https://guide.debianizzati.org/index.php/Installare_Debian:_configurazione_LVM)

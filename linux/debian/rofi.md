# Rofi

Rofi non è solo un lanciatore di applicazioni, ma permette di fare molte più cose, tra cui:

- aprire file;
- eseguire comandi bash;
- navigare tra le finestre aperte;
- ...

## Modalità

Rofi ha diverse modalità integrate che implementano casi d'uso comuni e possono essere estese da vari script o plugin.

Di seguito è riportato un elenco delle diverse modalità:

- **run**: permette di lanciare eseguibili nel terminale, a partire dal loro `$PATH`;
- **drun**: avvia le applicazioni installate nel sistema;
- **window**: permette di passare tra le varie finestre aperte;
- **ssh**: avvia il collegamento a un host remoto via ssh;
- **combi**: combina più modalità.

## Installazione su Debian e derivate

```bash
sudo apt update && sudo apt install -y rofi
```

## Configurazione

Per generare il file di configurazione predefinito:

```bash
mkdir -p ~/.config/rofi
rofi -dump-config > ~/.config/rofi/config.rasi
```

Questo comando crea un file chiamato `config.rasi` nel seguente percorso: `~/.config/rofi/`. È possibile modificare questo file per personalizzare rofi, anche graficamente con dei temi personalizzati.

Eccone un esempio: [config.rasi](./asset/rofi//config.rasi)

### Abilitare le varie modalità

È sufficiente specificare un elenco ordinato e separato da virgole delle modalità da abilitare. Ecco un esempio, con abilitate solo le modalità `run` e `ssh`:

```bash
rofi -modes "run,ssh" -show run
```

Per ottenere una visione combinata di due o più modalità:

```bash
rofi -show combi -combi-modes "window,run,ssh" -modes combi
```

## Temi personalizzati

Qui si trovano moltissimi temi:

[https://github.com/adi1090x/rofi](https://github.com/adi1090x/rofi)

Dopo aver installato `rofi`, clonare il precedente repository:

```bash
git clone --depth=1 https://github.com/adi1090x/rofi.git
cd rofi
chmod 755 setup.sh
./setup.sh
```
Quindi, per scegliere lo stile, modificare il file `~/.config/rofi/launchers/type-X/launcher.sh`, dove `X` è un numero da 1 a 7, inserendo lo stile prescelto.  

Nell'esempio:

```bash
theme='style-10'
```
Ecco il file in questione:

```
davide@piccihud:~$ cat ~/.config/rofi/launchers/type-4/launcher.sh
#!/usr/bin/env bash

## Author : Aditya Shakya (adi1090x)
## Github : @adi1090x
#
## Rofi   : Launcher (Modi Drun, Run, File Browser, Window)
#
## Available Styles
#
## style-1     style-2     style-3     style-4     style-5
## style-6     style-7     style-8     style-9     style-10

dir="$HOME/.config/rofi/launchers/type-4"
theme='style-10'

## Run
rofi \
    -show drun \
    -theme ${dir}/${theme}.rasi
```

Come prima, per cambiare i colori predefiniti del tema, modificare il seguente file:

```bash
davide@piccihud:~$ cat ~/.config/rofi/launchers/type-4/shared/colors.rasi
/**
 *
 * Author : Aditya Shakya (adi1090x)
 * Github : @adi1090x
 * 
 * Colors
 *
 * Available Colors Schemes
 *
 * adapta    catppuccin    everforest    navy       paper
 * arc       cyberpunk     gruvbox       nord       solarized
 * black     dracula       lovelace      onedark    yousai
 *
 **/

/* Import color-scheme from `colors` directory */

@import "~/.config/rofi/colors/dracula.rasi"	#Inserire il colore prescelto
```

Questo è tutto. Ora basta inserire nel file `config.rasi` la seguente riga, una volta scelto lo stile del tema:

```bash
davide@piccihud:~$ cat .config/rofi/config.rasi
@theme "~/.config/rofi/launchers/type-4/style-10.rasi"
```

![Rofi](./asset/rofi/rofi.png 'Rofi')

### Alcune aggiunte al tema

`~/.config/rofi/launchers/type-4/style-10.rasi`

```bash
 10 /*****----- Configuration -----*****/
 11 configuration {
 12         modi:                       "drun,window,run"; /*aggiunte alcune modalità, come quella per mostrare le finestre attive o per eseguire comandi da terminale*/
 13     show-icons:                 true;  /*le icone sono visibili*/
 14     display-drun:               " Apps";
 15     display-run:                "";
 16     display-filebrowser:        "";
 17     display-window:             "";
 18 /*      drun-display-format:        "{name} [<span weight='light' size='small'><i>({generic})</i></span>]"; */
 19         drun-display-format:        "{name}"; /*solo il nome dell'applicazione diventa visibile, senza descrizione, come sopra*/
 20         window-format:              "{w} · {c} · {t}";
 21 }

[..]

 53 /*****----- Main Window -----*****/
 54 window {
 55     /* properties for window widget */
 56     transparency:                "real";
 57     location:                    south; /*rofi è posizionato in basso*/                                                                                                                                                                                            
 58     anchor:                      south;
 59     fullscreen:                  false;
 60     width:                       100%;
 61     x-offset:                    0px;
 62     y-offset:                    0px;
```
Quindi modificare il file `.config/rodi/config.rasi`:

```bash
  1 configuration {
  3         terminal: "konsole";
  4         run-command: "{cmd}";
  5         run-list-command: "";
  6         run-shell-command: "{terminal} -e {cmd}"; /*Questa è la sintassi del comando*/
  7         kb-select-1: "Super+1";
  8         kb-select-2: "Super+2";
  9         kb-select-3: "Super+3";
 10         kb-select-4: "Super+4";
 11         kb-select-5: "Super+5";
 12         kb-select-6: "Super+6";
 13         kb-select-7: "Super+7";
 14         kb-select-8: "Super+8";
 15         kb-select-9: "Super+9";
 16         kb-select-10: "Super+0";
 17 }
 18 @theme "~/.config/rofi/launchers/type-4/style-10.rasi"
```

In questo modo, è possibile lanciare comandi bash direttamente da rofi, tramite la seguente sintassi:

```bash
terminal_name command

konsole ncdu
```

oppure passare tra le finestre attive, infine selezionare i risultati con delle scorciatoie (`Alt+X`), dove `X` indica un numero maggiore o uguale a 1, corrispondente al risultato dell'applicazione che si desidera avviare.

Il risultato finale:

![Rofi](./asset/rofi/final.png 'Rofi')

### Gestire le connessioni di rete

Rofi può essere esteso tramite plugin. Eccone uno per poter controllare la connessione ad intenet:

```bash
git clone https://github.com/ericmurphyxyz/rofi-wifi-menu.git
cd rofi-wifi-menu
mv rofi-wifi-menu.sh ~/bin
```
È possibile richiamare lo script dopo aver assegnato una scorciatoria da tastiera: `$HOME/bin/rofi-wifi-menu.sh`

![WiFi](./asset/rofi/wifi.png)

## Collegamenti

- [https://github.com/davatorium/rofi](https://github.com/davatorium/rofi)
- [https://github.com/adi1090x/rofi](https://github.com/adi1090x/rofi)
- [https://github.com/adi1090x/rofi/blob/master/files/config.rasi](https://github.com/adi1090x/rofi/blob/master/files/config.rasi)

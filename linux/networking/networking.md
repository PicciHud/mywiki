# Basic Linux Networking Commands

## Indirizzo IP

Il comando `ip --color a` permette di visualizzare l'indirizzo ip della macchina:

```bash
ip --color a

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
       
       [...]

3: wlp4s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000

       [...]

    inet 192.168.1.127/24 brd 192.168.1.255 scope global noprefixroute wlp4s0
       valid_lft forever preferred_lft forever
    inet6 fe80::497e:1f53:1218:37cc/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
```
### Impostare un IP statico

```bash
vim /etc/network/interfaces     

auto eth0
iface eth0 inet static
    address 192.168.1.56
    netmask 255.255.255.0
    gateway 192.168.1.1
    dns-nameservers 5.2.75.75 1.1.1.1

systemctl restart networking
```

Nel caso si volesse impostare un IP statico temporaneamente:

```bash
ip addr add 172.19.1.10/24 dev eth0
```

### Rimuovere un IP da un interfaccia

```bash
ip addr del 172.19.1.10/24 dev eth0
```

### Abilitare/disabilitare un interfaccia

```bash
ip link set eth0 up
ip link set eth0 down
```

## Tabella di routing

```bash
ip route show
```
### Aggiungere o rimuovere una rotta statica

```bash
ip route add 172.19.1.0/24 dev eth0

ip route del 172.19.1.0/24 dev eth0
```

Per aggiungere il default gateway:

```bash
ip route add default via 172.17.0.1
```
per rimuoverlo: `ip route del default`

## traceroute

Col comando traceroute è possibile identificare il percorso intrapreso dai pacchetti per raggiungere l'host.

```bash
traceroute duckduckgo.com

traceroute to duckduckgo.com (40.114.177.156), 30 hops max, 60 byte packets
 1  _gateway (192.168.1.1)  4.449 ms  11.289 ms  12.478 ms
 2  pppoe-server.net.ngi.it (81.174.0.21)  18.152 ms  19.602 ms  21.332 ms
 3  10.222.163.138 (10.222.163.138)  38.414 ms  37.719 ms  48.982 ms
 4  10.40.83.137 (10.40.83.137)  50.339 ms  48.975 ms  50.969 ms
 5  * * *
 6  ae70-0.mil30-96cbe-1a.ntwk.msn.net (104.44.37.3)  52.354 ms  28.915 ms  25.263 ms
 7  ae22-0.ear01.zrh20.ntwk.msn.net (104.44.235.8)  30.643 ms  39.978 ms  41.664 ms
 8  be-20-0.ibr01.zrh20.ntwk.msn.net (104.44.33.81)  61.505 ms  62.187 ms be-21-0.ibr02.zrh20.ntwk.msn.net (104.44.33.83)  77.807 ms
 9  be-7-0.ibr01.zrh21.ntwk.msn.net (104.44.30.69)  67.861 ms be-19-0.ibr02.mrs20.ntwk.msn.net (104.44.29.101)  76.403 ms be-7-0.ibr01.zrh21.ntwk.msn.net (104.44.30.69)  67.809 ms
10  be-2-0.ibr01.fra23.ntwk.msn.net (104.44.16.141)  55.156 ms  71.792 ms  71.739 ms
11  be-2-0.ibr04.bl7.ntwk.msn.net (104.44.7.77)  58.408 ms  53.057 ms be-4-0.ibr02.ams21.ntwk.msn.net (104.44.17.55)  53.295 ms
12  ae120-0.icr01.ams21.ntwk.msn.net (104.44.22.242)  47.145 ms ae100-0.icr01.ams21.ntwk.msn.net (104.44.22.234)  45.453 ms ae106-0.icr04.ams21.ntwk.msn.net (104.44.23.245)  62.217 ms
```

Per impostazione predefinita, traceroute utilizzerà IPv4, ma è possibile modificare questo comportamento utilizzando IPv6 con l'opzione `-6`.

## ping

Il comando ping (Packet Internet Groper) può essere considerato uno dei comandi più importanti durante la risoluzione dei problemi della rete, in quanto è il modo più comune per controllare la connettività tra host.

```bash
 ping 5.2.75.75

PING 5.2.75.75 (5.2.75.75) 56(84) bytes of data.
64 bytes from 5.2.75.75: icmp_seq=1 ttl=55 time=67.5 ms
64 bytes from 5.2.75.75: icmp_seq=2 ttl=55 time=132 ms
64 bytes from 5.2.75.75: icmp_seq=3 ttl=55 time=78.5 ms
64 bytes from 5.2.75.75: icmp_seq=4 ttl=55 time=50.5 ms
64 bytes from 5.2.75.75: icmp_seq=5 ttl=55 time=52.0 ms
64 bytes from 5.2.75.75: icmp_seq=6 ttl=55 time=74.6 ms
64 bytes from 5.2.75.75: icmp_seq=7 ttl=55 time=59.9 ms
64 bytes from 5.2.75.75: icmp_seq=8 ttl=55 time=51.4 ms
64 bytes from 5.2.75.75: icmp_seq=9 ttl=55 time=54.9 ms
```
Con l'opzione `-c` è possibile specificare il numero di pacchetti da inviare.

## dig

Il comando dig (Domain Information Groper) viene utilizzato per recuperare tutte le informazioni necessarie sul server DNS.

È facilmente installabile col comando `sudo apt install dnsutils`.

```
dig lealternative.net

; <<>> DiG 9.18.12-1-Debian <<>> lealternative.net
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 39808
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;lealternative.net.             IN      A

;; ANSWER SECTION:
lealternative.net.      60      IN      A       84.16.89.21

;; Query time: 83 msec
;; SERVER: 45.90.28.222#53(45.90.28.222) (UDP)
;; WHEN: Sun Mar 12 17:21:46 CET 2023
;; MSG SIZE  rcvd: 62

dig @5.2.75.75 lealternative.net

; <<>> DiG 9.18.12-1-Debian <<>> @5.2.75.75 lealternative.net
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 762
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1472
;; QUESTION SECTION:
;lealternative.net.             IN      A

;; ANSWER SECTION:
lealternative.net.      600     IN      A       84.16.89.21

;; Query time: 227 msec
;; SERVER: 5.2.75.75#53(5.2.75.75) (UDP)
;; WHEN: Sun Mar 12 17:22:06 CET 2023
;; MSG SIZE  rcvd: 62
```
Dopo la `@` è possibile specificare un server DNS.

## host

Il comando host viene utilizzato principalmente per ottenere l'indirizzo IP di un dominio specifico.

```bash
host lealternative.net

;; communications error to 45.90.28.222#53: timed out
lealternative.net has address 84.16.89.21
lealternative.net has IPv6 address 2001:1600:3:5::722
lealternative.net mail is handled by 5 mail.protonmail.ch.
lealternative.net mail is handled by 10 mailsec.protonmail.ch.


host beopen.neocities.org

beopen.neocities.org has address 198.51.233.2
beopen.neocities.org has IPv6 address 2620:2:6000::a:1
```
Allo stesso modo, è possibile utilizzare un indirizzo IP per recuperare il nome di dominio:

```bash
host 5.2.75.75
75.75.2.5.in-addr.arpa domain name pointer nl.ahadns.net.
```

## whois

Il whois può aiutare a trovare informazioni sui domini registrati, indirizzi IP e molto altro ancora.

```bash
sudo apt install whois
```
Ecco un esempio del comando:

```bash
whois androidworld.it


Domain:             androidworld.it
Status:             ok
Signed:             no
Created:            2009-03-17 11:31:57
Last Update:        2022-09-09 12:04:08
Expire Date:        2023-09-08

Registrant
  Organization:     Mondadori Media S.p.A.
  Address:          Via Bianca di Savoia 12
                    Milano
                    20122
                    MI
                    IT
  Created:          2021-06-14 17:22:49
  Last Update:      2022-09-07 12:29:28

Admin Contact
  Name:             Andrea Canepa
  Organization:     Mondadori Media S.p.A.
  Address:          Via Bianca di Savoia 12
                    Milano
                    20122
                    MI
                    IT
  Created:          2021-06-14 17:22:53
  Last Update:      2022-09-07 12:30:05

Technical Contacts
  Name:             Technical Manager
  Organization:     BARBERO and Associates Ltd
  Address:          3 Shortlands, 4th floor
                    London
                    W6 8DA
                    GB
                    GB
  Created:          2014-10-09 11:19:42
  Last Update:      2019-09-19 10:00:50

Registrar
  Organization:     BARBERO & Associates Ltd
  Name:             BARBERO-REG
  Web:              https://www.barbero.co.uk
  DNSSEC:           yes


Nameservers
  ns-666.awsdns-19.net
  ns-1703.awsdns-20.co.uk
  ns-74.awsdns-09.com
```

In alternativa, si può utilizzare un indirizzo IP invece di un dominio e si otterranno gli stessi dettagli.

## ifplugstatus

```bash
sudo apt install ifplugd
```
Viene utilizzato per rilevare lo stato del collegamento della porta ethernet:

```bash
sudo ifplugstatus

lo: link beat detected
eno1: unplugged
wlp4s0: link beat detected
```
## nmcli

È possibile utilizzare il comando nmcli per monitorare lo stato di connettività delle interfacce di rete:

```bash
nmcli dev status

DEVICE          TYPE      STATE                   CONNECTION 
wlp4s0          wifi      collegato               WiFi
lo              loopback  connected (externally)  lo         
p2p-dev-wlp4s0  wifi-p2p  disconnesso             --         
eno1            ethernet  non disponibile         --  
```
Se utilizzato senza alcuna opzione, stamperà a video più informazioni.

## nmap

nmap consente di effettuare *port scanning*, cioè individuazione di porte aperte su un computer bersaglio o anche su range di indirizzi IP, in modo da determinare quali servizi di rete siano disponibili.

Può anche essere utilizzato anche per scoprire le regole delle porte, la versione del sistema operativo, le vulnerabilità presenti...

```bash
nmap lealternative.net

Starting Nmap 7.93 ( https://nmap.org ) at 2023-03-12 17:44 CET
Nmap scan report for lealternative.net (84.16.89.21)
Host is up (0.048s latency).
Other addresses for lealternative.net (not scanned): 2001:1600:3:5::722
Not shown: 998 filtered tcp ports (no-response)
PORT    STATE SERVICE
80/tcp  open  http
443/tcp open  https

Nmap done: 1 IP address (1 host up) scanned in 11.23 seconds
```
### Scansionare le porte aperte

```bash
nmap scanme.nmap.org

Starting Nmap 7.93 ( https://nmap.org ) at 2023-03-12 18:19 CET
Nmap scan report for scanme.nmap.org (45.33.32.156)
Host is up (0.19s latency).
Other addresses for scanme.nmap.org (not scanned): 2600:3c01::f03c:91ff:fe18:bb2f
Not shown: 995 closed tcp ports (conn-refused)
PORT      STATE    SERVICE
22/tcp    open     ssh
25/tcp    filtered smtp
80/tcp    open     http
9929/tcp  open     nping-echo
31337/tcp open     Elite

Nmap done: 1 IP address (1 host up) scanned in 21.49 seconds
```
Se si va di fretta e è necessario finire la scansione il prima possibile, si può utilizzare l'opzione `-F`

### Scansionare più host

Potrebbe essere necessario eseguire la scansione di più host e nmap offre vari modi per eseguire tale scansione, tra cui usare i caratteri jolly:

```bash
nmap -F 192.168.1.*

Starting Nmap 7.93 ( https://nmap.org ) at 2023-03-12 18:25 CET
Nmap scan report for 192.168.1.1
Host is up (0.065s latency).
Not shown: 97 closed tcp ports (conn-refused)
PORT   STATE SERVICE
22/tcp open  ssh
53/tcp open  domain
80/tcp open  http

Nmap scan report for 192.168.1.21
Host is up (0.0077s latency).
Not shown: 96 closed tcp ports (conn-refused)
PORT     STATE SERVICE
22/tcp   open  ssh
80/tcp   open  http
6000/tcp open  X11
6001/tcp open  X11:1

Nmap scan report for 192.168.1.27
Host is up (0.015s latency).
All 100 scanned ports on 192.168.1.27 are in ignored states.
Not shown: 100 closed tcp ports (conn-refused)

Nmap done: 256 IP addresses (6 hosts up) scanned in 17.44 seconds
```
Se durante la scansione si volesse escludere un host utilizzare la seguente sintassi: `nmap 192.168.1.* --exclude 192.168.1.6`

### Scansionare porte specifiche

È possibile specificare una o più porte o un intervallo:

```bash
nmap -p 443,80 scanme.nmap.org

nmap -p 20-80 scanme.nmap.org
```
Per ottenere maggiorni informazioni sulle porte:

```bash
nmap -sV scanme.nmap.org

Starting Nmap 7.93 ( https://nmap.org ) at 2023-03-12 18:30 CET
Nmap scan report for scanme.nmap.org (45.33.32.156)
Host is up (0.19s latency).
Other addresses for scanme.nmap.org (not scanned): 2600:3c01::f03c:91ff:fe18:bb2f
Not shown: 995 closed tcp ports (conn-refused)
PORT      STATE    SERVICE    VERSION
22/tcp    open     ssh        OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.13 (Ubuntu Linux; protocol 2.0)
25/tcp    filtered smtp
80/tcp    open     http       Apache httpd 2.4.7 ((Ubuntu))
9929/tcp  open     nping-echo Nping echo
31337/tcp open     tcpwrapped
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 34.59 seconds
```
### Scansioni stealth

Le scansioni stealth vengono eseguite quando si desidera bypassare il firewall.

```bash
sudo nmap -sS scanme.nmap.org

Starting Nmap 7.93 ( https://nmap.org ) at 2023-03-12 18:34 CET
Nmap scan report for scanme.nmap.org (45.33.32.156)
Host is up (0.29s latency).
Other addresses for scanme.nmap.org (not scanned): 2600:3c01::f03c:91ff:fe18:bb2f
Not shown: 995 closed tcp ports (reset)
PORT      STATE    SERVICE
22/tcp    open     ssh
25/tcp    filtered smtp
80/tcp    open     http
9929/tcp  open     nping-echo
31337/tcp open     Elite

Nmap done: 1 IP address (1 host up) scanned in 30.03 seconds
```

### Informazioni sull'OS

Il comando nmap può essere utilizzato anche per trovare informazioni sul sistema operativo dell'host con l'opzione `-A`:

```bash
nmap -A scanme.nmap.org

Starting Nmap 7.93 ( https://nmap.org ) at 2023-03-12 18:36 CET
Nmap scan report for scanme.nmap.org (45.33.32.156)
Host is up (0.19s latency).
Other addresses for scanme.nmap.org (not scanned): 2600:3c01::f03c:91ff:fe18:bb2f
Not shown: 995 closed tcp ports (conn-refused)
PORT      STATE    SERVICE    VERSION
22/tcp    open     ssh        OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.13 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   1024 ac00a01a82ffcc5599dc672b34976b75 (DSA)
|   2048 203d2d44622ab05a9db5b30514c2a6b2 (RSA)
|   256 9602bb5e57541c4e452f564c4a24b257 (ECDSA)
|_  256 33fa910fe0e17b1f6d05a2b0f1544156 (ED25519)
25/tcp    filtered smtp
80/tcp    open     http       Apache httpd 2.4.7 ((Ubuntu))
|_http-server-header: Apache/2.4.7 (Ubuntu)
|_http-favicon: Nmap Project
|_http-title: Go ahead and ScanMe!
9929/tcp  open     nping-echo Nping echo
31337/tcp open     tcpwrapped
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 46.43 seconds
```

### host attivi

```bash
nmap -sP 192.168.1.*

Starting Nmap 7.93 ( https://nmap.org ) at 2023-03-12 18:39 CET
Nmap scan report for 192.168.1.1
Host is up (0.032s latency).
Nmap scan report for 192.168.1.21
Host is up (0.058s latency).
Nmap scan report for 192.168.1.24
Host is up (0.28s latency).
Nmap scan report for 192.168.1.27
Host is up (0.12s latency).
Nmap scan report for 192.168.1.34
Host is up (0.19s latency).
Nmap scan report for 192.168.1.101
Host is up (0.19s latency).
Nmap scan report for 192.168.1.103
Host is up (0.26s latency).
Nmap scan report for 192.168.1.119
Host is up (0.18s latency).
Nmap scan report for 192.168.1.127
Host is up (0.00012s latency).
Nmap done: 256 IP addresses (9 hosts up) scanned in 13.19 seconds


nmap -sP 192.168.1.0/24/
```


## speedtest-cli

Come suggerisce il nome, questo comando permette di eseguire uno speedtest della rete.

```bash
sudo apt install speedtest-cli

speedtest-cli

Retrieving speedtest.net configuration...
Testing from EOLO (146.241.81.101)...
Retrieving speedtest.net server list...
Selecting best server based on ping...
Hosted by STEL s.r.l. (Ferrara) [179.97 km]: 108.923 ms
Testing download speed................................................................................
Download: 14.33 Mbit/s
Testing upload speed......................................................................................................
Upload: 2.97 Mbit/s
```
## vnstat

```bash
vnstat -i wlp4s0 -l
```
Permette di monitorare il traffico di rete. L'opzione `-i` specifica l'interfaccia, mentre `-l` è utile per il monitoraggio dal vivo.

## netstat

netstat è una delle utility più popolari per monitorare le connessioni sulla rete.

Consente di monitorare facilmente le connessioni in entrata e in uscita, le porte in ascolto e altro ancora.

Per visualizzare tutte le porte in ascolto, utilizzare l'opzione `-l`:

```bash
netstat -l

Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State      
tcp        0      0 0.0.0.0:ssh             0.0.0.0:*               LISTEN     
tcp        0      0 localhost:ipp           0.0.0.0:*               LISTEN     
tcp        0      0 localhost:smtp          0.0.0.0:*               LISTEN     
tcp6       0      0 [::]:ssh                [::]:*                  LISTEN     
tcp6       0      0 localhost:ipp           [::]:*                  LISTEN     
tcp6       0      0 localhost:smtp          [::]:*                  LISTEN     
udp        0      0 0.0.0.0:mdns            0.0.0.0:*                          
udp        0      0 0.0.0.0:55417           0.0.0.0:*                          
udp        0      0 0.0.0.0:49399           0.0.0.0:*                          
udp        0      0 0.0.0.0:631             0.0.0.0:*                          
udp        0      0 0.0.0.0:21027           0.0.0.0:*                          
udp6       0      0 [::]:mdns               [::]:*                             
udp6       0      0 [::]:22000              [::]:*                             
udp6       0      0 [::]:39924              [::]:*                             
udp6       0      0 [::]:58803              [::]:*                             
udp6       0      0 [::]:21027              [::]:*                             
raw        0      0 0.0.0.0:255             0.0.0.0:*               7          
raw6       0      0 [::]:ipv6-icmp          [::]:*                  7      
```

Per visualizzare tutte le porte utilizzare il comando `netstat -a`.

### Porte TCP e UDP

```bash
netstat -lt

netstat -lu
```
I comandi precedenti permettono di elencare le porte TCP o UDP in ascolto nel sistema.

## Collegamenti

- [https://itsfoss.com/basic-linux-networking-commands/](https://itsfoss.com/basic-linux-networking-commands/)
- [https://linuxhandbook.com/netstat-command/](https://linuxhandbook.com/netstat-command/)
- [https://linuxhandbook.com/nmap-command/](https://linuxhandbook.com/nmap-command/)
- [https://linuxhandbook.com/ifup-ifdown-ifquery/](https://linuxhandbook.com/ifup-ifdown-ifquery/)
- [https://www.tecmint.com/ip-command-examples/](https://www.tecmint.com/ip-command-examples/)










# VI mode

Nel file `.bashrc` aggiungere:

```bash
set -o vi
```

Nel file `.inputrc`, invece, aggiungere:

```bash
#"\e[5~": history-search-backward
#"\e[6~": history-search-forward

set editing-mode vi
# SHOW THE VIM MODE IN THE PROMPT (COMMAND OR INSERT)
set show-mode-in-prompt on

# SET THE MODE STRING AND CURSOR TO INDICATE THE VIM MODE
#   FOR THE NUMBER AFTER `\e[`:
#     0: blinking block
#     1: blinking block (default)
#     2: steady block
#     3: blinking underline
#     4: steady underline
#     5: blinking bar (xterm)
#     6: steady bar (xterm)

set vi-ins-mode-string (ins)\1\e[5 q\2
set vi-cmd-mode-string (cmd)\1\e[1 q\2

$if mode=vi
                set keymap vi-command
                "k": history-substring-search-backward
                "j": history-substring-search-forward
#
                set keymap vi-insert
                "\C-l": clear-screen
$endif
```

Quindi per visualizzare immediatamente gli effetti:

```bash
source .bashrc
```

## Comandi principali

Si possono utilizzare tutti i comandi e le modalità di VIM, tra cui:

| Emacs | Vim | Result |
| --- | --- | --- |
| Ctrl+A | 0 | Move cursor to beginning of line. |
| Ctrl+E | $ | Move cursor to end of line. |
| Alt+B | b | Move cursor back one word. |
| Alt+F | w | Move cursor right one word. |
| Ctrl+B | h | Move cursor back one character. |
| Ctrl+F | l | Move cursor right one character. |
| Ctrl+P | k | Move up in Bash command history. |
| Ctrl+R | j | Move down in Bash command history. |


## Collegamenti

- [https://stackoverflow.com/questions/1039713/different-bash-prompt-for-different-vi-editing-mode](https://stackoverflow.com/questions/1039713/different-bash-prompt-for-different-vi-editing-mode)
- [https://www.reddit.com/r/commandline/comments/1526ck1/how_to_add_vim_to_your_bash_prompt/](https://www.reddit.com/r/commandline/comments/1526ck1/how_to_add_vim_to_your_bash_prompt/)
- [https://dev.to/brandonwallace/how-to-use-vim-mode-on-the-command-line-in-bash-fnn](https://dev.to/brandonwallace/how-to-use-vim-mode-on-the-command-line-in-bash-fnn)

# Alias
<!-- vim-markdown-toc GFM -->

* [Creazione di alias in Bash](#creazione-di-alias-in-bash)
* [Creare alias permanenti](#creare-alias-permanenti)
* [Creazione di alias Bash con argomenti (funzioni)](#creazione-di-alias-bash-con-argomenti-funzioni)
* [List all Alias](#list-all-alias)
* [Alias che potrebbero tornare utili](#alias-che-potrebbero-tornare-utili)
* [Collegamenti](#collegamenti)

<!-- vim-markdown-toc -->

In un sistema GNU/Linux è possibile personalizzare la shell impostando degli `alias`, cioè delle scorciatoie per i comandi lunghi utilizzati frequentemente. Ad esempio, digitando il comando

```bash
ls -a --color=auto
```

in un terminale otteniamo l"elenco delle cartelle e dei file (ls), compresi i file nascosti (-a), il tutto reso mediante una colorazione che automaticamente differenzia gli elementi (--color=auto). Con la definizione di un alias per il comando ls si può fare in modo che venga eseguito completo dei parametri senza bisogno di specificarli.

## Creazione di alias in Bash

La creazione di alias in bash è molto semplice. La sintassi è la seguente:

```bash
alias alias_name="command_to_run"
```

Quindi, riprendendo l"esempio precedente:

```bash
alias lsc="ls -a --color=auto"
```

Ora, se si digita il comando `lsc` sulla console, si otterrà lo stesso output che si avrebbe digitando `ls -a --color=auto`.

## Creare alias permanenti

L"alias sarà disponibile solo nella sessione di shell corrente: se si esce dalla sessione o si apre una nuova sessione da un altro terminale, l"alias non sarà utilizzabile.

Per rendere persistente l"alias è necessario dichiararlo nel file `~/.bash_profile` o `~/.bashrc`.  

Aprire il file `~/.bashrc` e aggiungere i propri alias:

```bash
nano ~/.bashrc

...

# Aliases
# alias alias_name="command_to_run"

# color ls
alias ll="ls -la"
```

Una volta fatto, salvare e chiudere il file. Per rendere disponibili gli alias nella sessione corrente digitare:

```bash
source ~/.bashrc
```

## Creazione di alias Bash con argomenti (funzioni)

A volte potrebbe essere necessario creare un alias che accetta uno o più argomenti, ecco dove le funzioni bash sono utili.

La sintassi per creare una funzione bash è molto semplice. Possono essere dichiarati in due diversi formati:

```bash
function_name () {
  [commands]
}
```

Oppure:

```bash
function function_name {
  [commands]
}
```

Per passare un numero qualsiasi di argomenti alla funzione bash, si deve inserirli subito dopo il nome della funzione, separati da uno spazio. I parametri passati sono $1, $2, $3 , ecc, dove il numero indica la posizione del parametro dopo il nome della funzione. La variabile $0 è riservata per il nome della funzione.

Ecco una semplice funzione bash che creerà una directory e poi si sposterà in essa:

```bash
mkcd ()
{
  mkdir -p -- "$1" && cd -P -- "$1"
}
```

Come per gli alias, bisogna aggiungere la funzione al file `~/.bashrc` ed eseguire il comando `source ~/.bashrc` per ricaricare il file.

Quindi, invece di utilizzare `mkdir` creare una nuova directory e il comando `cd` spostarsi in quella directory, è possibile digitare:

`mkcd nuova_cartella`

## List all Alias

Per vedere gli alias presenti, basta il seguente comando:

```bash
alias
```

## Alias che potrebbero tornare utili

```bash
## Terminal shortcuts
alias ..="cd ../"
alias ...="cd ../../"
alias ....="cd ../../../"
alias .....="cd ../../../../"
alias ll="ls -alhF --color=auto"
alias lc="ls -l --color=auto"
alias lsd="exa -lha --icons"
alias update="sudo apt update ; sudo apt -y upgrade ; sudo apt -y autoremove ; flatpak upgrade -y; flatpak uninstall --unused -y; sudo pkcon refresh ; sudo pkcon -y update"
alias install="sudo apt install -y"
alias reboot="sudo /sbin/reboot"
alias poweroff="sudo /sbin/poweroff"
alias grep="grep --color=auto"
alias egrep="egrep --color=auto"

## Github shortcuts
alias repo="gh repo create"
alias gi="git init"
alias ga="git add -A"
alias gc="git commit -m"
```

## Collegamenti

- [https://guide.debianizzati.org/index.php/Alias](https://guide.debianizzati.org/index.php/Alias)
- [https://noviello.it/come-creare-alias-bash-su-linux/](https://noviello.it/come-creare-alias-bash-su-linux/)

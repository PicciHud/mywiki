# Personalizzare Bash

Il prompt di default della riga comandi di Linux può essere personalizzato facilmente modificando la variabile di ambiente PS1.

`PS1="[stringa]"`

La composizione della stringa nel comando PS1 determina come apparirà il prompt.

## Come visualizzare la stringa del prompt corrente 

Pe vedere la configurazione attuale del prompt basta digitare sulla riga comandi 

`ECHO $PS1`

Sullo schermo viene visualizzata la stringa corrente, quella che definisce l'aspetto attuale del prompt. Ad esempio:

```bash
echo $PS1
\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$
```

## Le principali opzioni

Le principali opzioni per comporre la stringa sono le seguenti:

- `\d` visualizza la data formato "Giorno-della-settimana Mese Data" ("Tue May 26"); 
- `\H` visualizza il nome dell'host; 
- `\t` visualizza l'ora nel formato a 24 ore ( HH:MM:SS); 
- `\A` visualizza l'ora nel formato a 24 ore ( HH:MM ); 
- `\u` visualizza il nome dell'utente corrente;
- `\w` visualizza il nome della directory corrente o la tilde nel caso della directory $HOME;
- `\$` visualizza il simbolo $ per gli utenti normali o # per l'utente root;
- `\\` visualizza un singolo backslash;
- `\n` il carattere "newline";
- `\r` il carattere "carriage return";
- `\s` il nome della shell, il nome base di $0;
- `\!` il numero cronologico (history number) di questo comando;
- `\[` comuncia una sequenza di caratteri non stampabili, che potrebbero essere usati per inserire una sequenza di controllo del terminale nel prompt;
- `\]` termina la sequenza di caratteri non stampabili.

Si possono anche utilizzare colori, icone e altri elementi grafici utilizzando codici di colore ANSI e icone speciali. Il formato è 

`\[\033[CODICE;COLOREm\]`

## Modificare la variabile $PS1

Per modificare la variabile PS1 si può usare il comando 

`export PS1="nuova stringa di prompt"`

ad esempio 

`export PS1="\u@\h:\w$ "`

visualizzerà il nome dell'utente, il nome del host e la directory corrente.

### Salvare le modifiche

Per rendere permanenti le modifiche alla variabile PS1 si può aggiungere il comando export alla fine del file `.bashrc` presente nella home dell'utente.

```bash
# codici dei colori
DEFAULT="\[\033[0m\]"
RED="\[\033[1;31m\]"
GREEN="\[\033[0;32m\]"
BLUE="\[\033[1;34m\]"
EBLACK="\[\033[1;30m\]"
PURPLE="\[\033[35m\]"
YELLOW="\[\033[1;33m\]"
CYAN="\[\033[1;36m\]"

parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}

export PS1="$GREEN[\A \d]$CYAN\u$BLUE@\h $DEFAULT(\!):$YELLOW\w $PURPLE\n\$(parse_git_branch)$DEFAULT > "
```

Riavviare il terminale o eseguire il comando `source ~/.bashrc` per rendere effettive le modifiche.

La funzione `parse_git_branch` è una funzione personalizzata che consente di visualizzare il nome della branch corrente del repository git sulla riga di comando. 

In particolare:

- `git branch 2> /dev/null`: esegue il comando git branch e reindirizza l'output di errore in `/dev/null`, in modo che non venga visualizzato alcun messaggio di errore se non si è in un repository git;
- `sed -e '/^[^*]/d'`: utilizza sed per eliminare tutte le righe che non iniziano con un asterisco;
- `-e 's/* \(.*\)/\1/'`: utilizza sed per sostituire la riga restante con solo il contenuto dopo l'asterisco (che è il nome della branch corrente).

La funzione utilizzando questi comandi, restituirà solo il nome della branch corrente del repository git.

```bash
[23:05 lun gen 16]davide@laptop (2000):~/Documenti/Appunti.wiki
master >
```

## Come avere una lunghezza indefinita della history

```bash
# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=-1
HISTFILESIZE=-1
```

Basta impostare a `-1` questi valori nel file `.bashrc`.

## Ricerca nella history

Per ricercare aventi e indietro nella cronologia dei comandi, mentre questi vengono digitati, tramite i tasti PAGUP e PAGDOWN, aggiungere il seguente testo nel file `~/.inputrc`:

```bash
"\e[5~": history-search-backward   
"\e[6~": history-search-forward 
```

I tasti `\e[5` e `\e[6` sono proprio PAGUP e PAGDOWN.

In ogni caso, la pressione dei tasti CTRL+R permette di effettuare la ricerca ricorsiva dei comandi precedentemente digitati.

### Hstr

`hstr` è un comando che aiuta a navigare in maniera più efficace nella history. Per installarlo basta digitare questo semplice comando:

```bash
sudo apt install hstr
```

Ora, con la combinazione `CTRL+r` si accederà alla ricerca dei comandi.

Hstr può essere configurato per fornire suggerimenti di ricerca durante la digitazione, simile ai suggerimenti che vengono visualizzati in genere in un browser Web e in altre applicazioni.

Suggerisce i comandi utilizzati più spesso e li mantiene in cima alla lista. È inoltre possibile aggiungere manualmente i comandi ai preferiti per un accesso facile e veloce.

Aggiungere il testo seguente al file `.bashrc` per la configurazione: 

```bash
# HSTR configuration - add this to ~/.bashrc
 
 alias hh=hstr                    # hh to be alias for hstr
 export HSTR_CONFIG=monochromatic # get more colors
 shopt -s histappend              # append new history items to .bash_history
 export HISTCONTROL=ignorespace   # leading space hides commands from history
 export PROMPT_COMMAND="history -a; history -n; ${PROMPT_COMMAND}"
 # if this is interactive shell, then bind hstr to Ctrl-r (for Vi mode check doc)
 if [[ $- =~ .*i.* ]]; then bind '"\C-r": "\C-a hstr -- \C-j"'; fi
 # if this is interactive shell, then bind 'kill last command' to Ctrl-x k
 if [[ $- =~ .*i.* ]]; then bind '"\C-xk": "\C-a hstr -k \C-j"'; fi
```

## bash-completion

```bash
sudo apt install bash-completion
```

Aggiungere quindi le righe sottostanti nel file `~/bashrc`:

```bash
# Use bash-completion, if available
[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
    . /usr/share/bash-completion/bash_completion

# To get first completion and a listing you can add the following to bashrc
bind 'set show-all-if-ambiguous on'
bind 'TAB:menu-complete'
```

Mentre nel file `~/inputrc`:

```bash
"\e[5~": history-search-backward
"\e[6~": history-search-forward
```

in modo da avere il completamento della history coi tasti *pageup* e *pagedown*.

## Collegamenti

- [https://linuxhint.com/bash-ps1-customization/](https://linuxhint.com/bash-ps1-customization/)
- [http://www.megalab.it/5827/8/come-cambiare-il-prompt-di-bash](http://www.megalab.it/5827/8/come-cambiare-il-prompt-di-bash)
- [http://www.pluto.it/sites/default/files/ildp/HOWTO/Bash-Prompt-HOWTO/Bash-Prompt-HOWTO-2.html](http://www.pluto.it/sites/default/files/ildp/HOWTO/Bash-Prompt-HOWTO/Bash-Prompt-HOWTO-2.html)
- [https://linuxhint.com/hstr-command-history-browser-in-linux/](https://linuxhint.com/hstr-command-history-browser-in-linux/)
- [https://github.com/scop/bash-completion](https://github.com/scop/bash-completion)

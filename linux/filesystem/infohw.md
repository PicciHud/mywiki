# Informazioni sul sistema

Ci sono molti comandi per controllare le informazioni sull’hardware di un sistema GNU/Linux. 

| COMANDO                 | ESECUZIONE                                                                                                                                                                                     |
|-------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| lscpu                   | lscpu riporta informazioni sulla CPU e sulle unità di elaborazione. Non ha ulteriori opzioni o funzionalità                                                                                    |
| lshw                    | Riporta informazioni dettagliate e brevi su più unità hardware diverse come CPU, memoria, disco, controller USB, adattatori di rete ecc.                                                       |
| lshw -short             | Come il comando precedente, ma con un output più conciso                                                                                                                                       |
| hwinfo                  | Riporta informazioni dettagliate e brevi su più componenti hardware diversi e più di quanto lshw può riportare                                                                                 |
| lspci                   | Lo strumento lspci viene utilizzato per generare informazioni relative a tutti i controller PCI sul sistema più i dispositivi ad essi collegati                                                |
| lsscsi                  | Elenca i dispositivi scsi/sata come dischi rigidi e unità ottiche                                                                                                                              |
| lsusb                   | Questo comando mostra i controller USB e i dettagli sui dispositivi ad essi collegati                                                                                                          |
| usb-devices             | Informazioni estese sui dispositivi usb                                                                                                                                                        |
| dmidecode -t memory     | Informazioni sulla memoria                                                                                                                                                                     |
| dmidecode -t system     | Informazioni sul sistema                                                                                                                                                                       |
| dmidecode -t bios       | Informazioni sul BIOS                                                                                                                                                                          |
| dmidecode -t processore | Informazioni sul processore                                                                                                                                                                    |
| cat /etc/os-release     | Informazioni sulla versione del sistema operativo                                                                                                                                              |
| iwconfig                | Serve per identificare periferiche di rete wireless                                                                                                                                            |
| xrandr --query          | Il comando xrandr è uno strumento utile per il riconoscimento del monitor integrato e/o connesso al proprio computer                                                                           |
| inxi                    | Lo strumento inxi permette di reperire rapidamente informazioni sia sull'hardware (CPU, RAM, dischi rigidi ecc.) sia sul sistema installato (kernel, server grafico, Desktop, repository ecc.) |
| inxi -b                 | Mostra le informazioni di base sul sistema                                                                                                                                                     |
| inxi -F                 | Mostra le informazioni complete sul sistema                                                                                                                                                    |
| inxi -M                 | Mostra le informazioni relative a marca e modello della scheda madre                                                                                                                           |
| inxi -C                 | Mostra le informazioni sulle CPU                                                                                                                                                               |
| inxi -B                 | Mostra le informazioni relative alla batteria                                                                                                                                                  |
| uname -a                | Informazioni sul SO                                                                                                                                                                            |
| neofeth                 | Varie informazioni sul sistema                                                                                                                                                                 |
| free -m                 | Informazioni sulla RAM                                                                                                                                                                         |
| lsb_release -a          | Informazioni sulla versione del SO                                                                                                                                                             |
| hostnamectl             | Varie informazioni sul sistema                                                                                                                                                                 |

## Collegamenti

- [https://trgtkls.org/linux-comandi-per-avere-informazioni-sul-sistema/](https://trgtkls.org/linux-comandi-per-avere-informazioni-sul-sistema/)
- [https://wiki.ubuntu-it.org/Hardware/Periferiche/IdentificarePeriferiche](https://wiki.ubuntu-it.org/Hardware/Periferiche/IdentificarePeriferiche)
- [https://www.guidetti-informatica.net/2021/09/10-comandi-per-raccogliere-informazioni-di-sistema-e-hardware-in-linux/](https://www.guidetti-informatica.net/2021/09/10-comandi-per-raccogliere-informazioni-di-sistema-e-hardware-in-linux/)
- [https://www.redhat.com/sysadmin/linux-system-info-commands](https://www.redhat.com/sysadmin/linux-system-info-commands)
- [https://linuxize.com/post/how-to-check-linux-version/](https://linuxize.com/post/how-to-check-linux-version/)

# Megabit

Quando si fa riferimento alla capacità di un collegamento dati e, quindi, alla velocità massima per il trasferimento dei dati, come unità di misura si utilizzano i 
Megabit per secondo o Mbps (oppure Mb/s o Mbit/s).

Megabit e MegaByte sono due unità di misura completamente diverse. Il Megabit è una delle unità di misura del trasferimento dei dati o della velocità di download. 
Il MegaByte e i suoi multipli, invece, sono usati per indicare la quantità di dati scaricati.

Questi ultimi vengono spesso usati, inoltre, anche per indicare la quantità di dati (o di informazioni) che un dispositivo può riuscire a contenere. Ne sono classici esempi, le chiavette USB o gli hard disk esterni.

Il Megabit per secondo è un multiplo del bit e corrisponde alla trasmissione di 1.000.000 di bit al secondo. A seconda della capacità del collegamento con cui si ha a che fare si 
possono però usare altri multipli del bit: kbps (1.000 bit al secondo) o Gbps (1.000.000.000 bit al secondo).

I Gigabit per secondo o Gbps si utilizzano per riferirsi alla capacità delle più moderne connessioni in fibra FTTH (Fiber-to-the-Home)

## Differenza tra Megabit e Megabyte

Il Megabyte è un'unità di misura dell'informazione o della quantità di dati, uno dei vari multipli del byte.

Mentre per misurare la capacità del link si usano kbps, Mbps e Gbps, quando ad esempio si scarica un file con il browser, la velocità di download è espressa in byte per secondo (KB/s o MB/s).

Ciò è corretto perché appunto, in questo caso, si misura l'informazione trasferita nell'unità di tempo con riferimento - ad esempio - al peso di un file.

Per codificare un singolo carattere alfanumerico si utilizzano 8 bit. Il byte, che esprime una sequenza di 8 bit, è quindi 
storicamente diventato l'elemento base nelle architetture dei computer e l'unità di misura delle capacità di memoria.

Dal momento che il byte è formato da una sequenza di 8 bit, questo può assumere 256 possibili valori (ovvero 28). La famosa tabella dei caratteri 
ASCII è formata proprio da 256 voci, ciascuna corrispondente a uno dei caratteri che è possibile formare con una diversi combinazione degli 8 bit che compongono il byte.

Per passare dai bit ai byte, quindi, basta semplicemente dividere per 8.

Una connessione con capacità di 20 Mbps può trasferire fino 2,5 MB/s (20 diviso 8) ovvero 2.500 KB/s; una connessione da 100 Mbps fino a 12,5 MB/s.

I Megabyte al secondo (MB/s) danno immediatamente un'idea della quantità di informazione che è possibile trasferire in un secondo: se si potessero usare tutti i 100 Mbps 
di un link in fibra, si potrebbe prelevare dalla rete un documento di 12,5 Megabyte in un solo secondo; con un collegamento in fibra FTTH 
a 1 Gbps, un file da 125 Megabyte in un appena in secondo.

La distinzione tra Megabyte Mb è MegaByte MB sta tutta nella lettera b, che è minuscola se si parla di megabit al secondo (Mbps) e maiuscola se si parla di Megabyte (MBps).

## Collegamenti

- [https://www.ilsoftware.it/articoli.asp?tag=Differenza-tra-Megabit-e-Megabyte-come-non-cadere-in-errore_16910](https://www.ilsoftware.it/articoli.asp?tag=Differenza-tra-Megabit-e-Megabyte-come-non-cadere-in-errore_16910)
- [https://selectra.net/internet/guida/tecnologia/mbps-megabyte](https://selectra.net/internet/guida/tecnologia/mbps-megabyte)

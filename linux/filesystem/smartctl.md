# Smartctl

Gli `smartmontools` permettono di usare la funzionalità SMART di tutti i moderni HD grazie alla quale è possibile diagnosticare malfunzionamenti.

## Installazione

```bash
sudo apt install smartmontools
```
## Informazioni

Per visualizzare alcune informazioni generiche sul HD:

```bash
sudo smartctl -i /dev/nvme0

smartctl 7.3 2022-02-28 r5338 [x86_64-linux-6.1.0-6-amd64] (local build)
Copyright (C) 2002-22, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Number:                       Samsung SSD 980 500GB
Serial Number:                      S64DNL0TA11496W
Firmware Version:                   2B4QFXO7
PCI Vendor/Subsystem ID:            0x144d
IEEE OUI Identifier:                0x002538
Total NVM Capacity:                 500.107.862.016 [500 GB]
Unallocated NVM Capacity:           0
Controller ID:                      5
NVMe Version:                       1.4
Number of Namespaces:               1
Namespace 1 Size/Capacity:          500.107.862.016 [500 GB]
Namespace 1 Utilization:            275.385.454.592 [275 GB]
Namespace 1 Formatted LBA Size:     512
Namespace 1 IEEE EUI-64:            002538 da21a0c231
Local Time is:                      Sat Mar 25 18:41:22 2023 CET

sudo smartctl -i /dev/sda1

smartctl 7.3 2022-02-28 r5338 [x86_64-linux-6.1.0-6-amd64] (local build)
Copyright (C) 2002-22, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Vendor:               Intenso
Product:              Speed Line
Revision:             8.01
Compliance:           SPC-4
User Capacity:        251.658.240.000 bytes [251 GB]
Logical block size:   512 bytes
Logical Unit id:      0x4945450005080f000x200049454505080f
Serial number:        
Device type:          disk
Local Time is:        Sat Mar 25 18:44:18 2023 CET
SMART support is:     Available - device has SMART capability.
SMART support is:     Enabled
Temperature Warning:  Disabled or Not Supported
```

Oltre alle informazioni generiche, dalle ultime due righe si capisce che l'HD supporta la tecnologia SMART e che il supporto è attivato. Se non fosse attivato basterebbe digitare questo comando:

```bash
sudo smartctl -s on /dev/hda
```
per attivare il supporto SMART.

Per controllare lo stato di salute attuale: 

```bash
sudo smartctl -H /dev/hda

smartctl version 5.34 [i686-pc-linux-gnu] Copyright (C) 2002-5 Bruce Allen
Home page is https://www.smartmontools.org/

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED
```
L'ultima riga dice che la salute sembra buona e nessuno dei parametri interni controllati da SMART ha superato il livello di guardia. 

> Se il precedente comando non riporta PASSED smontaire immediatamente tutte le partizioni presenti su quell'HD ed effettuate un backup dei dati 

## Maggiori informazioni

```bash
smartctl -a /dev/sda1
```
L'output, abbastanza lungo (`-a` sta per "all"), è diviso in quattro sezioni. Il primo blocco rappresenta le informazioni generiche sull'HD (le stesse ottenute prima con `-i`),
la seconda sezione riporta le informazioni sul supporto SMART. La terza sezione elenca i parametri interni monitorati da SMART e se hanno mai superato il livello di guardia:

```bash
SMART Attributes Data Structure revision number: 16
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAG     VALUE WORST THRESH TYPE      UPDATED  WHEN_FAILED RAW_VALUE
  1 Raw_Read_Error_Rate     0x000b   200   200   051    Pre-fail  Always       -       0
  3 Spin_Up_Time            0x0007   099   091   021    Pre-fail  Always       -       4108
  4 Start_Stop_Count        0x0032   098   098   040    Old_age   Always       -       2590
  5 Reallocated_Sector_Ct   0x0033   200   200   140    Pre-fail  Always       -       0
  7 Seek_Error_Rate         0x000b   200   200   051    Pre-fail  Always       -       0
  9 Power_On_Hours          0x0032   092   092   000    Old_age   Always       -       6494
 10 Spin_Retry_Count        0x0013   100   100   051    Pre-fail  Always       -       0
 11 Calibration_Retry_Count 0x0013   100   100   051    Pre-fail  Always       -       0
 12 Power_Cycle_Count       0x0032   098   098   000    Old_age   Always       -       2435
196 Reallocated_Event_Count 0x0032   200   200   000    Old_age   Always       -       0
197 Current_Pending_Sector  0x0012   200   200   000    Old_age   Always       -       0
198 Offline_Uncorrectable   0x0012   200   200   000    Old_age   Always       -       0
199 UDMA_CRC_Error_Count    0x000a   200   200   000    Old_age   Always       -       19
200 Multi_Zone_Error_Rate   0x0009   200   200   051    Pre-fail  Offline      -       0
```

I parametri indicati come `Pre-fail` sono quelli che superano la soglia di guardia nelle 24 ore che precedono la rottura dell'HD, 
mentre quelli `Old_age` sono i parametri che superano la soglia di guardia quando ormai l'HD è vecchio e non è considerato più affidabile dal costruttore. 
Nell'esempio si vede che nessun parametro ha mai superato la soglia di guardia. 

## Effettuare test

È possibile effettuare dei test più o meno approfonditi sul disco. Alcuni test si possono effettuare con l'HD montato e funzionante, ed il test stesso avrà un impatto minimo o nullo
sulle prestazioni del sistema.

Per effettuare un test:

```bash
sudo smartctl -t tipo_test /dev/hda
```
dove *tipo_test* può essere:

- short: effettua un test sul disco di durata inferiore a 10 minuti, può essere eseguito durante il normale funzionamento e non ha alcun impatto sulle prestazioni. 
Questo test controlla le performance meccaniche ed elettriche del disco, oltre che le performance in lettura;

- long: effettua un test di durata da 40 minuti ad un ora (a seconda del disco). Può essere effettuato durante il normale funzionamento del disco e non ha impatto sulle prestazioni. 
Questo test è una versione più estesa dello short test;

- conveyance: effettua un test di alcuni minuti atto a scoprire difetti dovuti ad incurie nel trasporto dell'HD. Può essere eseguito durante il normale funzionamento dell'HD.

I risultati di questi test vengono riportati nella parte finale dell'output di `smartctl -a /dev/sda1`.

```bash
SMART Error Log Version: 1
No Errors Logged

SMART Self-test log structure revision number 1
Num  Test_Description    Status                  Remaining  LifeTime(hours)  LBA_of_first_error
# 1  Short offline       Completed without error       00%       952         -
# 2  Conveyance offline  Completed without error       00%       951         -
# 3  Short offline       Completed without error       00%       951         -
# 4  Short offline       Completed without error       00%       875         -
```

Nell'esempio si può vedere che sono stati effettuati 4 test, di cui tre di tipo short e uno di tipo conveyance. Nessuno di loro ha dato esito positivo (cioè non sono stati rilevati malfunzionamenti).

### Testare la reale dimensione di un dispositivo

```bash
sudo apt install f3

sudo f3probe --desrtuctive --time-ops /dev/sdX
```
Ad esempio:

```bash
sudo f3probe --destructive --time-ops /dev/sda

F3 probe 8.0
Copyright (C) 2010 Digirati Internet LTDA.
This is free software; see the source for copying conditions.

WARNING: Probing normally takes from a few seconds to 15 minutes, but
         it can take longer. Please be patient.

Good news: The device `/dev/sda' is the real thing

Device geometry:
                 *Usable* size: 234.38 GB (491520000 blocks)
                Announced size: 234.38 GB (491520000 blocks)
                        Module: 256.00 GB (2^38 Bytes)
        Approximate cache size: 0.00 Byte (0 blocks), need-reset=no
           Physical block size: 512.00 Byte (2^9 Bytes)

Probe time: 3'23"
 Operation: total time / count = avg time
      Read: 1.27s / 4818 = 265us
     Write: 3'21" / 4192321 = 48us
     Reset: 0us / 1 = 0us
```

### Testare la velocità del disco

Il comando `hdparm` può essere utilizzato per misurare le prestazioni del disco rigido, inclusa la velocità di lettura e scrittura reale e la velocità della cache.


Ad esempio, per misurare le prestazioni del disco rigido, è possibile utilizzare il seguente comando:

```bash
hdparm -t /dev/sda


sudo hdparm -t /dev/nvme0n1

/dev/nvme0n1:
 Timing buffered disk reads: 3390 MB in  3.00 seconds = 1129.60 MB/sec
```
Con l'opzione `-T` viene testata anche la cache:

```bash
sudo hdparm -Tt /dev/nvme0n1

/dev/nvme0n1:
 Timing cached reads:   19464 MB in  2.00 seconds = 9748.45 MB/sec
 Timing buffered disk reads: 3446 MB in  3.00 seconds = 1148.21 MB/sec
```

## Collegamenti

- [https://guide.debianizzati.org/index.php/Gestire_gli_HD:_stato_di_salute,_badblocks_e_ripristino_dati](https://guide.debianizzati.org/index.php/Gestire_gli_HD:_stato_di_salute,_badblocks_e_ripristino_dati)
- [https://github.com/AltraMayor/f3](https://github.com/AltraMayor/f3)


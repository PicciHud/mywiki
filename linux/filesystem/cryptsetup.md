# cryptsetup

Per prima cosa, individuare il disco e creare una partizione. Una volta creata, è possibile crittografare la partizione. Ad esempio, se fosse stata creata la partizione `sda1`:

```bash
#Creare il file con la passphrase
touch ~/.keyfile

sudo cryptsetup luksFormat /dev/sda1 ~/.keyfile
```

Ora che la partizione è stata crittografata, è possibile aprirla e montarla coi seguenti comandi:

```bash
sudo cryptsetup open /dev/sda1 Backup --key-file ~/.keyfile
udisksctl mount -b /dev/mapper/Backup
```

dove `Backup` indica il nome arbitrario associato al device.

## Creazione del file-system

Una volta aperta, è possibile creare il file-system:

```bash
sudo mkfs.ext4 -v -L "Backup" /dev/mapper/Backup
```

Infine, si volesse chiudere la partizione, ovvero ri-crittografarla:

```bash
sudo cryptsetup close Backup
```

## Auto mount

Per far sì che il dispositivo venga montato in automatico all'avvio, inserire nel file `/etc/crypttab` la seguente riga:

```bash
#volume-name encrypted-device key-file options

Backup UUID=4936592c-83f8-4468-94b0-a0645ff2153a /home/dado/.keyfile luks
```
Infine, nel file `/etc/fstab`:

```bash
/dev/mapper/Backup /media/dado/Backup ext4 defaults 0 2
```
Fatto questo, per testare che tutto sia scritto correttamente:

```bash
sudo cryptdisks_start Backup
```

Il disco ora dovrebbe essere correttamente montato.

## Collegamenti

- [https://opensource.com/article/21/3/encryption-luks](https://opensource.com/article/21/3/encryption-luks)
- [https://linuxconfig.org/introduction-to-crypttab-with-examples](https://linuxconfig.org/introduction-to-crypttab-with-examples)
- [https://man.archlinux.org/man/crypttab.5.en](https://man.archlinux.org/man/crypttab.5.en)
- [https://linuxhint.com/encrypt-storage-drives-using-luks-linux/](https://linuxhint.com/encrypt-storage-drives-using-luks-linux/)
- [https://linuxhint.com/encrypt-data-usb-linux/](https://linuxhint.com/encrypt-data-usb-linux/)
- [https://www.lffl.org/2021/04/guida-luks-criptare-disco-usb.html](https://www.lffl.org/2021/04/guida-luks-criptare-disco-usb.html)

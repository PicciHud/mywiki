# mount

Sui sistemi operativi GNU/Linux e UNIX, è possibile utilizzare il comando `mount` per montare file-system e dispositivi rimovibili.

Il *montaggio* (mounting) è un processo attraverso il quale un file-system si rende disponibile al sistema; dopo il montaggio i file saranno accessibili sotto il punto di mount. 

La directory su cui è montato il file-system può non essere vuota, ma deve esistere.

Su molti sistemi ci sono file-system che devono essere montati automaticamente all'avvio. Questi sono specificati nel file `/etc/fstab`.

## Elencare i file-system montati

Se utilizzato senza alcun argomento, il comando mount visualizzerà tutti i file-system attualmente montati sul sistema:

```bash
mount
```

Ogni riga contiene informazioni sul nome del dispositivo, la directory in cui è montato , il tipo di file-system e le opzioni di mount nel seguente formato:

```bash
device_name on directory type filesystem_type (options)
```
Per visualizzare solo determinati file-system utilizzare l'opzione `-t`.

Ad esempio, per stampare solo le partizioni ext4:

```bash
mount -t ext4

/dev/mapper/luks-487ce502-4541-488c-b07f-ea2ac4e91b9f on / type ext4 (rw,noatime,stripe=32)
```

Altri modi per elencare i file-system montati:

```bash
cat /proc/mounts

df -aTh

File system    Tipo         Dim. Usati Dispon. Uso% Montato su
sysfs          sysfs           0     0       0    - /sys
proc           proc            0     0       0    - /proc
udev           devtmpfs     6,8G     0    6,8G   0% /dev
devpts         devpts          0     0       0    - /dev/pts
tmpfs          tmpfs        1,4G  2,2M    1,4G   1% /run
/dev/dm-0      ext4         426G  109G    296G  27% /
securityfs     securityfs      0     0       0    - /sys/kernel/security
tmpfs          tmpfs        6,8G  6,6M    6,8G   1% /dev/shm
tmpfs          tmpfs        5,0M   16K    5,0M   1% /run/lock
cgroup2        cgroup2         0     0       0    - /sys/fs/cgroup
pstore         pstore          0     0       0    - /sys/fs/pstore
bpf            bpf             0     0       0    - /sys/fs/bpf
systemd-1      -               -     -       -    - /proc/sys/fs/binfmt_misc
mqueue         mqueue          0     0       0    - /dev/mqueue
hugetlbfs      hugetlbfs       0     0       0    - /dev/hugepages
debugfs        debugfs         0     0       0    - /sys/kernel/debug
tracefs        tracefs         0     0       0    - /sys/kernel/tracing
configfs       configfs        0     0       0    - /sys/kernel/config
fusectl        fusectl         0     0       0    - /sys/fs/fuse/connections
ramfs          ramfs           0     0       0    - /run/credentials/systemd-sysusers.service
ramfs          ramfs           0     0       0    - /run/credentials/systemd-tmpfiles-setup-dev.service
ramfs          ramfs           0     0       0    - /run/credentials/systemd-tmpfiles-setup.service
ramfs          ramfs           0     0       0    - /run/credentials/systemd-sysctl.service
binfmt_misc    binfmt_misc     0     0       0    - /proc/sys/fs/binfmt_misc
tmpfs          tmpfs        1,4G  2,6M    1,4G   1% /run/user/1000
```
dove con l'opzione `-a` tutti i file-system saranno visibili e con `-T` ne sarà indicato il tipo.

## Montare un file-system

Per montare un fyle-system in una determinata posizione (punto di *montaggio*), utilizzare il comando mount nel seguente formato:

```bash
mount [OPTION...] DEVICE_NAME DIRECTORY
```

Ad esempio, per montare il fyle-system /dev/sdb1 nella directory /mnt/media:

```bash
sudo mount /dev/sdb1 /mnt/media
```

Di solito quando si monta un dispositivo con un fyle-system comune, come ext4, il comando rileverà automaticamente il tipo di fyle-system. 
Tuttavia, alcuni fyle-system non sono riconosciuti e devono essere specificati in modo esplicito con l'opzione `-t`:

```bash
mount -t TYPE DEVICE_NAME DIRECTORY
```

Se non si deve scrivere nulla sul file-system è meglio usare l'opzione `-r` (read) per avere un file-system *in sola lettura*; 
in questo modo il kernel interromperà qualsiasi tentativo di scrittura.

### Montare un'unità USB

Per montare manualmente un dispositivo USB, attenersi alla seguente procedura:

- creare il punto di montaggio:

```bash
sudo mkdir -p /media/usb
```

- montare la periferica:

```bash
sudo mount /dev/sdd1 /media/usb
```

## Smontaggio di un fyle-system

Per scollegare un fyle-system montato, utilizzare il comando `umount` seguito dalla directory in cui è stato montato o dal nome del dispositivo:

```bash
umount DIRECTORY

umount DEVICE_NAME
```

Se il fyle-system è in uso, il comando umount non riuscirà a smontarlo.

Utilizzare l'opzione `-l` per smontare un fyle-system occupato non appena diventa libero:

```bash
umount -l DIRECTORY
```

## Collegamenti

- [https://noviello.it/come-montare-mount-e-smontare-unmount-file-system-su-linux/](https://noviello.it/come-montare-mount-e-smontare-unmount-file-system-su-linux/)
- [https://linuxhandbook.com/list-mounted-drives/](https://linuxhandbook.com/list-mounted-drives/)
- [http://www.pluto.it/sites/default/files/ildp/guide/lfh/x1111.html](http://www.pluto.it/sites/default/files/ildp/guide/lfh/x1111.html)

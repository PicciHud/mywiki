# Raid

## Concetti di base 

Un dispositivo RAID può essere configurato in diversi modi. A seconda della configurazione può essere classificato in dieci diversi livelli. Alcuni concetti:

**Hot Spare** – Questo è il disco aggiuntivo nell’array RAID. Se un disco si guasta, i dati dal disco difettoso saranno migrati in questo disco di riserva automaticamente.

**Mirroring** – Se questa caratteristica è abilitata, una copia degli stessi dati sarà salvata anche in un altro disco. 

**Striping** – Se questa funzione è abilitata, i dati saranno scritti in tutti i dischi disponibili in modo casuale. È proprio come condividere i dati tra tutti i dischi, così tutti si riempiono allo stesso modo.

**Parità** – Questo è il metodo di rigenerare i dati persi dalle informazioni di parità salvate.

Diversi livelli RAID sono definiti in base a come sono richiesti il mirroring e lo stripping. 

### RAID 0

Questo livello fornisce lo striping senza parità. Dal momento che non memorizza alcun dato di parità ed esegue operazioni di lettura e scrittura simultaneamente, la velocità e' molto più veloce di altri livelli. Questo livello richiede almeno due dischi. Qualsiasi guasto di un singolo disco dall’array risulterà in una perdita totale di dati.

### RAID 1

Questo livello fornisce parità senza striping. Scrive tutti i dati su due dischi. Se un disco è guasto o rimosso, tutti i dati sono ancora presenti sull’altro disco. Il primo disco rigido memorizza i dati originali mentre l’altro disco memorizza la copia esatta del primo disco. Poiché i dati vengono scritti due volte, le prestazioni saranno ridotte. 

### RAID 5

Questo livello fornisce sia parità che striping. Richiede almeno tre dischi. Scrive i dati di parità ugualmente in tutti i dischi. Se un disco è guasto, i dati possono essere ricostruiti dai dati di parità disponibili sui dischi rimanenti.

## Installazione

```bash
sudo apt install mdadm
```
## Creazione array Raid 1

```bash
sudo mdadm --create --verbose /dev/md1 --level=1 --raid-devices=2 /dev/sda1 /dev/sdb1
```

Nel caso di device crittografati, ecco un esempio:

```bash
sudo mdadm --create --verbose /dev/md1 --level=1 --raid-devices=2 /dev/mapper/device1 /dev/mapper/device2
```

## Creazione del file system

```bash
sudo mkfs.ext4 -v -L "RAID1" /dev/md1 
```

## Montaggio automatico

```bash
> cat /etc/fstab
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
/dev/md1 /media/dado/Backup ext4 defaults 0 2
```

Per vederne i dettagli

```bash
sudo mdadm --detail /dev/md1
```

Il risultato finale:

```bash
> lsblk -f
NAME            FSTYPE         FSVER    LABEL       UUID                                   FSAVAIL FSUSE% MOUNTPOINTS
sda                                                                                                       
└─sda1          crypto_LUKS    2                    df755df0-00b4-44e0-8edb-90d075242829                  
  └─BackupDR    linux_raid_mem 1.2      pc:1        8d0dc841-6ecd-869a-1dc8-476ddd8023bb                  
    └─md1       ext4           1.0      BackupRAID1 201d3518-e320-4f21-a080-4a35bc98cae6    361,8G    12% /media/dado/Backup
sdb                                                                                                       
└─sdb1          crypto_LUKS    2                    4936592c-83f8-4468-94b0-a0645ff2153a                  
  └─Backup      linux_raid_mem 1.2      pc:1        8d0dc841-6ecd-869a-1dc8-476ddd8023bb                  
    └─md1       ext4           1.0      BackupRAID1 201d3518-e320-4f21-a080-4a35bc98cae6    361,8G    12% /media/dado
```

# Collegamenti

- [https://it.linux-console.net/?p=2353](https://it.linux-console.net/?p=2353)
- [https://www.digitalocean.com/community/tutorials/how-to-create-raid-arrays-with-mdadm-on-ubuntu-22-04](https://www.digitalocean.com/community/tutorials/how-to-create-raid-arrays-with-mdadm-on-ubuntu-22-04)
- [https://reefrecovery.org/it/come-configurare-il-raid-in-linux-guida-passo-dopo-passo/](https://reefrecovery.org/it/come-configurare-il-raid-in-linux-guida-passo-dopo-passo/)

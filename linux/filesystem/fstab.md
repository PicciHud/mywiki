# fstab

Il file di configurazione `/etc/fstab` contiene le informazioni necessarie al montaggio delle periferiche di memorizzazione del sistema. 
Tale file viene letto all'avvio del sistema per determinare quali opzioni utilizzare per montare una specifica periferica o una partizione.

![fstab](./asset/fstab/fstab.jpg)

Per visualizzare il contenuto del file, digitare:

```bash
cat /etc/fstab

# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
# /dev/sda1
UUID=12102C02102CEB83 /media/windows  ntfs      silent,umask=0,locale=it_IT.utf8        0       0
# /dev/sda2
UUID=cee15eca-5b2e-48ad-9735-eae5ac14bc90 none swap sw 0 0
# /dev/sda3
UUID=98E0-6D24 /media/dati vfat defaults,utf8,umask=007,uid=0,gid=46,auto,rw,nouser 0 0
# /dev/sda4
UUID=0aa86c61-0df9-4f1a-8b0b-34abbee6b769 / ext3 nouser,defaults,errors=remount-ro,atime,auto,rw,dev,exec,suid 0 0
/dev/sr0        /media/cdrom0   udf,iso9660 user,noauto     0       0
```

Le periferiche vengono identificate dal proprio *Universally Unified IDentifier* o [UUID](./uuid.md). Quest'ultimo è un particolare codice identificativo, virtualmente univoco, che viene assegnato dal programma di generazione del filesystem, quando la partizione viene creata o formattata. 

## Parametri

| Campo | Descrizione
|----|----|
file system | indica il dispositivo
mount point | indica la directory (o mount point) dalla quale sarà possibile accedere al contenuto del dispositivo (per la swap non è richiesto il mount point)
type | tipo di file system
options | opzioni di accesso al dispositivo (per maggiori informazioni consultare la tabella successiva)
dump | attiva/disattiva il backup del filesystem (comando dump)
pass | attiva/disattiva il controllo di coerenza del disco (comando fsck) i valori possibili sono 0, 1 e 2. In genere si dovrebbe usare 1 per il filesystem di root, 2 per tutti gli altri filesystem montati normalmente e 0 per i filesystem che non sono montati di default.

### Opzioni

| Opzioni | Descrizione
|----|----|
auto | la partizione sarà montata all'avvio del sistema, oppure con il comando mount -a
noauto | la partizione può essere montata solo manualmente
defaults | assegna le impostazioni di default: per ext4 sono "rw,suid,dev,exec,auto,nouser,async"
exec | abilita l'esecuzione dei programmi presenti sulla partizione
noexec | inibisce l'esecuzione programmi presenti sulla partizione
relatime | aggiorna nell'inode solo i tempi di accesso al file system
noatime | non aggiorna l'inode con i tempi di accesso al file system
nodiratime | non aggiorna l'inode delle directory coi tempi di accesso al file system
ro | il mount della partizione può avvenire in sola lettura
rw | il mount della partizione può avvenire in lettura e scrittura
sync | operazioni di scrittura/lettura della partizione sincrone
async | operazioni di scrittura/lettura della partizione asincrone
suid | consente le operazioni di suid e sgid (esecuzione dei programmi con maggiori privilegi)
nosuid | inibisce le operazioni di suid e sgid
user | consente a tutti gli utenti di montare la partizione, con le opzioni di default:noexec,nosuid,nodev
users | permette agli utenti appartenenti al gruppo users di montare il filesystem
nouser | limita solo a root la possibilità di effettuare il mount del file system
owner | permette il mount al solo proprietario del punto di mount
nofail | da usarsi per dispositivi esterni (chiavette, dischi, fotocamere, ecc.) per evitare di avere messaggi di errore al boot
dev | interpreta le periferiche a blocchi o periferiche speciali all'interno del filesystem
nodev | impedisce l'interpretazione di periferiche a blocchi o periferiche speciali all'interno del filesystem
uid | imposta l'ID utente che verrà utilizzato per accedere al file system 
gid | imposta l'ID del gruppo che verrà utilizzato per accedere al file system
x-gvfs-show | controlla se il file system viene visualizzato nell'interfaccia utente
umask | rappresenta la negazione dei permessi. In origine tutti i file e le directory hanno il permesso 0777. La maschera li riduce. Ad esempio, una maschera 0111 riduce i permessi da 0777 a 0666. In pratica, suggerisce ai programmi avviati successivamente quali permessi negare al momento della creazione di nuovi file e directory. Differisce da chmod sia perché quest'ultimo modifica i permessi successivamente alla creazione dei file o directory, sia perché i permessi forniti con chmod sono quelli effettivi e non quelli negati. 


Per verificare di aver scritto tutto correttamente, provare il comando:

```bash
mount /punto/di/mount/in/fstab
```
Se non vengono restituiti errori, l'operazione è andata a buon fine.

## Collegamenti

- [http://linuxguide.altervista.org/fstab.html](http://linuxguide.altervista.org/fstab.html)
- [https://wiki.ubuntu-it.org/AmministrazioneSistema/Fstab](https://wiki.ubuntu-it.org/AmministrazioneSistema/Fstab)
- [https://www.andreaminini.com/linux/maschera-permessi-linux-umask](https://www.andreaminini.com/linux/maschera-permessi-linux-umask)
- [https://it.wikipedia.org/wiki/Umask](https://it.wikipedia.org/wiki/Umask)

# UUID

L' UUID è un identificatore univoco universale (Universally Unique IDentifier), consistente in una sequenza di 128 bit, espressi usualmente in forma esadecimale.

Gli UUID sono utilizzati comunemente per identificare le partizioni di un supporto di memoria di massa. 

## Ottenere l'uuid

Esistono diversi modi:

```bash
ls -l /dev/disk/by-uuid/

totale 0
lrwxrwxrwx 1 root root 15 19 mar 12.16 44d72477-5d0f-4d0d-a54b-f721a7e5a320 -> ../../nvme0n1p2
lrwxrwxrwx 1 root root 15 19 mar 12.16 487ce502-4541-488c-b07f-ea2ac4e91b9f -> ../../nvme0n1p1
lrwxrwxrwx 1 root root 10 19 mar 12.16 cd370d97-dd7c-41d1-9b53-1504c89f64fd -> ../../dm-1
lrwxrwxrwx 1 root root 10 19 mar 12.16 ce884a78-4404-4ea9-b6e7-d543bfb38a41 -> ../../dm-0
```

Oppure tramite il comando `blkid`.

Infine tramite `lsblk` con l'opzione `-f` è possibile visualizzare l'uuid e altre informazioni:

```bash
lsblk -f

NAME                                          FSTYPE      FSVER LABEL UUID                                 FSAVAIL FSUSE% MOUNTPOINTS
nvme0n1                                                                                                                   
├─nvme0n1p1                                   crypto_LUKS 1           487ce502-4541-488c-b07f-ea2ac4e91b9f                
│ └─luks-487ce502-4541-488c-b07f-ea2ac4e91b9f ext4        1.0         ce884a78-4404-4ea9-b6e7-d543bfb38a41  295,6G    25% /
└─nvme0n1p2                                   crypto_LUKS 1           44d72477-5d0f-4d0d-a54b-f721a7e5a320                
  └─luks-44d72477-5d0f-4d0d-a54b-f721a7e5a320 swap        1           cd370d97-dd7c-41d1-9b53-1504c89f64fd                [SWAP]
```

## Collegamenti

- [https://guide.debianizzati.org/index.php/UUID](https://guide.debianizzati.org/index.php/UUID)
- [https://linuxhandbook.com/get-uuid-disk/](https://linuxhandbook.com/get-uuid-disk/)

# dd

`dd` è un comando che serve a copiare `blocchi di dati` grezzi da una sorgente verso una destinazione. 
Il concetto è molto differente dalla semplice copia, infatti agisce direttamente a basso livello, cioé bit per bit.

Normalmente questo strumento è utilizzato per le seguenti motivazioni:

- formattazione a basso livello;
- backup di intere partizioni;
- creazione di dischi di avvio.

## Sintassi

La sintassi base di dd è la seguente:

```bash
dd if=FILEINPUT of=FILEOUTPUT
```

È doverosa qui una precisazione: su un sistema UNIX `qualunque cosa è un file`, anche un drive esterno.

## Specificare la dimensione dei blocchi

Quando si scrive a basso livello è bene ricordarsi che i byte scritti vengono sempre inviati nel file di uscita a blocchi.

Per specificare la dimensione di questi blocchi vi sono essenzialmente tre possibili parametri:

- `ibs`= numero di byte per la dimensione del blocco in ingresso;
- `obs`= numero di byte per la dimensione del blocco in uscita;
- `bs`= numero di byte per la dimensione di entrambi i blocchi

Il valore default di questi parametri è 512.

Si possono specificare delle unità di misura, come Kilo (K), Mega (M), Giga (G) e così via…

Ad esempio per trasferire 8 Mega alla volta:

```bash
dd if=nomefile of=nomefile bs=8M
```

### Numero blocchi trasferiti

Si può inserire il numero di blocchi che devono essere trasferiti con il parametro `count`. Ad esempio per trasferire 4MB bisogna scrivere:

```bash
dd if=nomefile of=nomefile count=8000 iflag=fullblock
```

> Ad esempio, per trasferire 2GiB in blocchi da 8M, sono necessari $2048/8 = 256$ blocchi

Il flag `iflag=fullblock` assicura che vengano trasferiti i blocchi nella dimensione intera.

## Visualizzare lo stato del comando

Il flag `status=progress` consente di visualizzare lo stato di avanzamento del comando:

```bash
dd if=FILEINPUT of=FILEOUTPUT status=progress
```

## Esempi d’uso

Per individuare una periferica di archiviazione attaccata utilizziare il comando:

```bash
blkid
```

Un output tipo potrebbe essere:

```bash
/dev/sda1: UUID="b4c1041e-7027-4619-aaa5-900e24c1fdee" BLOCK_SIZE="4096" TYPE="ext4"
/dev/sda2: UUID="dc1014fd-6c84-4dcd-acc3-13e914685136" TYPE="swap"
/dev/sdb1: UUID="3255683f-53a2-4fdf-91cf-b4c1041e2a62" BLOCK_SIZE="4096" TYPE="ext4"
```

Quello sull’estrema sinistra è un identificativo che rappresenta una particolare partizione del disco, nel primo caso è /dev/sda1 e nell’ultimo /dev/sdb1. 
La lettera rappresenta univocamente il disco, mentre il numero la partizione.

Nel caso di nvme e di supporti sd potrebbe cambiare la nomenclatura, ma il concetto è sempre simile.

### Scrittura di un immagine iso

L’utilizzo più famoso è forse quello di scrittura di un immagine iso in una pen-drive. Per prima cosa si deve creare il file-system col comando [mkfs](./mount.md)

Scaricato e individuato il file iso (ad esempio /percorso/file/immagine.iso) bisogna prima individuare il supporto d’uscita, utilizzando il comando `blkid`.

Quindi nel comando dd utilizzare come file di INPUT l’immagine iso e come file di OUTPUT il disco. 
Il block size dovrebbe essere allineato alla capacità di scrittura della pen-drive e della porta usb del pc, affinché sia possibile sfruttare più buffer:

```bash
dd if=/percorso/file/immagine.iso of=/dev/sda status=progress bs=8M
```

### Formattare un dispositivo

Per azzerare un dispositivo viene utilizzato un file virtuale creato da GNU/Linux contenente tutti e soli zeri che si trova nel percorso `/dev/zero`.

Quindi scrivere il comando dd utilizzando come file di INPUT /dev/null, mentre come file di OUTPUT il disco. 

```bash
dd if=/dev/zero of=/dev/sda status=progress bs=8M
```

### Backup

Con dd si può effettuare il backup di interi dischi. 

Quindi utilizzare il disco come parametro di INPUT e un file come parametro di OUTPUT. Il file sarà necessariamente un file IMG (immagine disco).

```bash
dd if=/dev/sda of=/percorso/backup.img status=progress bs=8M
```

Si può poi ripristinare invertendo gli operandi. Si può applicare il concetto anche alle partizioni:

```bash
dd if=/dev/sda1 of=/percorso/backupp1.img status=progress bs=8M
```

#### Backup e compressione

Se non viene specificato alcun file di output, viene utilizzato lo standard output del sistema.
Questo permette di concatenare `dd` con altri comandi, ad esempio gzip, in modo da poter fare un backup che viene compresso al volo:

```bash
dd if=/dev/sda | gzip > /percorso/backup.gz
```
### Clonazione di un disco

`dd` copia bit a bit. Questo significa che se si tenta di duplicare una partizione o un disco, copierà anche lo spazio libero.
Quindi, ad esempio, se si clona un disco rigido con 4 TB, si avrà bisogno di un'unità di destinazione con almeno 4 TB. 

```bash
dd if=/dev/sda of=/dev/sdb status=progress bs=8M conv=noerror,sync
```
Con l'opzione `conv-noerror` eventuali errori saranno ignorati. Il programma proseguirà fino al blocco finale senza fermarsi.
Se utilizzato in combinazione con `noerror`, `sync` assicurerà che eventuali blocchi di dati mancanti vengano automaticamente riempiti con informazioni nulle.

## Collegamenti

- [https://linuxhub.it/articles/howto-usare-dd/](https://linuxhub.it/articles/howto-usare-dd/)
- [https://linuxhandbook.com/dd-command/](https://linuxhandbook.com/dd-command/)

# Formattare un device da terminale

## Identificare la periferica

Per identificare la periferica in questione si possono utilizzare diversi comandi:

```bash
sudo fdisk -l

sudo blkid

df
```
Ecco un esempio:

```bash
sudo blkid

[sudo] password di davide: 
/dev/mapper/luks-44d72477-5d0f-4d0d-a54b-f721a7e5a320: UUID="cd370d97-dd7c-41d1-9b53-1504c89f64fd" TYPE="swap"
/dev/nvme0n1p1: UUID="487ce502-4541-488c-b07f-ea2ac4e91b9f" TYPE="crypto_LUKS" PARTUUID="0559b9a4-01"
/dev/nvme0n1p2: UUID="44d72477-5d0f-4d0d-a54b-f721a7e5a320" TYPE="crypto_LUKS" PARTUUID="0559b9a4-02"
/dev/mapper/luks-487ce502-4541-488c-b07f-ea2ac4e91b9f: UUID="ce884a78-4404-4ea9-b6e7-d543bfb38a41" BLOCK_SIZE="4096" TYPE="ext4"
/dev/sda1: BLOCK_SIZE="2048" UUID="2022-12-17-13-13-38-00" LABEL="d-live nf 11.6.0 kd amd64" TYPE="iso9660" PTUUID="66641822" PTTYPE="dos" PARTUUID="66641822-01"
/dev/sda2: SEC_TYPE="msdos" UUID="DEB0-0001" BLOCK_SIZE="512" TYPE="vfat" PARTUUID="66641822-02"
```
oppure

```bash
df

File system    1K-blocchi     Usati Disponib. Uso% Montato su
udev              7084012         0   7084012   0% /dev
tmpfs             1426036      2380   1423656   1% /run
/dev/dm-0       446217344 113436124 310041144  27% /
tmpfs             7130160      6748   7123412   1% /dev/shm
tmpfs                5120        16      5104   1% /run/lock
tmpfs             1426032       164   1425868   1% /run/user/1000
/dev/sda1         3600960   3600960         0 100% /media/davide/d-live nf 11.6.0 kd amd64
```
La chiavetta USB è identificata in questo caso come `/dev/sda` (/dev/sda1 e /dev/sda2 sono le due partizioni).

Per il comando `blkid` e sull'uuid vedere [qui](./uuid.md)

## Partizionare un disco con fdisk

Il programma più comune per partizionare un disco utilizzando la shell è `fdisk`. Il comando fdisk chiederà di specificare come argomento il nome del disco fisso da partizionare:

```bash
fdisk /dev/hdb
```

Nell'elenco che segue sono riportati i comandi di utilizzo più comune:

```bash
m: visualizza l'help
p: visualizza la tabella corrente delle partizioni
d: cancella una partizione
n: crea una nuova partizione
w: scrive la tabella delle partizioni sul disco
t: imposta il tipo di filesystem previsto per la partizione
a: abilita/disabilita il flag che rende la partizione avviabile (boot flag)
l: visualizza l'elenco dei tipi di filesystem utilizzabili
q: chiude fdisk senza modificare il disco

Create a new label
  g   create a new empty GPT partition table
  G   create a new empty SGI (IRIX) partition table
  o   create a new empty DOS partition table
  s   create a new empty Sun partition table
```

fdisk supporta diversi schemi di partizionamento. `MBR` e `GPT` sono i due standard più popolari. GPT è uno standard più recente che consente molti vantaggi rispetto all'MBR. 
I punti principali da considerare quando si sceglie quale standard di partizionamento usare:

- Utilizzare MBR per avviare il disco in modalità BIOS legacy;
- Utilizzare GPT per avviare il disco in modalità UEFI;
- Lo standard MBR supporta la creazione di una partizione del disco fino a 2 TiB. Altrimenti usare GPT;
- MBR ha un limite di 4 partizioni primarie, GPT fino a 128 partizioni

Premere `g` per creare una nuova tabella delle partizioni GPT:

L'output sarà simile al seguente:

```bash
Created a new GPT disklabel (GUID: 034596B2-1998-B241-BD8A-029063BDB87E).
```

Il prossimo passo sarà creare le nuove partizioni.

Premere `n` per creare una nuova partizione. 

Verrà richiesto di inserire il numero di partizione. Premere [Invio] per utilizzare il valore predefinito (1):

```bash
Partition number (1-128, default 1): 1
```

Successivamente, il comando chiederà di specificare il primo settore. Generalmente si consiglia sempre di utilizzare i valori predefiniti. Premere [Invio] per utilizzare il valore predefinito (2048):

```bash
First sector (2048-500118158, default 2048):
```

Ora è necessario inserire l'ultimo settore. È possibile utilizzare un valore assoluto oppure un valore relativo al settore iniziale, utilizzando il simbolo + seguito dalla dimensione della partizione. La dimensione può essere specificata in kilobyte (K), megabytes (M), gigabytes (G), terabytes (T) o petabytes (P).

Ad esempio, per creare una partizione di 100 GB, digitare `+100G` e premere [Invio]:

```bash
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-500118158, default 500118158): +100G

Created a new partition 1 of type 'Linux filesystem' and of size 100 GiB.
```

Per impostazione predefinita, il tipo di partizione è impostato su *filesystem Linux*, che dovrebbe andare bene per la maggior parte dei casi. Se si desidera modificare il tipo, premere `l` per ottenere un elenco di tipi di partizione, quindi premere `t` per modificare il tipo.

Infine premere `w` per scrivere le modifiche.

## Formattazione della periferica

Smontare la chiavetta USB:

```bash
sudo umount /dev/sda
```

### NTFS

Smontata la chiavetta dal sistema, digitare:

```bash
mkfs.ntfs /dev/sdaX -v
```
### EXT4

```bash
mkfs.ext4 /dev/sdaX -v
```

Un esempio:

```bash
sudo mkfs.ext4 -L "DAVIDE4GB" /dev/sdaX -v

mke2fs 1.46.6 (1-Feb-2023)
fs_types for mke2fs.conf resolution: 'ext4'
/dev/sda contains a ext4 file system
        last mounted on Sun Mar 19 12:18:08 2023
Proceed anyway? (y,N) y
Filesystem label=DAVIDE4GB
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
237104 inodes, 947456 blocks
47372 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=970981376
29 block groups
32768 blocks per group, 32768 fragments per group
8176 inodes per group
Filesystem UUID: 62fd2424-ae9d-45f8-84d9-5a3b63020661
Superblock backups stored on blocks: 
        32768, 98304, 163840, 229376, 294912, 819200, 884736

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done 
```
dove `-L` specifica il nome del filesystem, così che sia identificabile più facilmente. L'opzione `-v` indica *verbose*.

## Verificare la Formattazione

```bash
sudo fsck /dev/sda

sudo fsck /dev/sda 
fsck da util-linux 2.38.1
e2fsck 1.46.6 (1-Feb-2023)
/dev/sda is mounted.
e2fsck: Cannot continue, aborting.
```
Oppure col comando:

```bash
sudo file -sL /dev/sda
```
Ora che le partizioni sono state create, il passaggio successivo consiste nel formattare le partizioni e montarle.

## Collegamenti

- [https://www.chimerarevo.com/guide/linux-formattare-pendrive-usb-terminale-411029/](https://www.chimerarevo.com/guide/linux-formattare-pendrive-usb-terminale-411029/)
- [https://phoenixnap.com/kb/linux-format-usb](https://phoenixnap.com/kb/linux-format-usb)
- [https://wiki.archlinux.org/title/File_systems](https://wiki.archlinux.org/title/File_systems)
- [https://linuxhandbook.com/mkfs-command/](https://linuxhandbook.com/mkfs-command/)
- [https://www.thegeekdiary.com/mkfs-ext4-command-examples-in-linux/](https://www.thegeekdiary.com/mkfs-ext4-command-examples-in-linux/)
- [https://guide.debianizzati.org/index.php/Guida_alla_formattazione_dei_dischi_con_fdisk](https://guide.debianizzati.org/index.php/Guida_alla_formattazione_dei_dischi_con_fdisk)
- [https://guide.debianizzati.org/index.php/Guida_ai_comandi_da_terminale_-_Gestione_del_File_System](https://guide.debianizzati.org/index.php/Guida_ai_comandi_da_terminale_-_Gestione_del_File_System)
- [https://noviello.it/come-creare-partizioni-con-fdisk-su-linux/](https://noviello.it/come-creare-partizioni-con-fdisk-su-linux/)

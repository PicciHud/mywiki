# tmux

tmux consente di creare più finestre e riquadri all'interno di una singola finestra di terminale.

Ogni riquadro conterrà la propria istanza di terminale in esecuzione in modo indipendente. Ciò consente di avere più comandi e applicazioni in esecuzione 
visivamente uno accanto all'altro senza la necessità di aprire più finestre. Inoltre tmux mantiene queste finestre salvate in una sessione.

Si tratta di un'alternativa a [screen](./screen.md)

## Installazione

```bash
sudo apt install tmux
```
## Avvio di una sessione

```bash
tmux
```
Questo creerà una nuova sessione tmux con una bella barra di stato completamente verde in basso, che può essere personalizzata a piacere.

## Comandi principali

| Comando                                  | Descrizione                                                                                                 |
|------------------------------------------|-------------------------------------------------------------------------------------------------------------|
| `C-b` + `%`                              | Crea un nuovo pannello verticalmente                                                                        |
| `C-b` + `"`                              | Crea un nuovo pannello orizzontalmente                                                                      |
| `C-b` + `arrow key`                      | Con freccia che punta al riquadro a cui si desidera passare                                                 |
| `C-b` + `z`                              | Pannello selezionato a schermo intero. Digitare lo stesso comando per riportarlo alla dimensione originaria |
| `C-d`                                    | Chiude un pannello                                                                                          |
| `C-b` + `c`                              | Crea una nuova finestra                                                                                     |
| `C-b` + `p`                              | Per passare alla finestra successiva                                                                        |
| `C-b` + `n`                              | Per passare alla finestra precedente                                                                        |
| `C-b` + `number`                         | Dove `number` indica il numero della finestra                                                               |
| `C-b` + `d`                              | Scollega la sessione, lasciando tutti i processi e i comandi attivi in background                           |
| `C-b` + `alt+arrow key`                  | Ridimensiona il pannello                                                                                    |
| `tmux ls`                                | Elenco di tutte le sessioni                                                                                 |
| `tmux attach -t 0`                       | Per connettersi alla sessione 0                                                                             |
| `tmux new -s nome-sessione`              | Per creare una sessione con un nome significativo                                                           |
| `tmux rename-session -t 0 nome-sessione` | Per rinominare una sessione esistente                                                                       |
| `C-b` + `?`                              | Mostra i comandi principali                                                                                 |
| `C-b` + `:`                              | Entrare in modalità comando                                                                                 |
| `tmux kill-session -t sess_1`            | Per chiudere la sessione `sess_1`                                                                           |
| `tmux kill-server`                       | Terminare il server tmux                                                                                    |

## Configurazione


È possibile semplificarsi la vita, configurando le combinazioni di comandi tramite il file `tmux.conf`. Aggiungendo le seguenti righe, ad esempio, è possibile sostituire `C-b` con `C-a`:

```bash
set -g prefix C-a
unbind C-b
bind C-a send-prefix
```

### Barra di stato e plugins

Esempio di configurazione della barra di stato: [https://dev.to/brandonwallace/make-your-tmux-status-bar-100-better-with-bash-2fne](https://dev.to/brandonwallace/make-your-tmux-status-bar-100-better-with-bash-2fne)

tmux può anche essere esteso con vari plugins.

## Collegamenti

- [https://noviello.it/guida-rapida-come-usare-tmux-su-linux/](https://noviello.it/guida-rapida-come-usare-tmux-su-linux/)
- [https://www.lffl.org/2020/06/guida-come-utilizzare-tmux-terminal-multiplexer.html](https://www.lffl.org/2020/06/guida-come-utilizzare-tmux-terminal-multiplexer.html)
- [https://www.laseroffice.it/blog/2020/06/07/guida-come-utilizzare-tmux-terminal-multiplexer-su-gnu-linux/](https://www.laseroffice.it/blog/2020/06/07/guida-come-utilizzare-tmux-terminal-multiplexer-su-gnu-linux/)
- [https://www.guidetti-informatica.net/2022/10/come-personalizzare-la-configurazione-di-tmux/](https://www.guidetti-informatica.net/2022/10/come-personalizzare-la-configurazione-di-tmux/)
- [https://github.com/tmux-plugins/tpm](https://github.com/tmux-plugins/tpm)
- [https://thevaluable.dev/tmux-config-mouseless/](https://thevaluable.dev/tmux-config-mouseless/)
- [https://jdhao.github.io/2018/09/30/tmux_settings_for_vim_users/](https://jdhao.github.io/2018/09/30/tmux_settings_for_vim_users/)
- [https://gist.github.com/xinshuoweng/ea62e1b19f30dbba60184a85cf04e9a1](https://gist.github.com/xinshuoweng/ea62e1b19f30dbba60184a85cf04e9a1)
- [https://gist.github.com/tsl0922/d79fc1f8097dde660b34](https://gist.github.com/tsl0922/d79fc1f8097dde660b34)
- [https://linuxhint.com/kill-tmux-sessions/](https://linuxhint.com/kill-tmux-sessions/)

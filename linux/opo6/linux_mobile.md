# GNU/Linux mobile

Per prima cosa, nel caso fosse presente una versione superiore o inferiore, occorre installare Android alla versione 9.0.4 sull'OnePlus 6, questo per via di `Halium`,
che consente alle distribuzioni Linux come Ubuntu Touch di utilizzare gli stessi driver hardware e altri componenti a basso livello sviluppati originariamente per i dispositivi Android. Ciò
rende più facile per gli sviluppatori creare ROM personalizzate e altri software per questi dispositivi, senza dover preoccuparsi di problemi di compatibilità hardware.

Maggiori informazioni:

- [https://devices.ubuntu-touch.io/device/enchilada/](https://devices.ubuntu-touch.io/device/enchilada/)
- [https://ubports.com/nl/members/tag/halium-4](https://ubports.com/nl/members/tag/halium-4)
- [https://en.wikipedia.org/wiki/Halium](https://en.wikipedia.org/wiki/Halium)
- [https://halium.org/](https://halium.org/)

## Downgrade ad Android 9.0.4

[https://forum.xda-developers.com/t/guide-rollback-downgrade-from-oos-android-10-q-to-stable-9-pie.4000645/](https://forum.xda-developers.com/t/guide-rollback-downgrade-from-oos-android-10-q-to-stable-9-pie.4000645/)

1. Scaricare il seguente pacchetto:
```bash
wget -c https://oxygenos.oneplus.net/fulldowngrade_wipe_MSM_17819_181025_2315_user_MP1_release.zip
```
2. Aprire il file zip appena scaricato;
3. Copiare il file di appena estratto nella memoria interna del dispositivo;
4. Aprire Impostazioni sul telefono;
5. Selezionare Sistema e cliccare su Aggiornamenti di sistema;
6. Fare clic sull'icona dell'ingranaggio in alto a destra;
7. Sulla finestra pop-up, cliccare su Aggiornamento locale;
8. Trovare il file copiato al passaggio 3 e selezionarlo;
9. Cliccare su Conferma;
11. L'installazione richiederà alcuni minuti.

## Sblocco del bootloader

```bash
sudo apt install android-tools-adb android-tools-fastboot
```
1. Abilitare la *Modalità sviluppatore* sul OPO6;
2. Abilitare il *debug usb*;
3. Riavviare il dispositivo in `fastboot mode` 

Dare i seguenti comandi:

```bash
fastboot devices #restituisce i device collegati

fastboot oem unlock

fastboot reboot
```

## PostmarketOS

Da [https://postmarketos.org/download/](https://postmarketos.org/download/) scaricare la versione di interesse.

Esistono i seguenti DE:

```txt
gnome-mobile
phosh
plasma-mobile
sxmo-de-sway
```

1. Riavviare OPO6 in fastboot mode e collegarlo al pc;
2. Dopo aver scaricato l'archivio precedente, decomprimere l'archivio:

```bash
unxz 20230606-2346-postmarketOS-v23.06-phosh-22.2-oneplus-enchilada.img.xz

> ll
Permissions Size User   Date Modified Name
.rw-r--r--   13M davide  8 giu 21:07   20230606-2346-postmarketOS-v23.06-phosh-22.2-oneplus-enchilada-boot.img
.rw-r--r--  1,9G davide  8 giu 21:11   20230606-2346-postmarketOS-v23.06-phosh-22.2-oneplus-enchilada.img
```
3. Procedere con l'installazione:

```bash
fastboot erase dtbo
fastboot flash boot [the file that ends in -boot.img]
fastboot flash userdata [the other file]
fastboot reboot
```

## Mobian

1. Similmente, da [https://images.mobian.org/sdm845/](https://images.mobian.org/sdm845/) scaricare la versione di interesse;
2. Dopo aver scompattato l'archivio:

```bash
> ll
Permissions Size User   Date Modified Name
.rw-r--r--   23M davide  6 giu 12:25   mobian-sdm845-phosh-12.0.boot-enchilada.img
.rw-r--r--   41M davide  6 giu 12:25   mobian-sdm845-phosh-12.0.boot.img
.rw-r--r--  3,3G davide  6 giu 12:27   mobian-sdm845-phosh-12.0.rootfs.img
```
3. Installare *Mobian* coi seguenti comandi:

```bash
fastboot flash boot mobian-<processor>-phosh-YYYYMMDD.boot-<model>.img
fastboot flash system mobian-<processor>-phosh-YYYYMMDD.boot.img
fastboot -S 100M flash userdata mobian-<processor>-phosh-YYYYMMDD.rootfs.img
fastboot erase dtbo
fastboot reboot
```
Siccome OPO6 ha due partizioni A/B, per selezionarne una oppure l'altra:

```bash
fastboot --set-active=[a|b]
```

È possibile cambiare DE seguendo questa guida: [https://wiki.mobian-project.org/doku.php?id=desktopenvironments](https://wiki.mobian-project.org/doku.php?id=desktopenvironments)

## Operare da remoto

Sull'OPO6 installare ssh:

```bash
sudo apt install openssh-server

#abilitare la porta 22 nel file /etc/ssh/sshd_config

sudo systemctl restart sshd
```

### Installazione flatpak

[https://flatpak.org/setup/Debian](https://flatpak.org/setup/Debian)

Collegarsi via ssh e dare i seguenti comandi:

```bash
sudo -i

apt install flatpak
apt install gnome-software-plugin-flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

Installare i programmi necessari da [https://flathub.org/it](https://flathub.org/it)

### Installazione app Android (Mobian)

[https://wiki.mobian-project.org/doku.php?id=waydroid](https://wiki.mobian-project.org/doku.php?id=waydroid)

```bash
sudo apt install wget curl ca-certificates -y
curl https://repo.waydro.id | sudo bash
sudo apt install waydroid -y
sudo waydroid init
```

Quindi, al termine del download, avviare il container:

```bash
sudo waydroid container start
waydroid session start
```

#### Troubleshooting

```bash
waydroid show-full-ui
for i in ~/.local/share/applications/waydroid*desktop; do echo 'NoDisplay=true' >> $i; done #Hide launchers

#Disable suspend inside Waydroid
waydroid prop set persist.waydroid.no_suspend true
waydroid prop set persist.waydroid.suspend false
```

#### Installare app android

```bash
waydroid app install xyz.apk
```

#### Cartella condivisa

```bash
 sudo mount --bind ~/Documents ~/.local/share/waydroid/data/media/0/Documents 
 sudo mount --bind ~/Downloads ~/.local/share/waydroid/data/media/0/Download 
 sudo mount --bind ~/Music ~/.local/share/waydroid/data/media/0/Music 
 sudo mount --bind ~/Pictures ~/.local/share/waydroid/data/media/0/Pictures 
 sudo mount --bind ~/Videos ~/.local/share/waydroid/data/media/0/Movies
```

Per configurare una cartella condivisa tra Waydroid e il SO host: [https://docs.waydro.id/faq/setting-up-a-shared-folder](https://docs.waydro.id/faq/setting-up-a-shared-folder)

## Ubuntu Touch

Per prima cosa, è necessario reinstallare l'ultima versione di Android 9 disponibile per il OP6. È possibile scaricare la ROM dal link seguente: [https://androidfilehost.com/?w=files&flid=271877](https://androidfilehost.com/?w=files&flid=271877).

Maggiori dettagli: 

- [https://forum.xda-developers.com/t/how-to-update-return-to-stock-your-oneplus-6-6t-root-no-root-stock-based-others.3870795/](https://forum.xda-developers.com/t/how-to-update-return-to-stock-your-oneplus-6-6t-root-no-root-stock-based-others.3870795/)
- [https://forum.xda-developers.com/t/rom-stock-fastboot-op6-stock-fastboot-roms-for-oneplus-6.3796665/](https://forum.xda-developers.com/t/rom-stock-fastboot-op6-stock-fastboot-roms-for-oneplus-6.3796665/)

Quindi decomprimere il file scaricato col comando:

```bash
unzip 9.0.9-OnePlus6Oxygen_22_OTA_034_all_1909112343_dd26-FASTBOOT.zip

cd 9\ 0\ 9-OnePlus6Oxygen\ 22\ OTA\ 034\ all\ 1909112343\ dd26-FASTBOOT/
unzip images.zip
unzip Others_flashall.zip

cd images/
```

Collegare il dispositivo al pc e dalla cartella `images/` lanciare il seguente comando:

```bash
fastboot flashall
```

che si occuperà di flashare tutti le seguenti immagini:

```
fastboot flash aop_a aop.img
fastboot flash aop_b aop.img
fastboot flash bluetooth_a bluetooth.img
fastboot flash bluetooth_b bluetooth.img
fastboot flash boot_a boot.img
fastboot flash boot_b boot.img
fastboot flash dsp_a dsp.img
fastboot flash dsp_b dsp.img
fastboot flash dtbo_a dtbo.img
fastboot flash dtbo_b dtbo.img
fastboot flash modem_a modem.img
fastboot flash modem_b modem.img
fastboot flash oem_stanvbk oem_stanvbk.img
fastboot flash qupfw_a qupfw.img
fastboot flash qupfw_b qupfw.img
fastboot flash storsec_a storsec.img
fastboot flash storsec_b storsec.img
fastboot flash system_a system.img
fastboot flash system_b system.img
fastboot flash vbmeta_a vbmeta.img
fastboot flash vbmeta_b vbmeta.img
fastboot flash vendor_a vendor.img
fastboot flash vendor_b vendor.img
fastboot flash LOGO_a LOGO.img
fastboot flash LOGO_b LOGO.img
fastboot reboot bootloader
```

Il telefono si riavvierà e sarà installata l'ultima versione di Android 9, per la compatibilità con Halium.

Nel caso di errore:

> You need to set ANDROID_PRODUCT_OUT in /etc/environment.
> You may have a folder with boot.img and system.img etc. in, this is the right folder. Example: 
> export ANDROID_PRODUCT_OUT=/path/to/folder/with/images/
> After that you can call sudo fastboot flashall -w 

- [https://techpiezo.com/linux/fastboot-error-mke2fs-failed-cannot-generate-image-for-userdata/](https://techpiezo.com/linux/fastboot-error-mke2fs-failed-cannot-generate-image-for-userdata/)

Invece, se si ricevesse il seguente errore:

```bash
 > fastboot flash aop_a aop.img
Sending 'aop_a' (180 KB)                           OKAY [  0.006s]
Writing 'aop_a'                                    FAILED (remote: 'Flashing is not allowed for Critical Partitions
')
fastboot: error: Command failed')
```
dare i seguenti comandi, semplicemente:

```bash
fastboot flashing unlock_critical
fastboot reboot
```

Nel caso si volesse eseguire uno script:

```bash
fastboot erase boot_a && \
fastboot erase boot_b && \
fastboot erase system_a && \
fastboot erase system_b && \
fastboot erase recovery && \
fastboot erase recovery_a && \
fastboot erase recovery_b && \
fastboot erase userdata && \
fastboot erase userdata_a && \
fastboot erase userdata_b && \
fastboot flash aop_a aop.img && \
fastboot flash aop_b aop.img && \
fastboot flash bluetooth_a bluetooth.img && \
fastboot flash bluetooth_b bluetooth.img && \
fastboot flash boot_a boot.img && \
fastboot flash boot_b boot.img && \
fastboot flash dsp_a dsp.img && \
fastboot flash dsp_b dsp.img && \
fastboot flash dtbo_a dtbo.img && \
fastboot flash dtbo_b dtbo.img && \
fastboot flash fw_4j1ed_a fw_4j1ed.img && \
fastboot flash fw_4j1ed_b fw_4j1ed.img && \
fastboot flash fw_4u1ea_a fw_4u1ea.img && \
fastboot flash fw_4u1ea_b fw_4u1ea.img && \
fastboot flash modem_a modem.img && \
fastboot flash modem_b modem.img && \
fastboot flash oem_stanvbk oem_stanvbk.img && \
fastboot flash qupfw_a qupfw.img && \
fastboot flash qupfw_b qupfw.img && \
fastboot flash storsec_a storsec.img && \
fastboot flash storsec_b storsec.img && \
fastboot flash system_a system.img && \
fastboot flash system_b system.img && \
fastboot flash vbmeta_a vbmeta.img && \
fastboot flash vbmeta_b vbmeta.img && \
fastboot flash vendor_a vendor.img && \
fastboot flash vendor_b vendor.img && \
fastboot flash LOGO_a LOGO.img && \
fastboot flash LOGO_b LOGO.img && \
fastboot flash persist persist.img && fastboot reboot
```
Maggiori informazioni: [https://xdaforums.com/t/rom-stock-fastboot-op6-stock-fastboot-roms-for-oneplus-6.3796665/](https://xdaforums.com/t/rom-stock-fastboot-op6-stock-fastboot-roms-for-oneplus-6.3796665/)

### Installazione di Ubuntu Touch

Ora basta scaricare l'installer: [https://ubuntu-touch.io/it/get-ubuntu-touch](https://ubuntu-touch.io/it/get-ubuntu-touch)

Quindi installarlo, collegare il cellulare e seguire le istruzioni a schermo.

## Lista app Linux mobile

- [https://mglapps.frama.io/](https://mglapps.frama.io/)
- [https://linuxphoneapps.org/apps/](https://linuxphoneapps.org/apps/)
- [https://wiki.mobian.org/doku.php?id=apps](https://wiki.mobian.org/doku.php?id=apps)

## Collegamenti

- [https://nerdschalk.com/how-to-roll-back-oneplus-6-6t-to-android-9-pie/](https://nerdschalk.com/how-to-roll-back-oneplus-6-6t-to-android-9-pie/)
- [https://linuxconfig.org/how-to-install-android-debug-tools-on-debian-10-buster](https://linuxconfig.org/how-to-install-android-debug-tools-on-debian-10-buster)
- [https://wiki.postmarketos.org/wiki/OnePlus_6_(oneplus-enchilada)#Pre-built_images](https://wiki.postmarketos.org/wiki/OnePlus_6_(oneplus-enchilada)#Pre-built_images)
- [https://wiki.mobian-project.org/doku.php?id=install-android](https://wiki.mobian-project.org/doku.php?id=install-android)
- [https://wiki.mobian-project.org/doku.php?id=waydroid](https://wiki.mobian-project.org/doku.php?id=waydroid)
- [https://docs.waydro.id/usage/install-and-run-android-applications](https://docs.waydro.id/usage/install-and-run-android-applications)
- [https://www.droidviews.com/restore-oneplus-6-to-stock-oxygen-os-rom/](https://www.droidviews.com/restore-oneplus-6-to-stock-oxygen-os-rom/)
- [https://droidwin.com/restore-oneplus-6-stock-via-fastboot-commands/](https://droidwin.com/restore-oneplus-6-stock-via-fastboot-commands/)

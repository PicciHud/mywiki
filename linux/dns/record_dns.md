# DNS

La sigla DNS sta per Domain Name System, sistema dei nomi di dominio. Si tratta in sostanza della rubrica telefonica del Web in cui i domini sono organizzati e identificati. Così come una rubrica telefonica traduce un nome come "Pizzeria da Ciro" nel numero di telefono da chiamare, il DNS traduce un indirizzo web come "www.google.com" nell'indirizzo IP fisico, ad esempio "74.125.19.147", del computer che ospita il sito (nel nostro esempio, la home page di Google).

Ad un nome DNS possono corrispondere diversi tipi di informazioni. Per questo motivo, esistono diversi tipi di record DNS. Ogni voce del database DNS deve essere caratterizzata da un tipo. I principali tipi sono: 

## Record MX


## Record TXT

Un record TXT è un record DNS che contiene informazioni di testo utilizzate da fonti esterne al dominio e può essere utilizzato per una serie di scopi diversi: vengono utilizzati per verificare la proprietà del dominio e per implementare misure di sicurezza per l'email, tra cui SPF, DKIM e DMARC.

## Record CNAME

Il record CNAME, o record con nome canonico, collega un nome alias a un nome di dominio vero o canonico. Ad esempio, www.example.com potrebbe essere collegato a example.com. 

## Record A

Il record A o Address (detto anche "record host") collega il dominio all'indirizzo IP fisico del computer che ospita i servizi del dominio.  

## Record AAAA 

È come il Record A, ma lavora con l'IPv6 e restituisce un indirizzo IPv6.

## TTL (Time to Live)

Il TTL è un valore in un record DNS che determina il numero di secondi prima che le successive modifiche al record entrino in vigore. Ciascuno dei record DNS del dominio, quali i record MX, CNAME e così via, dispone di un valore TTL. Il valore TTL attuale di un record determina il periodo di tempo necessario per l'entrata in vigore di qualsiasi modifica. 

## Email spoofing

Email spoofing è una tecnica di attacco informatico che consiste nella creazione di email con indirizzo del mittente contraffatto. Viene comunemente usata per email spam e phishing al fine di ingannare il destinatario circa l'origine del messaggio. 

I record MX, SPF, DKIM e DMARC rappresentano un valido aiuto per evitare che il dominio venga utilizzato per attacchi di Email Spoofing. Sono anche utili per migliorare la reputazione del dominio.

## Record MX

Il record MX (acronimo di Mail Exchanger), una risorsa del sistema DNS che permette di indicare al protocollo SMTP quale sia il dominio del server deputato a ricevere il messaggio di posta elettronica. Per individuare dove sia localizzato il destinatario della email, infatti, il protocollo interroga il sistema DNS affinché fornisca 
l'indirizzo esatto dove poter recapitare il messaggio. Così facendo, il dominio del destinatario (tutto ciò che compare dopo la chiocciola in un indirizzo di posta elettronica)
può essere risolto (tradotto) nell'indirizzo IP del server e avviare il processo di trasmissione della missiva.

I record MX (Mail Exchange) reindirizzano le email di un dominio ai server che ospitano i relativi account utente. In un dominio è possibile configurare più record MX e assegnare a ciascun record una priorità diversa. Se non è possibile recapitare la posta utilizzando il record con il livello di priorità più elevato, verrà utilizzato quello con il livello di priorità successivo e così via. 

In altre parole, specifica un mail server autorizzato ad accettare messaggi mail per conto del dominio di destinazione, e contenente un valore indicante la priorità se sono disponibili più server email. Il set di MX records di un nome di dominio specifica come possano essere instradate le richieste di invio di posta mediante il Simple Mail Transfer Protocol (SMTP). 

### Record SPF

SPF è un Record nel DNS che *dichiara* quali sono gli indirizzi IP autorizzati ad inviare email a nome del dominio. Inoltre comunica ai destinatari che cosa devono fare nel caso in cui ricevano dei messaggi da IP diversi da quelli autorizzati. 

Il sistema consente, per le e-mail collegate a un dominio, di poter definire gli host (sever email) autorizzati a spedire messaggi di quel dominio. Così facendo consentono, quindi, al 
ricevente di controllarne la validità. La lista degli host autorizzati a inviare e-mail per un determinato dominio è pubblicata nel Domain Name System (DNS) sotto forma di record TXT appositamente formattati.

### Record DKIM

DKIM è un protocollo che serve a firmare digitalmente i messaggi in uscita dai server SMTP. Serve a certificare che quel messaggio sia stato effettivamente inviato da uno specifico provider. 
Consente, a chi riceve un’email, di verificare la provenienza del messaggio, in modo da accertarsi dell’autenticità del mittente. Il DKIM è utilizzato 
per contrastare il phishing e lo spam delle email.

In termini tecnici, il DKIM consente a un dominio di associare il proprio nome a un'email mediante l'apposizione di una firma digitale. 

### Record DMARC

DMARC è un protocollo di validazione dei messaggi email. Esso diventa molto utile in particolare quando un messaggio non rispetta le specifiche SPF e DKIM dichiarate dal proprietario del dominio,
stabilendo cosa debbano fare i provider destinatari.

Si tratta di uno standard che basato su SPF e DKIM. Consente al proprietario del dominio di creare una policy che indica ai provider di cassette postali (come Google o Microsoft) cosa fare 
se l'email non supera i controlli SPF e DKIM. 

La policy DMARC è pubblicata come record TXT nel DNS del proprio dominio.

## FQDN

Un FQDN (sigla di Fully Qualified Domain Name) è un nome di dominio non ambiguo che specifica la posizione assoluta di un nodo all'interno della gerarchia dell'albero DNS.

In breve, rappresenta il nome completo e univoco di un host su Internet. È composto dal nome specifico dell'host seguito dal nome del dominio.

Per esempio, dato un host con il nome "miopc" e un dominio "miodominio.it", l'FQDN è "miopc.miodominio.it". 

## Collegamenti

- [https://it.wikipedia.org/wiki/Tipi_di_record_DNS](https://it.wikipedia.org/wiki/Tipi_di_record_DNS)
- [https://it.wikipedia.org/wiki/FQDN](https://it.wikipedia.org/wiki/FQDN)
- [https://www.ionos.it/digitalguide/domini/gestione-dei-domini/fqdn/](https://www.ionos.it/digitalguide/domini/gestione-dei-domini/fqdn/)
- [https://www.ionos.it/digitalguide/hosting/tecniche-hosting/record-cname/](https://www.ionos.it/digitalguide/hosting/tecniche-hosting/record-cname/)
- [https://it.wikipedia.org/wiki/MX_record](https://it.wikipedia.org/wiki/MX_record)
- [https://www.qboxmail.it/2022/05/06/come-verificare-e-aggiornare-spf-dkim-dmarc/](https://www.qboxmail.it/2022/05/06/come-verificare-e-aggiornare-spf-dkim-dmarc/)
- [https://it.wikipedia.org/wiki/DomainKeys_Identified_Mail](https://it.wikipedia.org/wiki/DomainKeys_Identified_Mail)
- [https://it.wikipedia.org/wiki/Sender_Policy_Framework](https://it.wikipedia.org/wiki/Sender_Policy_Framework)
- [https://help.activecampaign.com/hc/it/articles/206903370-Autenticazione-SPF-DKIM-e-DMARC#dkim-0-2](https://help.activecampaign.com/hc/it/articles/206903370-Autenticazione-SPF-DKIM-e-DMARC#dkim-0-2)
- [https://it.wikipedia.org/wiki/Tipi_di_record_DNS#Record_DNS_normalmente_in_uso](https://it.wikipedia.org/wiki/Tipi_di_record_DNS#Record_DNS_normalmente_in_uso)
- [https://kinsta.com/it/knowledgebase/cosa-e-il-dns/](https://kinsta.com/it/knowledgebase/cosa-e-il-dns/)

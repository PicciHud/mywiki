# dmesg

Il comando dmesg consente infatti di stampare o controllare i messaggi inviati dal kernel.

## Sintassi

```bash
dmesg [opzioni]
```

## Filtrare gli errori

Per quanto facile possa essere leggere l’output, torna spesso utile filtrare per un certo livello.

Ogni riga/log viene classificata con un livello di importanza:

- `emerg`: sistema inutilizzabile
- `alert`: azione immediata richiesta
- `crit`: critico
- `err`: errore
- `warn`: avviso
- `notice`: normale
- `info`: informazione
- `debug`: informazioni per sviluppatori

Per visualizzare solo i log con livello warn, quindi solo avvisi:

```bash
dmesg --level=warn
```

## Collegamenti

- [https://linuxhub.it/articles/howto-utilizzo-del-comando-dmesg/](https://linuxhub.it/articles/howto-utilizzo-del-comando-dmesg/)

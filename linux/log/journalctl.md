# Journalctl

Per molti anni i log sono stati gestiti da un'utilità chiamata syslogd. La maggior parte dei sistemi operativi basati su GNU/Linux è passata a systemd, 
che usa **journalctl** per gestire i log di sistema.

## Leggere i log

Solo gli utenti che appartengono ai gruppi `adm` o `systemd-journal` possono leggere i log di sistema.

Digitare il seguente comando:

```bash
groups
```

Se non viene visualizzato "adm" oppure "systemd-journal" nell'output, aggiungere l'utente al gruppo col comando:

```bash
sudo adduser $USER adm
```
Sarà necessario riavviare la sessione affinché questa modifica abbia effetto.

## Journald

journald è un demone che raccoglie e scrive tutti i log del sistema nel file journal.

Il suo file di configurazione predefinito si trova in: `/etc/systemd/journald.conf`. Eccone un esempio:

```bash
# See journald.conf(5) for details.

[Journal]
#Storage=auto
#Compress=yes
#Seal=yes
#SplitMode=uid
#SyncIntervalSec=5m
#RateLimitInterval=30s
#RateLimitBurst=1000
#SystemMaxUse=
#SystemKeepFree=
#SystemMaxFileSize=
#SystemMaxFiles=100
#RuntimeMaxUse=
#RuntimeKeepFree=
#RuntimeMaxFileSize=
#RuntimeMaxFiles=100
#MaxRetentionSec=
#MaxFileSec=1month
#ForwardToSyslog=yes
#ForwardToKMsg=no
#ForwardToConsole=no
#ForwardToWall=yes
#TTYPath=/dev/console
#MaxLevelStore=debug
#MaxLevelSyslog=debug
#MaxLevelKMsg=notice
#MaxLevelConsole=info
#MaxLevelWall=emerg
```

Un certo numero di distribuzioni non consentono l'archiviazione permanente dei log sul disco per impostazione predefinita.

È possibile abilitare questa opzione impostando "Storage" su "persistent" come mostrato di seguito. Questo creerà la directory `/var/log/journal` e tutti i file journal saranno memorizzati
al suo interno.

```bash
sudo vim /etc/systemd/journald.conf

[Journal]
Storage=persistent
```
## Journalctl

journalctl è l'utilità che permette di visualizzare il contenuto scritto dal servizio journald, all interno della directory `/var/log/journal`.

- Per mostrare tutti i messaggi di log raccolti:

```bash
journalctl -r
```
- È possibile visualizzare un codice per ogni avvio, i relativi ID e i timestamp col comando:

```bash
journalctl --list-boots
IDX BOOT ID                          FIRST ENTRY                 LAST ENTRY                 
-58 9899ae7a3234496caead003883e62736 Sun 2023-01-29 15:50:01 CET Sun 2023-01-29 16:29:44 CET
-57 3f9349712f9f4048a58323ceb5bee7a6 Sun 2023-01-29 16:31:45 CET Sun 2023-01-29 16:56:00 CET
-56 94b990994f0c4e9286f7a1882e1191e7 Sun 2023-01-29 16:57:49 CET Sun 2023-01-29 18:46:05 CET
-55 f3c06acc6f1c45fd8b0843c57c77ef0c Sun 2023-01-29 18:48:10 CET Sun 2023-01-29 19:13:58 CET
-54 118bfa08b2014bcf9b6768b74e2a1420 Sun 2023-01-29 19:15:25 CET Sun 2023-01-29 19:39:30 CET
-53 5a4ab4de4e0f4f4cb3e48c362a6f2114 Sun 2023-01-29 19:41:05 CET Sun 2023-01-29 20:43:06 CET
-52 8a2db867772a4fd980847f22ba414bfa Sun 2023-01-29 22:12:56 CET Sun 2023-01-29 23:19:13 CET
-51 46bcde99b8da4547b68737e768244c69 Mon 2023-01-30 20:48:18 CET Mon 2023-01-30 23:28:00 CET
-50 bec4f26fec024306bfc42ca6a0016da6 Tue 2023-01-31 21:40:48 CET Tue 2023-01-31 23:00:23 CET
-49 578137da3a374630819e9305b2891fad Thu 2023-02-02 20:36:17 CET Thu 2023-02-02 21:00:25 CET
-48 52e20d014d0b45a9b541da4ac98f2b39 Thu 2023-02-02 21:01:46 CET Thu 2023-02-02 22:01:58 CET
```

- Per visualizzare le voci dell'avvio corrente (numero 0), utilizzare l'opzione -b:

```bash
journalctl -b
```
e per vedere un boot precedente, usare l'ID relativo con l'opzione -b:

```bash
journalctl -b -1
```
In alternativa, utilizzare l'ID di avvio:

```bash
journalctl -b 9fb590b48e1242f58c2579defdbbddc9
```

- Per vedere tutte le voci da una data e un'ora particolari, ad es. 15 giugno 2017 alle 8:15, digitare questo comando:

```bash
journalctl --since "2017-06-15 08:15:00"
journalctl --since today
journalctl --since yesterday
```

- Per visualizzare gli ultimi n messaggi (10 per impostazione predefinita), utilizzare il flag -n:

```bash
journalctl -n
journalctl -n 20
```
- Per vedere solo i messaggi del kernel, simile all'output del comando dmesg, usare il flag -k:

```bash
journalctl -k
journalctl -k -b
journalctl -k -b 9fb590b48e1242f58c2579defdbbddc9
```
- Per visualizzare tutte le voci di una determinata unità, utilizzare l'opzione -u come segue.

```bash
journalctl -u apache2.service
```

- Per mostrare i log dal precedente avvio:

```bash
journalctl -b -1 -u apache2.service
journalctl -u apache2.service --since today
```

- Per visualizzare i registri generati da un utente o gruppo particolare, specificare l'ID:

```bash
journalctl _UID=1000
journalctl _UID=1000 --since today
journalctl _UID=1000 -b -1 --since today
```

- Per mostrare tutti i log generati da un file eseguibile:

```bash
journalctl /usr/bin/bash
```

- È inoltre possibile filtrare l'output in base alle priorità dei messaggi utilizzando il flag -p. 

```bash
journalctl -p err

journalctl -p 3 -xe
```
dove l'opzione `-x` indica *show-priority*. Con questa opzione abilitata, il comando journalctl mostra anche il livello di priorità dei messaggi di log insieme al loro contenuto. 
I messaggi possono essere contrassegnati con diverse priorità, come emergenza, avviso, errore, avviso, informazione, ecc.

L'opzione `-e` indica *pager-end*. Con questa opzione abilitata, il comando journalctl visualizza l'output dell'ultimo messaggio di log e posiziona il cursore all'ultima riga, 
consentendo di scorrere rapidamente fino all'ultimo messaggio di log senza dover scorrere l'intero registro.

| Priority | Code    |
|----------|---------|
| 0        | emerg   |
| 1        | alert   |
| 2        | crit    |
| 3        | err     |
| 4        | warning |
| 5        | notice  |
| 6        | info    |
| 7        | debug   |

È possibile vedere i log in un range di gravità:

```bash
journalctl -p 4..6 -b0
```

- È possibile vedere i log mentre vengono scritti con l'opzione -f (simile alla funzionalità tail -f ):

```bash
journalctl -f
```

## Pulizia dei log

I file di log si trovano in `/var/log/journal/`

```bash
journalctl --disk-usage
```
Per ruotare i file di log, digitare il comando:

```bash
journalctl --rotate
```

### Cancellare i log più vecchi di...

```bash
journalctl --vacuum-time=2d
```
### Limitare la dimensione dei log

```bash
journalctl --vacuum-size=100M
```

## Limitare il numero dei file di log

```bash
journalctl --vacuum-files=5
```

### Cancellazione automatica dei vecchi file di log

È possibile configurare systemd per gestire automaticamente i vecchi file di log.

Il file di configurazione si trova in `/etc/systemd/journald.conf`. 

```bash
      │ File: /etc/systemd/journald.conf
──────┼────────────────────────────────────────────────────────────────────────────────────────────────────────────
  1   │ [Journal]
  2   │ #Storage=auto
  3   │ #Compress=yes
  4   │ #Seal=yes
  5   │ #SplitMode=uid
  6   │ #SyncIntervalSec=5m
  7   │ #RateLimitIntervalSec=30s
  8   │ #RateLimitBurst=10000
  9   │ #SystemMaxUse=
  10  │ #SystemKeepFree=
  11  │ #SystemMaxFileSize=
  12  │ #SystemMaxFiles=100
  13  │ #RuntimeMaxUse=
  14  │ #RuntimeKeepFree=
  15  │ #RuntimeMaxFileSize=
  16  │ #RuntimeMaxFiles=100
  17  │ #MaxRetentionSec=
  18  │ #MaxFileSec=1month
  19  │ #ForwardToSyslog=no
  20  │ #ForwardToKMsg=no
  21  │ #ForwardToConsole=no
  22  │ #ForwardToWall=yes
  23  │ #TTYPath=/dev/console
  24  │ #MaxLevelStore=debug
  25  │ #MaxLevelSyslog=debug
  26  │ #MaxLevelKMsg=notice
  27  │ #MaxLevelConsole=info
  28  │ #MaxLevelWall=emerg
  29  │ #LineMax=48K
  30  │ #ReadKMsg=yes
  31  │ #Audit=yes
```

Ecco alcuni utili parametri:

| Setting | Description |
| --- | --- |
| SystemMaxUse | Max disk space logs can take |
| SystemMaxFileSize | Max size of an INDIVIDUAL log file |
| SystemMaxFiles | Max number of log files |

Una volta modificato il file, riavviare il servizio:

```bash
systemctl restart systemd-journald
```

## Collegamenti

- [https://codepre.com/it/guia-para-principiantes-para-analizar-registros-en-linux-con-el-comando-journalctl.html](https://codepre.com/it/guia-para-principiantes-para-analizar-registros-en-linux-con-el-comando-journalctl.html)
- [https://it.linux-console.net/?p=632#gsc.tab=0](https://it.linux-console.net/?p=632#gsc.tab=0)
- [https://linuxhandbook.com/journalctl-command/](https://linuxhandbook.com/journalctl-command/)
- [https://linuxhandbook.com/clear-systemd-journal-logs/](https://linuxhandbook.com/clear-systemd-journal-logs/)

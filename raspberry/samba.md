# samba

Esistono diversi protocolli per la condivisione di files, ma se nella rete si trovano sia computer con GNU/Linux che con Microsoft Windows, 
allora è quasi inevitabile che la scelta debba ricadere su SMB/CIFS. 

samba è una suite di strumenti utilizzati per la condivisione tra reti miste Windows-GNU/Linux, implementando proprio il protocollo di rete `SMB/CIFS`.

## Installazione

```bash
apt install samba samba-common smb4k
```
## Creazione directory

Prima di andare a configurare samba occorre creare le shares, ovvero le directory da condividere. 

In questo esempio verrà creata una directory share dentro `/srv`, come indicato dal Filesystem Hierarchy Standard (FHS):

```bash
sudo mkdir -p /srv/samba

sudo mkdir /srv/samba/pubblici
sudo mkdir /srv/samba/files_privati
```
Vedremo successivamente quali permessi impostare a queste directory.

## Configurazione

Tutti i parametri di configurazione di samba si trovano in `/etc/samba/smb.conf`.

Ogni volta che si apportano delle modifiche al file sopra citato è necessario riavviare il servizio di samba:

```bash
sudo systemctl restart smbd.service
sudo systemctl restart nmbd.service
```

### Sezione global

La sezione `[global]` si trova sempre all'inizio della configurazione del file `/etc/samba/smb.conf`.

Definisce le impostazioni del server samba e le opzioni di default che verranno assegnate alle condivisioni. I principali parametri del server:

```bash
[global]
    allow hosts = 192.168.1.0/24                 # Range di IP che possono accedere al server     
    workgroup = WORKGROUP                        # Nome del gruppo di lavoro
    security = USER                              # Livello di sicurezza delle condivisioni
    server string = FILE SERVER                  # Descrizione della macchina
    create mask = 755                            # Permessi sulle cartelle
```
Nel parametro `security` si poteva utilizzare in alternativa il valore `SHARE` in modo da creare condivisioni liberamente accessibili, senza necessità di autenticazioni.

Altri possibili valori sono:

- `user`: richiede utente e password per l'accesso alla condivisione;
- `domain`: questo valore va assegnato quando si vuole configurare il server samba come Domain Controller;
- `ADS`: questo valore consente al server samba di agganciarsi a un dominio Active Directory 

Se si volesse rendere accessibili delle condivisioni anche ad utenti senza credenziali, inserire anche questi due parametri:

```bash
map to guest = Bad User 
guest account = nobody
```

### Sezione condivisione

Aggiungere la cartella nel file di configurazione `/etc/samba/smb.conf`:

```bash
[shared]
    comment = Cartella condivisa        # Commento sulla condivisione
    path = /cartella/da/condividere     # Percorso della condivisione
    browseable = yes                    # Rende visibile la condivisione
    public = yes                        # Rende la cartella accessibile
    create mask = 0755                  # Permessi dei file
```

Dove /cartella/da/condividere è la directory da condividere.

#### Esempio di condivisione pubblica

Ecco alcuni esempi di condivisioni, ciascuno con una politica di sicurezza differente. Sotto alla sezione global scrivere:

```bash
[pubblici]
comment = files pubblici
path = /srv/samba/pubblici
writeable = yes
browsable = yes
guest ok = yes
read only = no
create mask = 770
```

A questa condivisione avrà accesso qualsiasi utente di sistema.

Se in [global] è presente l'opzione `map to guest = Bad User` non verrà richiesta alcuna password e l'utente che verrà utilizzato è quello indicato in guest account (nobody). 

In caso contrario verranno chieste le credenziali, ma qualsiasi utente di sistema potrà accedervi.

Con `create mask` vengono indicati i permessi nella classica scrittura Unix.

#### Esempio di condivisione privata

```bash
[files_privati]
comment = Files privati di Luca
path = /srv/samba/files_privati
browsable = no
guest ok = no
valid users = Luca
```

A questa condivisione avrà accesso solamente l'utente Luca.

Tramite l'opzione browsable=no è impedita la visualizzazione della cartella tra le condivisioni samba. L'utente Luca per accedervi dovrà richiamarla direttamente. 

## Montare condivisione

Per montare una condivisone samba presente su un altro file server, si utilizza il seguente comando:

```bash
smbmount //SERVER/condivisione /punto/di/mount
```

Dove `/punto/di/mount/` è una directory creata con privilegi di amministrazione solitamente in `/media`.

## Connessione al server

Per connettersi ad un server samba si usa:

```bash
smbclient //SERVER/condivisione
```
### Sicurezza e controllo accessi

L'accesso alle directory condivise viene regolato sia nativamente a livello di file system, sia a livello di condivisione samba.

#### Aggiungere utente

Per quanto riguarda la condivisione `[files_privati]`, solo l'utente Luca può accedervi e scrivere al suo interno. 
Perché questo funzioni però bisogna creare l'utente Luca su samba e assegnargli, sul server, la directory interessata.

Prima di aggiunge un utente su samba bisogna che questo sia presente nel sistema.

```bash
sudo adduser luca --home=/home/public --shell=/bin/false --disabled-password
```

L’opzione `--shell=/bin/false disabilita` l’accesso alla shell da parte dell’utente creato, che non avrà quindi possibilità di fare un vero e proprio login. Questo per ragioni di sicurezza, come accade per gli account di sistema.

Creare quindi l'utente in samba: 

```bash
sudo smbpasswd -a Luca
```

Creato l'utente bisogna ora assegnargli la directory alla quale dovrà accedere:

```bash
sudo chown luca files_privati
sudo chmod -R 700 /srv/samba/files_privati
```

In qualsiasi momento è possibile vedere la lista degli utenti samba con relative informazioni tramite il comando:

```bash
sudo pdbedit -L -v
```
dove -L richiede l'elenco degli utenti, l'opzione -v, invece, sta per `verbose`.

## Log di samba

Tutti i log di samba sono presenti nella cartella `/var/log/samba`.

## Collegamenti

- [https://wiki.ubuntu-it.org/Server/samba](https://wiki.ubuntu-it.org/Server/samba)
- [https://www.mrw.it/linux/creiamo-file-server-samba_7411.html](https://www.mrw.it/linux/creiamo-file-server-samba_7411.html)
- [https://guide.debianizzati.org/index.php/Installare_Samba_per_condividere_directory_-_Debian_Stretch](https://guide.debianizzati.org/index.php/Installare_Samba_per_condividere_directory_-_Debian_Stretch)
- [https://guide.debianizzati.org/index.php/Samba:_guida_rapida](https://guide.debianizzati.org/index.php/Samba:_guida_rapida)


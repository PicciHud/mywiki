# Nas casalingo

<style>
.big{
width: 60%;
height: auto;
}
</style>


Occorrente:

- Raspberry PI 4 2 GB di ram;
- cavo di rete;
- micro SDXC da 32 GB;
- 2 HDD TOSHIBA MG07ACA14TE (14 TB l'uno) 

```bash
=== START OF INFORMATION SECTION ===
Model Family:     Toshiba MG07ACA... Enterprise Capacity HDD
Device Model:     TOSHIBA MG07ACA14TE
Serial Number:    X2V0A0YFF94G
LU WWN Device Id: 5 000039 c18d3727f
Firmware Version: 0104
User Capacity:    14,000,519,643,136 bytes [14.0 TB]
Sector Sizes:     512 bytes logical, 4096 bytes physical
Rotation Rate:    7200 rpm
Form Factor:      3.5 inches
Device is:        In smartctl database [for details use: -P show]
ATA Version is:   ACS-3 T13/2161-D revision 5
SATA Version is:  SATA 3.3, 6.0 Gb/s (current: 6.0 Gb/s)
Local Time is:    Sun Feb 19 13:47:44 2023 CET
SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM feature is:   Unavailable
APM level is:     1 (minimum power consumption with standby)
Rd look-ahead is: Enabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Disabled, NOT FROZEN [SEC1]
Wt Cache Reorder: Enabled
```

- 2 adattatori SATA-USB 3.0

- Case Terrapi Extreme Duo ([https://shop.inux3d.com/it/home/101-134-terrapi-xtreme-duo.html#/13-colore-arancione](https://shop.inux3d.com/it/home/101-134-terrapi-xtreme-duo.html#/13-colore-arancione))

Qui le istruzioni per il montaggio: [https://shop.inux3d.com/it/index.php?controller=attachment&id_attachment=25](https://shop.inux3d.com/it/index.php?controller=attachment&id_attachment=25)

## Installazione Raspberry Pi OS Lite

Scaricare ed eseguire l'installer da questo link: [https://www.raspberrypi.com/software/](https://www.raspberrypi.com/software/), tramite i seguenti comandi:

```bash
cd Scaricati
wget -c https://downloads.raspberrypi.org/imager/imager_latest_amd64.deb
sudo apt install ./imager_latest_amd64.deb
```
Avviare il programma. Scegliere come OS `Raspberry Pi OS Lite (64 bit)`

![liteos](./asset/nas/64bit.webp)

selezionare la micro SD, quindi cliccare sul pulsante delle impostazioni per personalizzare il SO:

![settings](./asset/nas/64bitsettings.webp)

![raspinstaller](./asset/nas/installer.png)

Una volta inserite le impostazioni desiderate, cliccare su `Salva`, poi su `Scrivi` e attendere qualche minuto. Per poter connettersi via `ssh` e chiave pubblica, seguire la guida [ssh](./SSH.md).

Connettere il Raspberry Pi al router tramite cavo di rete, oppure via WiFi. Inserire la micro SD all’interno del Raspberry Pi, alimentarlo e attendere qualche minuto in modo che il Raspberry Pi completi la procedura di boot.

Assicurarsi di raggiungere il Raspberry Pi 4:

```bash
ping indirizzo_ip_raspberry -c 10

 > ping 192.168.52.48 -c 10
PING 192.168.52.48 (192.168.52.48) 56(84) bytes of data.
64 bytes from 192.168.52.48: icmp_seq=1 ttl=64 time=113 ms
64 bytes from 192.168.52.48: icmp_seq=2 ttl=64 time=62.3 ms
64 bytes from 192.168.52.48: icmp_seq=3 ttl=64 time=124 ms
64 bytes from 192.168.52.48: icmp_seq=4 ttl=64 time=41.7 ms
64 bytes from 192.168.52.48: icmp_seq=5 ttl=64 time=131 ms
64 bytes from 192.168.52.48: icmp_seq=6 ttl=64 time=161 ms
64 bytes from 192.168.52.48: icmp_seq=7 ttl=64 time=86.2 ms
64 bytes from 192.168.52.48: icmp_seq=8 ttl=64 time=122 ms
64 bytes from 192.168.52.48: icmp_seq=9 ttl=64 time=63.8 ms
64 bytes from 192.168.52.48: icmp_seq=10 ttl=64 time=136 ms

--- 192.168.52.48 ping statistics ---
10 packets transmitted, 10 received, 0% packet loss, time 9004ms
rtt min/avg/max/mdev = 41.735/104.007/160.501/36.464 ms
```

Accedere ora da remoto al Raspberry Pi tramite SSH. Nel caso non si avesse generato la coppia di chiavi, ci si può sempre connettere tramite il comando:

```bash
ssh host@indirizzo_ip

ssh pi@192.168.1.20
```

## Installazione OpenMediaVault

Aggiornare i pacchetti, le dipendenze e il SO col comando:

```bash
sudo apt update && sudo apt upgrade -y && sudo apt autoremove -y && sudo apt full-upgrade
```

Una volta completata la procedura di aggiornamento del Raspberry Pi, eseguire il seguente comando che in primo luogo scaricherà lo script di installazione di **OpenMediaVault**
e successivamente lo eseguirà.
Attraverso questo script verranno installati e configurati tutti i componeti necessari per eseguire **OpenMediaVault** sul Raspberry Pi:

```bash
sudo wget -O - https://github.com/**OpenMediaVault**-Plugin-Developers/installScript/raw/master/install | sudo bash
```

Al termine dell'installazione, la sessione SSH verrà chiusa automaticamente.

## Configurazione

Per essere in grado di accedere alla web GUI di **OpenMediaVault**, è necessario conoscere l’indirizzo IP del Raspberry Pi. Se non lo si conoscesse, è possibile eseguire il seguente comando per visualizzarlo:

```bash
hostaname -I
```

Con l’indirizzo IP locale del Raspberry Pi, si può accedere all'interfaccia web:

```bash
http://ip-raspberrypi
```

Dopo aver caricato l’interfaccia web di **OpenMediaVault**, verrà chiesto di effettuare il login per poter procedere.

Il nome utente predefinito è `admin` e la password predefinita è `openmediavault`.

![access](./asset/nas/access.png)

Una delle prime cose da fare è cambiare la password del account admin di OpenMediaVault:

![password](./asset/nas/password.png)

Dallo stesso menù è anche possibile personalizzare la `Dashboard` in modo da avere più informazioni a colpo d'occhio:

![dashboard](./asset/nas/dashboard.png)

## Creazione del File System

Dopo aver collegato i due dischi, formattarli e ripristinarli allo stato di fabbrica:

![formattazione](./asset/nas/formattazione.png)

Impostare i due dischi nel modo seguente:

![hdd](./asset/nas/disco.png)

### Creazione RAID 1

![raid](./asset/nas/raid1.png)

```bash
Tipo: BTRFS
Profile: RAID1

Selezionare i due HDD
```

![raid](./asset/nas/raid2.png)

Cliccare su crea e attendere che venga creato appunto il File System.

## Cartelle condivise

### Utenti

Come prima cosa, creare gli utenti di OMV necessari, i quali potranno accedere alle cartelle condivise in lettura/scrittura o solo in lettura. Fondamentale che l'utente sia aggiunto al gruppo `sambashare`.

![user](./asset/nas/user.png)

### Shared Folders

Creare la cartella (o le cartelle) da condividere:

![sharedfolders](./asset/nas/folder.png)

Impostare i permessi di lettura/scrittura corretti:

![permessi](./asset/nas/permessi.png)

![permessi](./asset/nas/permessi2.png)

![acl](./asset/nas/acl.png)

### Condivisione

In questo modo viene condivisa la cartella precedentemente creata

![smb](./asset/nas/smb.png)

![smb](./asset/nas/smb2.png)

### Testare la Condivisione

Da Dolphin o qualsiasi File Manager:

![connection](./asset/nas/connection.png)

Accedere con l'utente amministratore o con l'utente standard, a seconda:

![connection](./asset/nas/connection1.png)

## Collegamenti

- [https://www.fargionconsulting.com/installazione-di-openmediavault-su-raspberry-pi/](https://www.fargionconsulting.com/installazione-di-openmediavault-su-raspberry-pi/)
- [https://github.com/OpenMediaVault-Plugin-Developers/installScript](https://github.com/OpenMediaVault-Plugin-Developers/installScript)
- [https://www.maffucci.it/2022/02/20/installare-raspberry-pi-os-senza-monitor-e-tastiera-abilitazione-server-ssh/]( https://www.maffucci.it/2022/02/20/installare-raspberry-pi-os-senza-monitor-e-tastiera-abilitazione-server-ssh/)
- [https://www.ilsoftware.it/articoli.asp?tag=OpenMediaVault-cos-e-e-come-costruire-un-NAS-da-soli_19009](https://www.ilsoftware.it/articoli.asp?tag=OpenMediaVault-cos-e-e-come-costruire-un-NAS-da-soli_19009)

# Nas con samba

## Impostare IP statico

Collegarsi al raspberry via ssh, quindi modificare il seguente file, aggiungendo le righe sottostanti:

```bash
nano /etc/dhcpcd.conf

interface wlan0
static ip_address=192.168.1.107/24
static routers=192.168.1.1
static domain_name_servers=192.168.1.1 5.2.75.75
```

## Identificare e partizionare i dischi

```bash
fdisk -l

Disk /dev/sda: 12.73 TiB, 14000519643136 bytes, 27344764928 sectors
Disk model: Generic         
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: gpt
Disk identifier: 4AE9456D-B7BA-4011-87ED-A73741BAA769

Device     Start         End     Sectors  Size Type
/dev/sda1   2048 27344764894 27344762847 12.7T Linux filesystem


Disk /dev/sdb: 12.73 TiB, 14000519643136 bytes, 27344764928 sectors
Disk model: Generic         
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: gpt
Disk identifier: 1499002A-74AD-446F-994F-F07DF88B7E16
```
È possibile identificare i dischi anche con altri comandi:

```bash
lsblk -f

NAME        FSTYPE FSVER LABEL  UUID                                 FSAVAIL FSUSE% MOUNTPOINT
sda                                                                                 
sdb                                                                                 
mmcblk0                                                                             
|-mmcblk0p1 vfat   FAT32 bootfs 37CA-39EC                             224.3M    12% /boot
`-mmcblk0p2 ext4   1.0   rootfs a4af13c6-d165-4cbd-a9f6-c961fef8255d   54.4G     3% /

blkid

/dev/mmcblk0p1: LABEL_FATBOOT="bootfs" LABEL="bootfs" UUID="37CA-39EC" BLOCK_SIZE="512" TYPE="vfat" PARTUUID="be140fba-01"
/dev/mmcblk0p2: LABEL="rootfs" UUID="a4af13c6-d165-4cbd-a9f6-c961fef8255d" BLOCK_SIZE="4096" TYPE="ext4" PARTUUID="be140fba-02"
/dev/sda: PTUUID="4ae9456d-b7ba-4011-87ed-a73741baa769" PTTYPE="gpt"
/dev/sdb: PTUUID="1499002a-74ad-446f-994f-f07df88b7e16" PTTYPE="gpt"
```
### Partizionare i dischi

```bash
fdisk /dev/sda

Welcome to fdisk (util-linux 2.36.1).

Command (m for help): m

Help:

 GPT
  M   enter protective/hybrid MBR

 Generic
  d   delete a partition
  F   list free unpartitioned space
  l   list known partition types
  n   add a new partition
  p   print the partition table
  t   change a partition type
  v   verify the partition table
  i   print information about a partition

 Misc
  m   print this menu
  x   extra functionality (experts only)

 Script
  I   load disk layout from sfdisk script file
  O   dump disk layout to sfdisk script file

 Save & Exit
  w   write table to disk and exit
  q   quit without saving changes

 Create a new label
  g   create a new empty GPT partition table
  G   create a new empty SGI (IRIX) partition table
  o   create a new empty DOS partition table
  s   create a new empty Sun partition table


Command (m for help): g
Created a new GPT disklabel (GUID: 4D12D92A-E17B-114E-8805-762CC6E5A690).

Command (m for help): n
Partition number (1-128, default 1): 1
First sector (2048-27344764894, default 2048): 
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-27344764894, default 27344764894): 

Created a new partition 1 of type 'Linux filesystem' and of size 12.7 TiB.

Command (m for help): w
The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks.

```
Dopo aver partizionato anche `/dev/sdb`:

```bash
lsblk -f

NAME        FSTYPE FSVER LABEL  UUID                                 FSAVAIL FSUSE% MOUNTPOINT
sda                                                                                 
`-sda1                                                                              
sdb                                                                                 
`-sdb1                                                                              
mmcblk0                                                                             
|-mmcblk0p1 vfat   FAT32 bootfs 37CA-39EC                             224.3M    12% /boot
`-mmcblk0p2 ext4   1.0   rootfs a4af13c6-d165-4cbd-a9f6-c961fef8255d   54.4G     3% /
```

## Formattare il disco

Assicurarsi che entrambi i dispositivi siano smontati:

```bash
umount /dev/sda
umount: /dev/sda: not mounted.

umount /dev/sdb
umount: /dev/sdb: not mounted.
```

Quindi formattarli in `ext4`:

```bash
mkfs.ext4 -L "NAS1" /dev/sda1 -v 

mke2fs 1.46.2 (28-Feb-2021)
fs_types for mke2fs.conf resolution: 'ext4', 'big'
Filesystem label=NAS1
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
427261952 inodes, 3418095355 blocks
170904767 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=4294967296
104312 block groups
32768 blocks per group, 32768 fragments per group
4096 inodes per group
Filesystem UUID: 95db731d-5687-4681-bdac-b0d57dea0260
Superblock backups stored on blocks: 
       32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208, 
       4096000, 7962624, 11239424, 20480000, 23887872, 71663616, 78675968, 
       102400000, 214990848, 512000000, 550731776, 644972544, 1934917632, 
       2560000000

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (262144 blocks): done
Writing superblocks and filesystem accounting information: done
```
### Verificare la formattazione

```bash
file -sL /dev/sda1
/dev/sda1: Linux rev 1.0 ext4 filesystem data, UUID=95db731d-5687-4681-bdac-b0d57dea0260, volume name "NAS1" (extents) (64bit) (large files) (huge files)

file -sL /dev/sdb1
/dev/sdb1: Linux rev 1.0 ext4 filesystem data, UUID=c2ccecd7-5ce2-4441-a134-f8d293831aea, volume name "NAS2" (extents) (64bit) (large files) (huge files)
```

## fstab

Creare i punti di mount nella cartella `/srv/`:

```bash
root@iron:/# cd /srv/
root@iron:/srv# mkdir private/
root@iron:/srv# mkdir public/
```
Procedere quindi a modificare il file fstab:

```bash
root@iron:/media# cat /etc/fstab 
proc            /proc           proc    defaults          0       0
PARTUUID=be140fba-01  /boot           vfat    defaults          0       2
PARTUUID=be140fba-02  /               ext4    defaults,noatime  0       1

# I due dischi

UUID=95db731d-5687-4681-bdac-b0d57dea0260 /srv/private/ ext4 defaults 0 2
UUID=c2ccecd7-5ce2-4441-a134-f8d293831aea /srv/public/ ext4 defaults 0 2
```
Se, provando a montare i dischi non vengono restituiti errori, la procedura è avvenuta correttamente e i dischi verranno montati automaticamente all'avvio:

```bash
root@iron:/media# mount /srv/private/
root@iron:/media# mount /srv/public/

root@iron:/media# lsblk -f

NAME        FSTYPE FSVER LABEL  UUID                                 FSAVAIL FSUSE% MOUNTPOINT
sda
`-sda1      ext4   1.0   NAS1   95db731d-5687-4681-bdac-b0d57dea0260     12T     0% /srv/private
sdb
`-sdb1      ext4   1.0   NAS2   c2ccecd7-5ce2-4441-a134-f8d293831aea     12T     0% /srv/public
mmcblk0
|-mmcblk0p1 vfat   FAT32 bootfs 37CA-39EC                             224.3M    12% /boot
`-mmcblk0p2 ext4   1.0   rootfs a4af13c6-d165-4cbd-a9f6-c961fef8255d   54.4G     3% /

root@iron:/media# mount -t ext4

/dev/mmcblk0p2 on / type ext4 (rw,noatime)
/dev/sda1 on /srv/private type ext4 (rw,relatime)
/dev/sdb1 on /srv/public type ext4 (rw,relatime)
```

## Configurazione di samba

Installare i seguenti paccchetti:

```bash
root@iron:/media# apt install samba samba-common-bin cifs-utils

Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
cifs-utils is already the newest version (2:6.11-3.1+deb11u1).
The following additional packages will be installed:
 attr ibverbs-providers libavahi-client3 libboost-iostreams1.74.0 libboost-thread1.74.0 libcephfs2 libcups2
 libgfapi0 libgfrpc0 libgfxdr0 libglusterfs0 libibverbs1 libldb2 librados2 librdmacm1 libtdb1 liburing1
 python3-cffi-backend python3-cryptography python3-dnspython python3-gpg python3-ldb python3-markdown
 python3-pygments python3-requests-toolbelt python3-samba python3-talloc python3-tdb python3-yaml samba-common
 samba-dsdb-modules samba-libs samba-vfs-modules tdb-tools
Suggested packages:
 cups-common python-cryptography-doc python3-cryptography-vectors python3-sniffio python3-trio python-markdown-doc
 python-pygments-doc ttf-bitstream-vera bind9 bind9utils ctdb ldb-tools ntp | chrony smbldap-tools ufw winbind
 heimdal-clients
The following NEW packages will be installed:
 attr ibverbs-providers libavahi-client3 libboost-iostreams1.74.0 libboost-thread1.74.0 libcephfs2 libcups2
 libgfapi0 libgfrpc0 libgfxdr0 libglusterfs0 libibverbs1 libldb2 librados2 librdmacm1 libtdb1 liburing1
 python3-cffi-backend python3-cryptography python3-dnspython python3-gpg python3-ldb python3-markdown
 python3-pygments python3-requests-toolbelt python3-samba python3-talloc python3-tdb python3-yaml samba
 samba-common samba-common-bin samba-dsdb-modules samba-libs samba-vfs-modules tdb-tools
0 upgraded, 36 newly installed, 0 to remove and 0 not upgraded.
Need to get 29.6 MB of archives.
After this operation, 113 MB of additional disk space will be used.
Do you want to continue? [Y/n] 
```
### Global section

In `/etc/samba/smb.conf` aggiungere:

```bash
[global]
  #allow hosts = 192.168.1.0/24  
  workgroup = WORKGROUP       
  security = USER               
  server string = FILE SERVER    
  create mask = 0755               
  browsable = no
```

### Share section

```bash
[private]
   comment = Cartella condivisa       
   path = /srv/private         
   browsable = no
   read only = no
   writable = yes       
   guest ok = no
   create mask = 0775
   valid users = @samba
```

Verificare che la configurazione sia corretta col comando:

```bash
testparm
```

Quindi riavviare i servizi:

```bash
systemctl restart smbd.service
systemctl restart nmbd.service

# oppure

systemctl restart smbd nmbd
```

### Aggiunta utente

```bash
adduser davide --home=/home/public --shell=/bin/false --disabled-password

Adding user `davide' ...
Adding new group `davide' (1001) ...
Adding new user `davide' (1001) with group `davide' ...
Creating home directory `/home/public' ...
Copying files from `/etc/skel' ...
Changing the user information for davide
Enter the new value, or press ENTER for the default
       Full Name []: 
       Room Number []: 
       Work Phone []: 
       Home Phone []: 
       Other []: 
Is the information correct? [Y/n] 
```
#### Assegnare una password all'utente

```bash
smbpasswd -a davide

New SMB password:
Retype new SMB password:
Added user davide.
```

### Aggiungere l'utente al gruppo samba

```bash
usermod -aG samba utente 
```

#### Assegnare la cartella condivisa all'utente

```bash
chown davide:samba /srv/private/
chmod -R 770 /srv/private/
ls -lh1 /srv/private/
total 20K
drwxrwx--- 2 davide davide 4.0K Mar 26 17:46 files
```

## Montare automaticamente la cartella condivisa

Installare sul client i seguenti pacchetti:

```bash
sudo apt-get install cifs-utils smb4k
```

Creare nella propria home un file `.smbcredentials` coi seguenti dati:

```bash
username=shareuser
password=sharepassword
domain=workgroupname
```

Quindi `chmod 0600 ~/.smbcredentials`.

Modificare il file `/etc/fstab` nel seguente modo:

```bash
cat /etc/fstab 
#
# <file system>             <mount point>  <type>  <options>  <dump>  <pass>

//192.168.1.107/private/ /media/davide/samba cifs credentials=/home/davide/.smbcredentials,uid=davide,gid=davide 0 0
```

Infine dare il comando `sudo mount -a`. La cartella dovrebbe essere correttamente montata.

```bash
sudo mount -a

mount: (hint) your fstab has been modified, but systemd still uses
      the old version; use 'systemctl daemon-reload' to reload.
```

### Connettersi tramite file manager

Inserire nel path:

```bash
smb://server/share_folder/


smb://192.168.1.107/private/
```

## Collegamenti

- [https://linuxhint.com/raspberry_pi_static_ip_setup/](https://linuxhint.com/raspberry_pi_static_ip_setup/)
- [https://askubuntu.com/questions/157128/proper-fstab-entry-to-mount-a-samba-share-on-boot](https://askubuntu.com/questions/157128/proper-fstab-entry-to-mount-a-samba-share-on-boot)
- [https://noviello.it/come-installare-e-configurare-samba-server-su-ubuntu-18-04-lts/](https://noviello.it/come-installare-e-configurare-samba-server-su-ubuntu-18-04-lts/)
- [https://www.howtogeek.com/50787/add-a-user-to-a-group-or-second-group-on-linux/](https://www.howtogeek.com/50787/add-a-user-to-a-group-or-second-group-on-linux/)

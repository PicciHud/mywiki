## Welcome to my Wiki


![Piccihud](https://gitea.it/avatars/7eeeb156c6a3d3e5e91b9d848d59165d?size=580)

> “Non è da uomo saggio dire «Vivrò». Vivere domani è già troppo tardi: vivi oggi"

> "Potersi sentire soddisfatto della propria vita significa vivere due volte"

    Marziale

```
\          SORRY            /
    \                         /
     \    This page does     /
      ]   not exist yet.    [    ,'|
      ]                     [   /  |
      ]___               ___[ ,'   |
      ]  ]\             /[  [ |:   |
      ]  ] \           / [  [ |:   |
      ]  ]  ]         [  [  [ |:   |
      ]  ]  ]__     __[  [  [ |:   |
      ]  ]  ] ]\ _ /[ [  [  [ |:   |
      ]  ]  ] ] (#) [ [  [  [ :===='
      ]  ]  ]_].nHn.[_[  [  [
      ]  ]  ]  HHHHH. [  [  [
      ]  ] /   `HH("N  \ [  [
      ]__]/     HHH  "  \[__[
      ]         NNN         [
      ]         N/"         [
      ]         N H         [
     /          N            \
    /           q,            \
   /                           \
```
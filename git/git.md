# Git comandi principali

## Cos'è git?

Git è un software di *controllo versione distribuito* utilizzabile da interfaccia a riga di comando, creato da Linus Torvalds nel 2005. Git lavora con i repository, ognuno dei quali ha 4 stati di lavoro:

1. il primo stato è la *directory* corrente del progetto;
2. il secondo è l'*index* che fa da spazio di transito per i files (`git add *`);
3. il terzo è l'*head* che punta all'ultimo commit fatto (`git commit -m "messaggio"`);
4. l'ultimo è il *repository online* (`git push server`).

I repository online e locali possono essere divisi in ramificazioni o *branch*. I branch permettono di creare delle versioni alternative al codice presente nel ramo `master`, ovvero quello principale e stabile. Solitamente queste ramificazioni secondarie sono chiamate `dev`. Ciò permette la creazione di features o aggiornamenti in fase alpha che non vanno ad intaccare minimamente il codice del progetto. Finito la scrittura della ramificazione il branch dev verrà unito con il master.

Git permette anche di gestire i `tag`, le versioni del software in uso.

## Configurazioni di base di git

Configurare il git con le proprie credenziali:

```txt
  git config --global user.name 'username'
  git config --global user.email 'email@email.it'
```

### Creare un progetto

Ci son due modi per creare un progetto:

1. Inizializzare un progetto non esistente in locale:

```txt
  git init
```

2. Inizializzare un progetto esistente su un server git (in remoto):

```txt
  git clone serverURL.git 
```

Esempio:

```txt
  git clone https://gitea.it/PicciHud/Appunti.wiki.git
```

Git clone permette di copiare in locale il file .git del server e anche l'intero repository.

## Configurazione del server remoto

Con questo comando si visualizza la lista di server remoti salvati con il relativo url:

```txt
  git remote -v
```

Di solito il server principale si chiama `origin`, ma è possibile specificare qualsiasi nome a gradimento.

### Aggiungere un un server remoto

```txt
  git remote add identificatoreServerRemoto UrlServerRemoto
```

Esempio:

```txt
  git remote add origin https://gitea.it/PicciHud/Appunti.wiki.git
```

## Lavorare nel progetto locale

### Aggiungere i file dalla directory del progetto all'index

```txt
  git add nome_file
```

Si può utilizzare l'asterisco `*` per aggiungere tutti i file. Se si vuole escludere un file dalla selezione totale, basta creare un file denominato `.gitignore` e inserire all'interno i file o le directory che non devono essere aggiunte all'index.

```txt
  git add *
```

### Aggiungere i file dell'index all'head

```txt
  git commit -m "Messaggio del commit"
```

Annullamento dell'ultimo commit:

```txt
  git commit --amend
```

Il file ritorna allo stato precedente dell’ultimo commit:

```txt
  git checkout -- nomeFile
```

## Lavorare con il server remoto

### Aggiornare il repository locale al commit più recente

```txt
  git pull
```

### Upload dei commit

```txt
  git push identificatoreServerRemoto nomeBranch
```

Esempio:

```txt
  git push -u origin master
```

### Rinominare un file remoto

```txt
  git remote rename identificatoreServerRemoto nomeFileVecchio nomeFileNuovo
```

### Eliminare un file remoto

```txt
  git remote rm nomeFile
```

## Stato del progetto

Per vedere le modifiche del progetto:

```txt
  git status
```

Per vedere i cambiamenti dei singoli files:

```txt
  git diff
```

Vedere tutti i commit:

```txt
  git log
```

## Gestire i tag

Per visualizzare tutte le versioni esiste il comando:

```txt
  git tag
```

Per visualizzare tutte le versioni con un determinato numero:

```txt
  git tag -l 1*
```

### Creazione di un tag

```txt
  git tag -a versioneSoftware -m "nota sul tag"
```

Esempio:

```txt
  git tag -a 1.2.3rc1 -m "aggiornato la navbar"
```

### Vedere tutte le modifiche di un tag

```txt
  git show 1.2.3rc1
```

### Condividere i tag

```txt
  git push identificatoreServerRemoto tagDaPubblicare
```

Esempio:

```txt
  git push origin 1.2.3rc1
```

Condividere tutti i tag:

```txt
  git push identificatoreServerRemoto --tag
```

Esempio:

```txt
  git push origin --tag
```

## Gestire i Branch

Lista dei Rami:

```txt
  git branch
```

### Creazione di un branch

```txt
  git branch nomeBranch
```

Esempio:

```txt
  git branch feature
```

Creare il ramo e passare a quel branch:

```txt
  git checkout -b nomeBranch
```

Esempio:

```txt
  git checkout -b feature
```

### Cambiare ramo

```txt
  git checkout nomeBranch
```
  
Esempio:

```txt
  git checkout feature
```

Per ritornare al branch originale digitare:

```txt
  git checkout master
```

### Eliminare un ramo

```txt
  git branch -d nomeBranch
```

Esempio:

```txt
  git branch -d feature
```

### Unire il branch al master (fare un commit nel branch)

```txt
  git checkout master
  git merge feature
```

## Git Parameters

```txt

*** Inizializza l'area di lavoro ***

 clone      Clona un repository in una cartella
 init       Crea un git repository o ne inizializza uno

*** Lavorare nel progetto corrente ***

  add        Aggiungere i file nel INDEX
  rm         Rimuove i file dalla directory corrente e nel INDEX

*** Mostra la cronologia e lo stato ***

  log        Mostra i log
  status     stato del contenuto di un progetto
  show       Show various types of objects

*** Grow, mark and tweak your common history ***

  branch     Visualizza, crea e elimina ramo (branches)
  checkout   Cambia ramo (branches) o ripristina la struttura dell'area di lavoro
  commit     Registra le modifiche del repository
  diff       Confronta i commit
  merge      Unisce una o più cronologie di sviluppo
  tag        Crea, visualizza la lista, elimina o verifica il tag della versione del progetto

```

## Collegamenti

- http://rogerdudler.github.io/git-guide/index.it.html

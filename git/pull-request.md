# Git: la Pull Request

La *Pull Request* è una richiesta, fatta all’autore originale di un sofware o di un documento, di includere le modifiche al suo progetto.

Dopo aver trovato un progetto, si deve cliccare sul tasto `Fork`, in alto a destra:

![Fork](./asset/pull-request//fork1.png 'Fork di un progetto')

## Forkare un progetto

Il tasto `fork` creerà una copia del progetto sul nostro profilo personale di GitHub, Gitea o simili. Di questa copia non potrà essere toccato il ramo `master`, perchè appartiene agli sviluppatori originari del progetto, tuttavia è possibile creare un ramo secondario, ove apportare le dovute modifiche.

Nell'esempio, viene forkato il seguente progetto:

[https://github.com/wsdfhjxc/virtual-desktop-bar](https://github.com/wsdfhjxc/virtual-desktop-bar)

## Clonare il progetto

Ora basterà copiare il collegamento `ssh` al nostro progetto e clonarlo in locale, tramite i seguenti comandi:

```bash
git clone git@github.com:PicciHud/virtual-desktop-bar.git
```

![Clone](./asset/pull-request//clone.png 'Clonare un progetto')

Per la creazione delle chiavi `SSH`, seguire la precedente guida: [SSH](https://gitea.it/PicciHud/Appunti/wiki/SSH 'Creazione chiavi SSH')

## Creare un nuovo branch

Per apportare delle modifiche, occorre creare un ramo secondario, di sviluppo:

```bash
git branch dev
```

dove `dev` indica il nome del ramo.

È quindi possibile spostarsi nel nuovo ramo tramite il comando:

```bash
git checkout dev
```

## Apportare modifiche

Adesso è possibile apportare qualsiasi modifica al progetto, creando nuovi files, cartelle, o qualsiasi cosa si ritenga opportuna.

Nell'esempio, abbiamo modificato il file `install-applet.sh`, correggendo il `$PATH` del file `build-applet.sh`, oltre ad aver creato l'installer per Debian

![Modifiche](./asset/pull-request//mod.png 'Modifiche')

In pratica è stato risolto questo bug:

![Issue](./asset/pull-request//bug.png 'Issue')

Una volta terminato, aggiungere le modifiche alla *staging area*:

```bash
git add .
```

quindi il comando 

```bash
git commit -a -m ‘nome modifiche’
```

salva la nuova versione del documento.

Infine il comando 

```bash
git push –set-upstream -origin dev
```

invia il nuovo ramo di sviluppo al nostro repository su GitHub o simili.

Si tratta di un comando abbastanza complicato da ricordare a memoria. Git però viene in aiuto, basterà quindi semplicemente ricordare il comando classico `git push` per ricevere da git un messaggio di errore che contiene l’indicazione sulla sintassi esatta del comando da usare.

## Pull request

Su GitHub cliccare sul pulsante *Compare & Pull request* per avviare agli sviluppatori originari del progetto una richiesta di integrazione del nostro ramo con le nostre integrazioni e modifiche. Descriviamo i cambiamenti che abbiamo fatto e inviamo la Pull Request.

![Pull Request](./asset/pull-request//pullrequest1.png 'Pull Request')

Per approfondire i comandi di Git, si veda la seguente guida: [Git](https://gitea.it/PicciHud/Appunti/wiki/Git 'Comandi Git')

## Collegamenti

- [https://marcolombardo.com/blog/open/source/2019/03/13/iniziare-con-git-e-github-la-pull-request.html](https://marcolombardo.com/blog/open/source/2019/03/13/iniziare-con-git-e-github-la-pull-request.html)

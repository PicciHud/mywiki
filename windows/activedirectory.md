# Active Directory

- Installare la versione *Windows Server (Desktop Experience)*
- Impostare Ip statico direttamente da `Server Manager` e abilitare la connessione tramite `Desktop remoto`. Quindi rinominare la VM

![./asset/ip_remote_desktop.png](./asset/ip_remote_desktop.png)

In questo esempio, abbiamo due *domain controller*:

```bash
### DC1
192.168.1.126
255.255.255.0
192.168.1.1

### DC2
192.168.1.127
255.255.255.0
192.168.1.1
```
- Impostare la Time zone corretta, sempre da Server Manager

## Regola di firewall
- Consentire i ping in entrata
    
![./asset/firewall_ping.png](./asset/firewall_ping.png)

Seguire la seguente guida: [Come accettare le richieste di ping](https://www.pcprofessionale.it/news/software-news/windows/accettare-le-richieste-di-ping/)

Ora il server risponde correttamente:

```bash
> ping 192.168.1.126

PING 192.168.1.126 (192.168.1.126) 56(84) bytes of data.
64 bytes from 192.168.1.126: icmp_seq=1 ttl=128 time=0.282 ms
64 bytes from 192.168.1.126: icmp_seq=2 ttl=128 time=0.227 ms
64 bytes from 192.168.1.126: icmp_seq=3 ttl=128 time=0.225 ms
64 bytes from 192.168.1.126: icmp_seq=4 ttl=128 time=0.227 ms
64 bytes from 192.168.1.126: icmp_seq=5 ttl=128 time=0.243 ms
64 bytes from 192.168.1.126: icmp_seq=6 ttl=128 time=0.216 ms
^C
--- 192.168.1.126 ping statistics ---
6 packets transmitted, 6 received, 0% packet loss, time 5109ms
rtt min/avg/max/mdev = 0.216/0.236/0.282/0.021 ms
```
## Installazione di AD

È possibile aggiungere *ruoli* e/o funzionalità.

- `Rules`: indica il ruolo del server, cosa diventerà (DHCP Server, ecc);
- `Feature`: arricchisce il server di una caratteristica generica.

##### L'unico requisito per AD è avere un servizio DNS attivo sul server. 
Installando AD, viene contemporaneamente installato anche ruolo di DNS server.

![./asset/role_features.png](./asset/role_features.png)

### Installazione di AD

- Installare il ruolo *Active Directory Domain Services*
- Aggiungere la features per il supporto a Samba 1.0
- Il server sarà riavviato automaticamente

### Promuovere il server a Domain Controller

- Creare una nuova foresta

![./asset/domain.png](./asset/domain.png)

- Impostare la password per il ripristino del database di AD
- Verificare il nome NetBIOS

![./asset/netbiosname.png](./asset/netbiosname.png)

### Configurare il ruolo DNS

- Impostare i server di inoltro condizionali, che servono per la risoluzione di tutte quelle richieste dns che non possono essere risolte dal server dns locale. Nel nostro esempio, impostiamo i server dns di google, ovvero 8.8.8.8 e 8.8.4.4
- Impostare una *Reverse Lookup Zones*: risoluzione di indirizzi ip in nomi di dominio

![./asset/reverse_lookup_zones.png](./asset/reverse_lookup_zones.png)

Nella sezione `Zone di ricerca diretta` si fare doppio clic sul nome di dominio quindi su quello del server spuntando la casella Aggiorna record puntatore (PTR) associato.

Aprendo una finestra del prompt dei comandi in Windows Server e digitando `ipconfig /all` si noterà che il server DNS predefinito è diventato 127.0.0.1 ossia la macchina stessa.

Digitando inoltre `nslookup nome-dominio` si può verificare che il server DNS locale risolve sia il nome a dominio locale (nell’esempio domain.local) che domini di terze parti attestati su server remoti.

### Ruolo DHCP

- Installare il ruolo di server DHCP

![./asset/dhcp_server.png](./asset/dhcp_server.png)

Terminare la configurazione

![./asset/dhcpconfig.png](./asset/dhcpconfig.png)

#### Configurazione scope

Creare quindi il pool di indirizzi che il server andrà a distribuire

- Da *Tools/DHCP*

![./asset/newscope.png](./asset/newscope.png)

- Inserire il range di indirizzi da distribuire

![./asset/rangescope.png](./asset/rangescope.png)

- Successivamente, è possibile anche escludere degli indirizzi. È possibile impostare **un server di failover**. Nel nostro esempio, sarà impostato DC2

Altre informazioni al seguente indirizzo: [Installare e configurare il server DHCP](https://learn.microsoft.com/it-it/windows-server/networking/technologies/dhcp/quickstart-install-configure-dhcp-server?tabs=gui)

### Verificare che il DHCP sia funzionante

Dalle impostazione del server DHCP, autorizzare il server e riavviare il servizio. Si dovrebbe vedere una spunta verde

### Backup DB AD

Per attivare l'instanza di backup, dare i seguenti comandi:

```bash
ntdsutil
activate instance ntds
ifm
create full c:\users\admninistrator\desktop\ifm
```

### OUA, users and computers

Ora è possibile creare unità organizzative, utenti, computer e altri oggetti

![./asset/oua.png](./asset/oua.png)

## Installare secondo DC

- Dopo aver installato AD, promuovere il server a secondo domain controller, aggiungendolo a un dominio esistente. Nel caso non trovasse il dominio, impostare un secondo server dns nelle impostazioni della scheda di rete
- Proseguire col join e con l'installazione

![./asset/joindc.png](./asset/joindc.png)

- Verificare che vengano sincronizzate le impostazioni del server DNS e che DC1 sia il server master per la risoluzione inversa dei nomi
- Verificare che gli oggetti vengano sincronizzati
- Installare il ruolo di DHCP server sul DC2, quindi sincronizzarlo dal DC1, impostando DC2 come failover di DC1

![./asset/dhcpfailover.png](./asset/dhcpfailover.png)

>> Nel caso si creasse un secondo dominio nella stessa rete, il nome NetBios del dominio deve essere differente. Ad esempio, se creassimo un dominio domain.lab, il nome NetBIOS non potrà essere DOMAIN, come per domain.local creato precedentemente, ma dovrà per forza essere qualcosa del tipo DOMAIN0

![./asset/newdomain.png](./asset/newdomain.png)

## Ruoli FSMO

Microsoft suddivise le responsabilità di un DC in 5 ruoli distinti che insieme realizzano un sistema AD completo.

I ruoli FSMO (Flexible Single Master Operations) sono cinque ruoli chiave in un ambiente Active Directory (AD) che gestiscono operazioni specifiche:

- `Schema Master`: Gestisce le modifiche allo schema di Active Directory. Solo un Domain Controller (DC) alla volta può detenere questo ruolo.

- `Domain Naming Master`: Gestisce l'aggiunta o la rimozione di domini nell'foresta. Anche questo ruolo è unico e può essere detenuto solo da un DC alla volta.

- `RID Master (Relative ID Master)`: Assegna i RID (Relative Identifiers) univoci ai nuovi oggetti creati all'interno di un dominio. Ogni dominio deve avere un RID Master dedicato.

- `PDC Emulator (Primary Domain Controller Emulator)`: Fornisce la compatibilità con i vecchi sistemi operativi NT4. Inoltre, è responsabile della sincronizzazione dell'orologio in tutta la foresta.

- `Infrastructure Master:` Mantiene la coerenza tra i riferimenti di oggetti nei domini di una foresta. Se un dominio contiene riferimenti a oggetti in altri domini, l'Infrastructure Master si assicura che essi siano aggiornati.

Per maggiori info: [Ruoli FSMO](https://learn.microsoft.com/it-it/troubleshoot/windows-server/identity/view-transfer-fsmo-roles)

Dare il seguente comando per verificare i ruoli:

```bash
netdom.exe query fsmo
```

![./asset/fsmo.png](./asset/fsmo.png)

## Collegamenti

- [https://www.varonis.com/it/blog/fsmo-roles](https://www.varonis.com/it/blog/fsmo-roles)
- [https://learn.microsoft.com/it-it/windows-server/identity/ad-ds/deploy/install-active-directory-domain-services--level-100-](https://learn.microsoft.com/it-it/windows-server/identity/ad-ds/deploy/install-active-directory-domain-services--level-100-)
- [https://www.bartolomeoalberico.it/installazione-active-directory/](https://www.bartolomeoalberico.it/installazione-active-directory/)
- [https://www.ilsoftware.it/non-aspettare-il-2024-per-risparmiare-passa-a-sorgenia-oggi-stesso/](https://www.ilsoftware.it/non-aspettare-il-2024-per-risparmiare-passa-a-sorgenia-oggi-stesso/)
- [https://carlistefano.wordpress.com/tag/server-dns/](https://carlistefano.wordpress.com/tag/server-dns/)
- [https://www.windowserver.it/2007/01/active-directory-introduzione-ai-ruoli-di-fsmo/](https://www.windowserver.it/2007/01/active-directory-introduzione-ai-ruoli-di-fsmo/)
- [https://www.windowserver.it/2019/01/windows-server-active-directory-best-practice/](https://www.windowserver.it/2019/01/windows-server-active-directory-best-practice/)

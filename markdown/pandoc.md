# Pandoc

Pandoc è un tool open source da linea di comando scritto che serve a convertire da un linguaggio di markup ad un altro in modo semplice e veloce. Supporta svariati formati tra cui:

- latex
- markdown
- html

Inoltre è in grado di convertire i file da un determinato linguaggio di markup ad un formato pdf, epub, ecc. 

## Installazione

```bash
sudo apt install pandoc texlive-latex-extra texlive-latex-base texlive-xetex texlive-publishers texlive-fonts-extra
```
## Sintassi

Per convertire un file da un formato all’altro basterà eseguire:

```bash
pandoc -s nomefile.formato -o nomefile.nuovoformato
```
Per esempio, per convertire un file markdown note.md in un file pdf note.pdf:

```bash
pandoc -s note.md -o note.pdf
```

### Altre opzioni

- `-f FORMAT`: specifica il formato di input del documento
- `-t FORMAT`: specifica il formato di output del documento
- `-o FILE`: specifica il nome del file di output
- `-s` o `--standalone`: crea un documento autonomo con intestazione e piè di pagina
- `--template=FILE`: specifica il file di modello yaml da utilizzare
- `-V KEY=VALUE`, ovvero `--variable=KEY:VALUE`: imposta una variabile di modello
- `-H FILE`, `--include-in-header=FILE`: include il contenuto del file specificato nell'intestazione del documento
- `--toc`: per aggiungere un indice, utile per pdf e epub
- `--number-sections`: per numerare sezioni, capitoli e sottocapitoli, utile per la conversione nel formato pdf e epub
- `--pdf-engine=xelatex`: per specificare un motore di conversione alternativo a quello predefinito. Questa scelta rispetto al motore pre-impostato, pdflatex, deriva dal fatto che con xelatex è possibile creare pdf anche da documenti che hanno caratteri speciali in formato UnicodeQuesta scelta rispetto al motore pre-impostato, pdflatex, deriva dal fatto che con xelatex è possibile creare pdf anche da documenti che hanno caratteri speciali in formato Unicode
- `-V lang=it`: con l'opzione `-V` vengono introdotte le variabili. Serve per comunicare a LaTeX di utilizzare la sintassi italiana
- `-V colorlink`: serve per rendere i link colorati
- `-V table-use-row-colors=true`: le righe delle tabelle sono colorate alternativamente

Ecco un esempio di comando:

```bash
pandoc -s file.md -f markdown -t pdf --pdf-engine=xelatex -V lang=it -V colorlinks=true -V linkcolor=teal -V urlcolor=teal -V toccolor=gray --template eisvogel --listings -V table-use-row-colors=true -o file.pdf
```

## Tema personalizzato

Qui un ottimo tema personalizzato: [https://github.com/Wandmalfarbe/pandoc-latex-template](https://github.com/Wandmalfarbe/pandoc-latex-template)

### Installazione del tema personalizzato

```bash
cd 
wget -c https://github.com/Wandmalfarbe/pandoc-latex-template/blob/master/eisvogel.tex

mkdir -p ~/.local/share/pandoc/templates/
mv eisvogel.tex ~/.local/share/pandoc/templates/
```
Maggiori informazioni sulle variabili di Pandoc: [https://pandoc.org/MANUAL.html#variables-for-latex](https://pandoc.org/MANUAL.html#variables-for-latex)

### Personalizzare header

Al principio del file .md:

```md
---
title: "The Document Title"
author: [Example Author, Another Author]
date: "2017-02-20"
keywords: [Markdown, Example]
...
---
```

## Alias

Per semplificare, è consigliato crearsi un alias nel file `~/.bashrc`:

```bash
alias pd="pandoc -f markdown -t pdf --pdf-engine=xelatex -V lang=it -V colorlinks=true -V linkcolor=teal -V urlcolor=teal -V toccolor=gray --template eisvogel --listings -V table-use-row-colors=true"
```
Alla conversione successiva da file markdown a pdf sarà sufficiente digitare:

```bash
pd -s file.md -o file.pdf
```


## Collegamenti

- [https://linuxhub.it/articles/howto-convertire-i-formati-con-pandoc/](https://linuxhub.it/articles/howto-convertire-i-formati-con-pandoc/)
- [https://www.avvocati-e-mac.it/blog/2019/4/9/compilare-un-atto-telematico-in-markdown-con-pandoc](https://www.avvocati-e-mac.it/blog/2019/4/9/compilare-un-atto-telematico-in-markdown-con-pandoc)
- [https://www.howtogeek.com/678022/how-to-use-pandoc-to-convert-files-on-the-linux-command-line/](https://www.howtogeek.com/678022/how-to-use-pandoc-to-convert-files-on-the-linux-command-line/)
- [https://jdhao.github.io/2019/05/30/markdown2pdf_pandoc/](https://jdhao.github.io/2019/05/30/markdown2pdf_pandoc/)
- [https://learnbyexample.github.io/customizing-pandoc/](https://learnbyexample.github.io/customizing-pandoc/)
- [https://it.wikibooks.org/wiki/Scrivere_in_modo_sostenibile_usando_il_testo_semplice_con_Pandoc_e_Markdown](https://it.wikibooks.org/wiki/Scrivere_in_modo_sostenibile_usando_il_testo_semplice_con_Pandoc_e_Markdown)

# Tutorial Markdown

## Headings

Gli headings si ottengono con il carattere `#`. Più cancelletti vengono inseriti, più piccolo sara l'heading.

```md
# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
###### Heading 6
```

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

---

## Liste

Per le *liste ordinate*, si utilizzano i numeri, mentre per le *liste non ordinate* i simboli: `-` , `*` o `+`

```md
1. Primo elemento
2. Secondo elemento
3. Terzo elemento

- Elemento
- Elemento
- Elemento
```

## Elementi inline

Il *corsivo* si ottiene inserendo del testo tra un `*` o un `_`

```md
_testo in corsivo con underscore_

*testo in corsivo con asterisco*
```

Il **grassetto** si ottiene inserendo del testo tra due `**` o due `__`

```md
__testo in grassetto con due underscore__

**testo in grassetto con due asterischi**
```

## Le citazioni (Blockquote)

I blockquote o citazioni si ottengono con la parentesi angolare `>`

> Questa è una citazione. Rimango sulla stessa linea.

## Immagini

Le immagini vengono mostrate con la sintassi seguente:

```md
![Descrizione](https://gitea.it/avatars/7eeeb156c6a3d3e5e91b9d848d59165d?size=580 'piccihud')
```

![Descrizione](https://gitea.it/avatars/7eeeb156c6a3d3e5e91b9d848d59165d?size=580 'piccihud')

## Link

Ecco la sintassi per i link:

```md
[Collegamento a Wikipedia.it](https://wikipedia.it 'Vai su wikipedia')
```

[Collegamento a Wikipedia.it](https://wikipedia.it 'Vai su wikipedia')

## Tables

Nelle tabelle, le colonne sono separate dalla pipe `|`, mentre tra la riga d'intestazione e quelle sottostanti si inseriscono una fila `---` separati da pipe per ogni colonna:

```md
| Taglia | Colore |
| ------ | ------ |
| S      | Giallo |
| M      | Giallo |
| L      | Giallo |
| XL     | Blu    |
```

| Taglia | Colore |
| ------ | ------ |
| S      | Giallo |
| M      | Giallo |
| L      | Giallo |
| XL     | Blu    |

```md
| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |
```

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |

## Note a piè di pagina

```md
Here is a simple footnote[^1].

A footnote can also have multiple lines[^2].  

You can also use words, to fit your writing style more closely[^note].

[^1]: My reference.
[^2]: Every new line should be prefixed with 2 spaces.  
  This allows you to have a footnote with multiple lines.
[^note]:
    Named footnotes will still render with numbers instead of the text but allow easier identification and linking.  
    This footnote also has been made with a different syntax using 4 spaces for new line
```

## Code snippets

Il codice può essere inserito inline o come blocco.

Per del codice inline basta inserire una o più parole tra backtick:

```md
La root dell'applicazione è nel file `App.js` nella cartella `src`
```

La root dell'applicazione è nel file `App.js` nella cartella `src`

Il codice in blocco può essere racchiuso tra tre backtick. È anche possibile specificare il linguaggio del blocco per una sintassi correttamente evidenziata:

```html
<div class="scrollmenu">
  <a href="#home">Home</a>
  <a href="#news">News</a>
  <a href="#contact">Contact</a>
  <a href="#about">About</a>
  ...
</div>
```

## Editor MarkDown

Uno degli editor migliori per il Markdown è `VSCodium`. Questi i plugin necessari:

- [https://open-vsx.org/vscode/item?itemName=yzhang.markdown-all-in-one](https://open-vsx.org/vscode/item?itemName=yzhang.markdown-all-in-one)

- [https://open-vsx.org/vscode/item?itemName=yzane.markdown-pdf](https://open-vsx.org/vscode/item?itemName=yzane.markdown-pdf)

- [https://open-vsx.org/vscode/item?itemName=DavidAnson.vscode-markdownlint](https://open-vsx.org/vscode/item?itemName=DavidAnson.vscode-markdownlint)

- [https://open-vsx.org/vscode/item?itemName=marp-team.marp-vscode](https://open-vsx.org/vscode/item?itemName=marp-team.marp-vscode)

![MarkDown Plugin VSCodium](./asset/markdown/plugin.png 'Markdown')

Oppure si può utilizzare `Kate`, installando il seguente plugin per avere la preview del file:

[https://apps.kde.org/markdownpart/](https://apps.kde.org/markdownpart/)

```bash
sudo apt update ; sudo apt install -y markdownpart
```

![Preview](./asset/markdown/preview.png 'Preview')

## Numeri e notazione matematiche

Vedere qui: [https://upyesp.org/posts/makrdown-vscode-math-notation/](https://upyesp.org/posts/makrdown-vscode-math-notation/) e anche [https://csrgxtu.github.io/2015/03/20/Writing-Mathematic-Fomulars-in-Markdown/](https://csrgxtu.github.io/2015/03/20/Writing-Mathematic-Fomulars-in-Markdown/)

## Diagrammi

È possibile utilizzare Mermaid: [https://mermaid.js.org/intro/](https://mermaid.js.org/intro/)

## Presentazioni

Esiste il framework MarpIT: [https://marpit.marp.app/directives](https://marpit.marp.app/directives)

## Licenze

[https://gist.github.com/lukas-h/2a5d00690736b4c3a7ba](https://gist.github.com/lukas-h/2a5d00690736b4c3a7ba)

## Collegamenti

- [https://it.m.wikibooks.org/wiki/Scrivere_in_modo_sostenibile_usando_il_testo_semplice_con_Pandoc_e_Markdown](https://it.m.wikibooks.org/wiki/Scrivere_in_modo_sostenibile_usando_il_testo_semplice_con_Pandoc_e_Markdown)
- [https://csrgxtu.github.io/2015/03/20/Writing-Mathematic-Fomulars-in-Markdown/](https://csrgxtu.github.io/2015/03/20/Writing-Mathematic-Fomulars-in-Markdown/)
- [https://gist.github.com/lukas-h/2a5d00690736b4c3a7ba](https://gist.github.com/lukas-h/2a5d00690736b4c3a7ba)
- [https://apps.kde.org/it/markdownpart/](https://apps.kde.org/it/markdownpart/)
- [https://www.markdownguide.org/basic-syntax/#images-1](https://www.markdownguide.org/basic-syntax/#images-1)
- [https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
- [https://linuxtldr.com/markdown-cheatsheet/](https://linuxtldr.com/markdown-cheatsheet/)

# Sincronizzare con NextCloud

<!-- Nascondere i bordi della tabella -->

<style>
table, td, th, tr {
   border: 0px !important;
   margin: auto;
}

/*Ridurre dimensione delle immagini*/

img {
    height: auto;
    width: 80%;
}
</style>

`Nextcloud` è una suite libera di software client-server per file hosting, cloud storage, memorizzazione e sincronizzazione online.

Su Nextcloud i file sono archiviati in directory convenzionali, accessibili tramite `WebDAV`. *Web-based Distributed Authoring and Versioning*, abbreviato in WebDAV (in italiano traducibile come "*creazione e versionamento distribuiti di documenti sul web*"), è un protocollo di rete che consente di rendere disponibili e trasferire file tramite Internet. Si tratta di un’estensione dell’*Hypertext Transfer Protocol* (HTTP), responsabile unicamente della visualizzazione delle pagine web.

Per farla breve, NextCloud funziona proprio come un normale servizio di file hosting completamente installabile in autonomia su un proprio server. Ma Nextcloud va ben oltre il semplice hosting di file, essendo infatti una suite completa di tutto. Permette di sincronizzare i contatti, il calendario, le attività, di scrivere documenti, presentazioni e figli di calcolo online, condividere file di qualsiasi genere, di gestire gruppi di utenti coi relativi permessi...

## Accedere a NextCloud su Android

Nextcloud è un servizio nato per il *self-hosting*, però ci sono anche tantissime realtà che offrono il servizio già configurato e pronto per l'uso gratuitamente o a pagamento, a seconda dello spazio a disposizione. Eccone alcune molto valide:

- [webo.hosting](https://cloud.webo.hosting/)
- [Hetzner](https://www.hetzner.com/storage/storage-share)
- [Murena](https://murena.com/ecloud-subscriptions/): gratuitamente offrono 1GB di spazio

Dopo essersi iscritti a uno di questi servizi, scaricare le seguenti app per Android:

- [NextCloud](https://f-droid.org/en/packages/com.nextcloud.client/)
- [DavX5](https://f-droid.org/en/packages/at.bitfire.davdroid/): permette di sincronizzare contatti, eventi del calendario e attività

Avviare quindi l'applicazione di NextCloud ed accedere con l'indirizzo del proprio server. Nell'esempio sottostante è utilizzato [https://murena.io/](https://murena.io/):

|||
|:---:|:---:|
|![nextcloud2.png](./asset/nextcloud//nextcloud2.png)|![nextcloud3.png](./asset/nextcloud//nextcloud3.png)|

È stata abilitata l'autenticazione a due fattori, quindi inserire il codice OTP ed accedere al proprio account:

|||
|:---:|:---:|
|![nextcloud4.png](./asset/nextcloud//nextcloud4.png)|![nextcloud5.png](./asset/nextcloud//nextcloud5.png)|![nextcloud6.png](./asset/nextcloud//nextcloud6.png)|

Una volta fatto ciò, si avrà accesso alla propria cartella radice su NextCloud.

## Sincronizzare i contatti

Una volta installata l'app [DavX5](https://f-droid.org/en/packages/at.bitfire.davdroid/), entrare nelle impostazioni dell'app di NextCloud e cliccare la voce `Sincronizza calendario e contatti`. Verrà quindi chiesto di accedere al proprio account sul server e di inserire, eventualmente, il codice OTP:

|||
|:---:|:---:|
|![dav1.png](./asset/nextcloud//dav1.png)|![dav2.png](./asset/nextcloud//dav2.png)|
|![dav3.png](./asset/nextcloud//dav3.png)|![dav4.png](./asset/nextcloud//dav4.png)|

Dopo aver effettuato l'accesso, abilitare la sincronizzazione dei contatti e del calendario in `DavX5` e nell'app del calendario:

|||
|:---:|:---:|
|![dav5.png](./asset/nextcloud//dav5.png)|![dav6.png](./asset/nextcloud//dav6.png)|
|![dav7.png](./asset/nextcloud//dav7.png)|![dav8.png](./asset/nextcloud//dav8.png)|

Ottima applicazione per il calendario è [Simple Calendar Pro](https://f-droid.org/packages/com.simplemobiletools.calendar.pro/)

## Collegamenti

- [https://it.wikipedia.org/wiki/Nextcloud](https://it.wikipedia.org/wiki/Nextcloud)
- [https://it.wikipedia.org/wiki/Web-based_Distributed_Authoring_and_Versioning](https://it.wikipedia.org/wiki/Web-based_Distributed_Authoring_and_Versioning)
- [https://www.lealternative.net/2020/06/04/nextcloud-lalternativa-a-google-drive/](https://www.lealternative.net/2020/06/04/nextcloud-lalternativa-a-google-drive/)
- [https://www.ionos.it/digitalguide/server/know-how/webdav/](https://www.ionos.it/digitalguide/server/know-how/webdav/)
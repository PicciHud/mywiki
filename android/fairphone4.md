# Liberare il Fairphone 4

Per installare una custom rom su un qualsiasi cellulare, il primo passo è quello di **sbloccarne il bootloader**, il programma che, nella fase di avvio (boot) del computer, carica il kernel del sistema operativo dalla memoria secondaria (ad esempio un hard disk) alla memoria primaria (generalmente la RAM), permettendone l'esecuzione da parte del processore e il conseguente avvio del sistema.

Prima di procedere, è necessario installare **adb** (Android Debug Bridge), uno strumento compreso all'interno del software SDK (Software Develompent Kit) e usato per mettere in comunicazione un dispositivo Android ed un computer, tramite un'interfaccia a riga di comando.

Su Debian basta lanciare il seguente comando:

```bash
sudo apt install android-tools-adb android-tools-fastboot
```

## Sbloccare il bootloader - parte 1

Si ricorda che, sbloccando il bootloader, saranno **cancellati tutti** i dati presenti nel cellulare.

Ogni telefono ha una procedura differente per lo sblocco. Quella del Fairphone è molto semplice e descritta ai seguenti link:

[https://support.fairphone.com/hc/en-us/articles/4405858258961-FP4-Manage-the-bootloader](https://support.fairphone.com/hc/en-us/articles/4405858258961-FP4-Manage-the-bootloader)

[https://www.fairphone.com/en/bootloader-unlocking-code-for-fairphone-3/](https://www.fairphone.com/en/bootloader-unlocking-code-for-fairphone-3/)

### Ottenere il codice di sblocco

Scollegare il cellulare dal computer, se precedentemente connesso.

1. Trovare il codice IMEI del telefono:
   * **Impostazioni > Informazioni sul telefono > IMEI (slot SIM 1)**

!['IMEI'](./asset/fairphone/bootloader1.png)

2. Trovare il Serial Number (S/N) del dispositivo:
   * **Impostazioni > Informazioni sul telefono > Modello > Numero di Serie**

!['S/N'](./asset/fairphone/bootloader2.png)

3. A [questo indirizzo](https://www.fairphone.com/en/bootloader-unlocking-code-for-fairphone-3/) inserire il codice IMEI e il S/N per ottenere il codice di sblocco

!['Ottenere il codice di sblocco'](./asset/fairphone/unlock.png)

### Inserire il codice per lo sblocco

1. Abilitare la `modalità sviluppatore`
   * **Impostazioni > Informazioni sul telefono > Numero build**
   * Premere tante volte (di solito 7) finché non apparirà il messaggio `attivato le impostazioni di sviluppo`

!['Abilitare la modalità sviluppatore'](./asset/fairphone/bootloader3.png)

2. Abilitare lo sblocco del bootloader (OEM) e la modalità `debug USB`
   * **Impostazioni > Sistema > Opzioni sviluppatore** e abilitare la voce `Sblocco EOM`, quindi inserire il codice precedentemente ottenuto, come da immagine:

|Inserire il codice||
|:---:|:---:|
|!['Sblocco OEM'](./asset/fairphone/bootloader4.png)|!['Sblocco OEM'](./asset/fairphone/bootloader5.png)|

1. Ora abilitare la voce `Debug USB`, affinché sia possibile impartire dei comandi tramite `adb`.

## Sbloccare il bootloader - parte 2

Collegare, tramite cavo USB, il cellulare al computer, nel quale sia stato precedentemente installato `adb` e aprire una shell dei comandi. Quindi digitare:

```bash
adb -l devices             # Lista dei dispositivi connessi
List of devices attached
15b99cd8        device
```

Sul cellulare apparirà una notifica, quindi consentire il debug, cosicché il telefono sia rilevato e sia possibile lanciare i comandi:

!['Consentire il debug USB'](./asset/fairphone/allowdebug.webp)

Proseguire con i comandi seguenti per effettuare lo sblocco del bootloader dalla modalità `fastboot` ([https://en.wikipedia.org/wiki/Fastboot](https://en.wikipedia.org/wiki/Fastboot)):

```bash
abd reboot bootloader               # Riavvia il telefono in modalità fastboot
fastboot flashing unlock
# Seguire le istruzioni a video e confermare. 
# IMPORTANTE: verranno CANCELLATI TUTTI I DATI
```

Riavviare il cellulare ed entrare nuovamente in `fastboot mode`:

```bash
abd reboot bootloader
fastboot flashing unlock_critical
# Seguire le istruzioni a video e confermare lo sblocco
```

Riavviare il dispositivo.

Ora il bootloader è sbloccato ed è possibile installare una **custom rom**.

## Installazione DivestOS

Il Fairphone 4 ha a disposizione diverse custom rom ad oggi, fine 2022, come:

- CalyxOS ([https://calyxos.org/install/devices/FP4/linux/](https://calyxos.org/install/devices/FP4/linux/))
- DivestOS ([https://divestos.org/index.php?page=bootloader](https://divestos.org/index.php?page=bootloader))
- /e ([https://doc.e.foundation/devices/FP4/install](https://doc.e.foundation/devices/FP4/install))
- iodè ([https://gitlab.com/iode/ota](https://gitlab.com/iode/ota))

Nella guida procederemo con l'installazione di ` DivestOS 20.0 beta`, una custom rom basata su Android 13. Se si preferisce una versione più stabile, scaricare la versione `19.1`, basata su Android 12L.

**DivestOS** è un sistema operativo (SO) libero basato su Android. È un fork di LineageOS che mira ad aumentare la sicurezza e la privacy, il più possibile rimuovendo le componenti proprietarie di Android e includendo solo software libero.

Le build di DivestOS sono firmate, quindi il bootloader può essere ribloccato su molti dispositivi.

DivestOS include poche applicazioni predefinite, come F-droid, uno store per applicazioni FOSS, Mull, un fork di Firefox Mobile, Graphene Camera e poco altro. **NON INCLUDE LE GAPPS** e i servizi di Google (se per il dispositivo dovesse essere disponibile la recovery `TWRP` e venisse installata al posto della recovery stock di DivestOS, sarebbe possibile installarne il relativo pacchetto. Non risulta, al momento, disponibile per il Fairphone 4).

La sottostante procedura d'installazione, è identica, al netto di minime variazioni, per tutti i dispositivi supportati da DivestOS

Alla seguente pagina [https://www.divestos.org/index.php?page=devices&base=LineageOS&golden=false](https://www.divestos.org/index.php?page=devices&base=LineageOS&golden=false) scaricare la rom per il Fairphone 4, la `recovery` e il file `AVB Key`

!['Download'](./asset/fairphone/divest1.png)

e metterli nella stessa directory, per comodità.

```bash
mkdir ~/Scaricati/FP4
cd FP4/
wget -c https://www.divestos.org/mirror.php?base=LineageOS&f=fp4/divested-20.0-20221020-dos-FP4.zip
wget -c https://www.divestos.org/mirror.php?base=LineageOS&f=fp4/divested-20.0-20221020-dos-FP4-recovery.img
wget -c https://www.divestos.org/builds/LineageOS/fp4/avb_pkmd-fp4.bin

# Questa dovrebbe essere la situazione

ll

totale 1G
-rwxrwxrwx 1 piccihud piccihud 1,1K  4 nov 21.42 avb_pkmd-fp4.bin*
-rwxrwxrwx 1 piccihud piccihud  96M  4 nov 15.05 divested-20.0-20221020-dos-FP4-recovery.img*
-rwxrwxrwx 1 piccihud piccihud 875M  4 nov 15.04 divested-20.0-20221020-dos-FP4.zip*
```

Qui le istruzioni generiche per l'installazione:

[https://www.divestos.org/index.php?page=bootloader](https://www.divestos.org/index.php?page=bootloader)

Si ricorda che, per proseguire, è **obbligatorio** aver sbloccato il bootloader e riabilitato la modalità `Debug USB`.

1. Collegare il cellulare al PC e riavviare in modalità fastboot tramite il comando `adb reboot bootloader`
2. Se è disponibile a link precedente solamente il file `recovery.img `, come nel caso del Fairphone 4, e non il file `fastboot.zip`:
   
   * flashare ('installare') la suddetta custom recovery: `fastboot flash recovery divested-version-date-dos-device-recovery.img`. Nel nostro caso: `fastboot flash recovery divested-20.0-20221020-dos-FP4-recovery.img` o versione successiva

3. Riavviare il telefono in `recovery mode` col comando `fastboot reboot recovery`, oppure usando i pulsanti 'Volume su' e 'Volume giù' per navigare tra le modalità e il tasto 'Power' per selezionare la modalità desiderata.
4. **Selezionare `Apply update` > `Apply from ADB`**, quindi lanciare da shell il comando `adb sideload divested-version-date-dos-device.zip` per flashare la custom rom. Nel esempio: `adb sideload divested-20.0-20221020-dos-FP4.zip` o versione successiva
5. Tornare ala schermata principale della Recovery e cliccare `factory reset`
6. Infine riavviare in modalità fastboot (tramite i pulsanti 'Volume su' e 'Volume giù' o il comandon `adb reboot bootloader`), quindi:

```bash
fastboot erase avb_custom_key
fastboot flash avb_custom_key avb_pkmd-fp4.bin
```

7. Riavviare in DivestOS

|Alcune immagini di DivestOS||
|:---:|:---:|
!['DivestOS'](./asset/fairphone/divest2.png)|!['DivestOS'](./asset/fairphone/divest3.png)|
!['DivestOS'](./asset/fairphone/divest4.png)|!['DivestOS'](./asset/fairphone/divest5.png)|

## Ribloccare il bootloader - opzionale

Per questioni di sicurezza, dopo aver provveduto ad installare una custom rom, sarebbe meglio ribloccare il bootloader.

Collegare il cellulare al Pc, abilitare il `Debug USB`, quindi:

```bash
adb reboot bootloader
fastboot flashing lock_critcal
# Riavviare, quindi ritornare nella modalità fastboot
adb reboot bootloader
fastboot flashing lock
```

Il tutto è descritto anche al seguente link:

[https://support.fairphone.com/hc/en-us/articles/4405858258961-FP4-Manage-the-bootloader](https://support.fairphone.com/hc/en-us/articles/4405858258961-FP4-Manage-the-bootloader)

## Collegamenti

* [https://it.wikipedia.org/wiki/Android_Debug_Bridge](https://it.wikipedia.org/wiki/Android_Debug_Bridge)
* [https://it.wikipedia.org/wiki/Boot_loader](https://it.wikipedia.org/wiki/Boot_loader)

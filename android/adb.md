# adb

Per installare adb:

```bash
sudo apt install android-tools-adb android-tools-fastboot
```

## adb rules

Nel caso le regole non venissero create in automatico:

```bash
[15:33 dom set 24]dado@pc (15270):~ 
 > lsusb 
Bus 006 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 005 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 003 Device 002: ID 3532:a0a4 Ranked Nova 60% Keyboard
Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 001 Device 003: ID 8087:0029 Intel Corp. AX200 Bluetooth
Bus 001 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver
Bus 001 Device 007: ID 18d1:4ee2 Google Inc. Nexus/Pixel Device (MTP + debug)
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
```

Identificare il dispositivo Android connesso via USB, nell'esempio:

```bash
Bus 001 Device 007: ID 18d1:4ee2 Google Inc. Nexus/Pixel Device (MTP + debug)
```
Prendere nota dell'*idVendor*, 18d1, e dell'*idProduct*, 4ee2.

Quindi:

```bash
sudo vim /etc/udev/rules.d/51-android.rules

SUBSYSTEM=="usb", ATTR{idVendor}=="2e04", ATTR{idProduct}=="c008", MODE="0666", GROUP="plugdev"

sudo udevadm control --reload-rules
```
Un esempio del file:

```bash
[15:33 dom set 24]dado@pc (15270):~ 
 > cat /etc/udev/rules.d/51-android.rules 
SUBSYSTEM=="usb", ATTR{idVendor}=="18d1", ATTR{idProduct}=="4ee2", MODE="0666", GROUP="plugdev"
```

## Collegamenti

- [https://lynxbee.com/solved-no-permissions-user-in-plugdev-group-are-your-udev-rules-wrong/](https://lynxbee.com/solved-no-permissions-user-in-plugdev-group-are-your-udev-rules-wrong/)
- [https://askubuntu.com/questions/1169509/user-in-plugdev-group-are-your-udev-rules-wrong-or-error-insufficient-permissi](https://askubuntu.com/questions/1169509/user-in-plugdev-group-are-your-udev-rules-wrong-or-error-insufficient-permissi)


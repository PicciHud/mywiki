# Syncthing

```bash
sudo apt-get install syncthing
```
## Avviare la sincronizzazione con Systemd

```bash
sudo vim /etc/systemd/system/syncthing@.service
```

Quindi inserire il seguente testo:

```
[Unit]
Description=Syncthing - Open Source Continuous File Synchronization for %I
Documentation=man:syncthing(1)
After=network.target

[Service]
User=%i
ExecStart=/usr/bin/syncthing -no-browser -gui-address="0.0.0.0:8384" -no-restart -logflags=0
Restart=on-failure
SuccessExitStatus=3 4
RestartForceExitStatus=3 4

# Hardening
ProtectSystem=full
PrivateTmp=true
SystemCallArchitectures=native
MemoryDenyWriteExecute=true
NoNewPrivileges=true

[Install]
WantedBy=multi-user.target
```
Abilitare il servizio:

```
sudo systemctl daemon-reload
sudo systemctl start syncthing@$USER
sudo systemctl enable syncthing@$USER
```
## Configurare la sincronizzazione

Collegarsi, tramite browser all'indirizzo:

```
https://127.0.0.1:8384
```
e impostare un utente e una password di accesso.

![syncting](asset/nextcloud/sync1.png)

Quindi dal dispositivo principale, creare la cartella che dovrà essere condivisa con quelli secondari:

![syncting](asset/nextcloud/sync2.png)

È possibile scegliere se monitorare i cambiamenti e quindi avere una sorta di versioning sui file. Il dispositivo principale può inviare solamente, ricevere o 
entrambi i dispositivi possono ricevere/inviare. Da scegliere a seconda dei casi

![syncting](asset/nextcloud/sync3.png)

Dal dispositivo principale, aggiungere il dispositivo remoto tramite il suo ID e avviare la richiesta di accoppiamento.

Dare quindi un nome identificativo al device:

![syncting](asset/nextcloud/sync6.png)

Verificare che entrambi i dispositivi siano connessi e che i file siano effettivamente sincronizzati:

![syncting](asset/nextcloud/sync11.png)

![syncting](asset/nextcloud/sync12.png)

![syncting](asset/nextcloud/sync13.png)

Per una maggior sicurezza, la cartella può essere sincronizzata anche con NextCloud.

## Collegamenti

- [https://www.lffl.org/2020/12/guida-syncthing-p2p-sharing.html](https://www.lffl.org/2020/12/guida-syncthing-p2p-sharing.html)

- [https://gist.github.com/zentralwerkstatt/b94ab44938fbdc8b957c674bf9261954](https://gist.github.com/zentralwerkstatt/b94ab44938fbdc8b957c674bf9261954)


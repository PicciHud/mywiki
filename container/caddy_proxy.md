# Caddy

Un reverse proxy è un tipo particolare di server proxy che si posiziona davanti ad uno o più server back-end, gestendo tutte le richieste in ingresso dai client, inoltrandole poi opportunamente al server corretto.

Caddy è un software open-source multipiattaforma progettato proprio come reverse proxy, con una configurazione semplice basata su file JSON o YAML anziché complesse direttive.

## Installazione

Su Debian e derivate:

```bash
sudo apt install caddy
```

Quindi avviare il servizio:

```bash
sudo systemctl enable --now caddy.service
```

## Configurazione

La configurazione è semplicissima. Per prima cosa, creare il seguente file:

```bash
mkdir /etc/caddy

touch /etc/caddy/Caddyfile
```

In questo file andranno inserite le direttive di reindirizzamento.

Ogni volta che si modifica il file, ricaricare il servizio coi comandi:

```bash
sudo systemctl reload caddy.service
```

Per verificare eventuali errori o log del servizio:

```bash
sudo journalctl -u caddy.service -f
```

### Esempio di configurazione

```bash
 > cat /etc/caddy/Caddyfile
# The Caddyfile is an easy way to configure your Caddy web server.

:80 {
	# Set this path to your site's directory.
	root * /usr/share/caddy

	# Enable the static file server.
	file_server
}

# Refer to the Caddy docs for more information:
# https://caddyserver.com/docs/caddyfile

## Snikket
## In questo esempio, Caddy si occupa di girare tutte le connessioni diretta a chat.domain.com sulla porta 80/443 del server alla porta 5080/5443 del container Docker

http://chat.domain.com,
http://groups.chat.domain.com,
http://share.chat.domain.com {
	reverse_proxy localhost:5080
}

chat.domain.com,
groups.chat.domain.com,
share.chat.domain.com {
	reverse_proxy https://localhost:5443 {
		transport http {
			tls_insecure_skip_verify
		}
	}
}

## Collabora

https://code.domain.com {
  encode gzip
    reverse_proxy localhost:9980 {
    transport http {
      tls_insecure_skip_verify
    }
  }
}

## Kuma

https://status.domain.com {
    reverse_proxy localhost:3001
}

## ntfy

https://ntfy.domain.com {
    reverse_proxy localhost:3002
}

## Vaultwarden

https://vault.domain.com {
    reverse_proxy localhost:3003
}

## Molly-socket

https://molly.domain.com {
    reverse_proxy / localhost:8020
}
```

## Gestione certificati TLS

Funzione utilissima, Caddy genera e rinnova in automatico tramite Let’s Encrypt i certificati TLS per i servizi che gestisce

## Collegamenti

- [https://bobadin.icu/posts/guida-caddy/](https://bobadin.icu/posts/guida-caddy/)
- [https://caddyserver.com/docs/install](https://caddyserver.com/docs/install)
- [https://snikket.org/service/help/advanced/reverse_proxy/](https://snikket.org/service/help/advanced/reverse_proxy/)

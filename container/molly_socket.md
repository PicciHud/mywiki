# MollySocket

MollySocket consente di ricevere notifiche tramite UnifiedPush. Ogni volta che riceve un evento crittografato, invia una notifica al cellulare tramite UnifiedPush. UnifiedPush è un insieme di specifiche e strumenti che consente all'utente di scegliere come recapitare le notifiche push, il tutto in modo gratuito e open source. Il distributore piu' semplice da configurare e' [ntfy](https://f-droid.org/en/packages/io.heckel.ntfy/).

## Setup

- Per utilizzare UnifiedPush è necessaria la versione corretta di Molly: [https://github.com/mollyim/mollyim-android-unifiedpush](https://github.com/mollyim/mollyim-android-unifiedpush)
- Un'applicazione distributore (la più semplice è ntfy)

## Installazione

#### Creare l'utente dedicato

```bash
sudo useradd mollysocket -m -d /opt/mollysocket
sudo -su mollysocket
cd
```

in particolare:

- `-m` opzione che specifica di creare una home directory per l'utente (di default nella directory /home);
- `-d /opt/mollysocket` opzione che specifica la posizione della home directory dell'utente, nel caso specifico "/opt/mollysocket".

#### Scaricare il binario

- Scaricare il binario da [https://github.com/mollyim/mollysocket/releases/](https://github.com/mollyim/mollysocket/releases/). Quindi creare un link simbolico per il binario appena scaricato:

```bash
ln -s /opt/mollysocket/mollysocket-amd64-1.2.0 /opt/mollysocket/ms
```

Viene creato un collegamento simbolico (link soft) al file mollysocket-amd64-1.2.0 presente all’interno della directory /opt/mollysocket, dandogli il nuovo nome ms.

Questo il risultato:

```bash
 > ll /opt/mollysocket/
Permissions Size User        Date Modified Name
.rwxr--r--  4,9M mollysocket  4 dic  2023   mollysocket-amd64-1.2.0
.rw-r-----   12k mollysocket 23 mar 17:12   mollysocket.db
lrwxrwxrwx    40 mollysocket 23 mar 16:17   ms -> /opt/mollysocket/mollysocket-amd64-1.2.0
.rw-r--r--   141 mollysocket 23 mar 17:03   prod.toml
```

#### File di configurazione

- Scaricare il file di conf col comando:

```bash
wget -O /opt/mollysocket/prod.toml https://github.com/mollyim/mollysocket/raw/main/config-sample.toml
```

Un esempio:

```bash
 > cat /opt/mollysocket/prod.toml
 
db = '/opt/mollysocket/mollysocket.db'
allowed_endpoints = ['https://ntfy.domain.com']
allowed_uuids = ['*']
webserver = true
port = 8020
```

#### Servizio systemd

- Scaricare la unit:

```bash
wget -O /etc/systemd/system/mollysocket.service https://raw.githubusercontent.com/mollyim/mollysocket/main/mollysocket.service
```
- Quindi avviare il servizio:

```bash
systemctl enable --now mollysocket
```

#### Proxy Server

- Inserire le seguenti righe nel Caddyfile

```bash
 > cat /etc/caddy/Caddyfile

https://molly.domain.com {
    reverse_proxy / localhost:8020
}
```

#### Configurazione ntfy

- Nelle impostazioni impostare il server ntfy di default
- In Molly, nelle Impostazioni>Notifiche>UnifiedPush, inserire l'URL del server (es. https://molly.domain.com) e scegliere ntfy come metodo di notifica

## Collegamenti

- [https://github.com/mollyim/mollysocket](https://github.com/mollyim/mollysocket)
- [https://github.com/mollyim/mollysocket/blob/main/INSTALL.md](https://github.com/mollyim/mollysocket/blob/main/INSTALL.md)

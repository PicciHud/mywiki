# Docker compose

Tutti i file docker-compose.yml si trovano in `/etc/container-name/`.

Per eseguire tutti i servizi sottostanti, basta posizionarsi nella cartella che contiene il file docker-compose e lanciare i seguenti comandi:

```bash
cd /etc/container-name/

docker compose pull
docker compose up -d
```

Verificare anche che la configurazione del proxy sia corretta.

## Collabora Online Editor

```bash
 > cat /etc/collabora/docker-compose.yml

version: '3'

services:
  code:
    container_name: code
    image: collabora/code:latest
    env_file: .env
    restart: unless-stopped
    environment:
    #  - password=${COLLABORA_PASSWORD}
    #  - username=${COLLABORA_USERNAME}
      - domain=${COLLABORA_DOMAIN}
      - dictionaries=en it
      - extra_params=--o:ssl.enable=true --o:ssl.termination=false # Set SSL options
    ports:
      - 9980:9980
    volumes:
      - /etc/localtime:/etc/localtime
      - /etc/timezone:/etc/timezone
    cap_add:
      - MKNOD
    tty: true
```

`env` file con le variabili d'ambiente:

```bash
 > cat /etc/collabora/.env
#COLLABORA_USERNAME=admin
#COLLABORA_PASSWORD=veecheit0Phophiesh1fahPah0Wue3
COLLABORA_DOMAIN=collabora.domain.com
```

## Uptime Kuma

Si tratta di un tool per il monitoraggio della propria infrastruttura.

Altre info: [https://github.com/louislam/uptime-kuma](https://github.com/louislam/uptime-kuma)

```bash
 > cat /etc/kuma/docker-compose.yml
# Simple docker-compose.yml
# You can change your port or volume location

version: '3.3'

services:
  uptime-kuma:
    image: louislam/uptime-kuma:1
    container_name: uptime-kuma
    volumes:
      - ./uptime-kuma-data:/app/data
      - /var/run/docker.sock:/var/run/docker.sock ### Per il monitoraggio dei container: è un socket UNIX utilizzato da Docker per la comunicazione remota tra processi. Per default, il socket è accessibile solo localmente, ovvero solo ai processi che girano sullo stesso host
    ports:
      - 3001:3001  # <Host Port>:<Container Port>
    restart: always
```

## ntfy

```bash
 > cat /etc/ntfy/docker-compose.yml

version: "2.3"

services:
  ntfy:
    image: binwiederhier/ntfy
    container_name: ntfy
    command:
      - serve
    environment:
      - TZ=CET    # optional: set desired timezone
    # user: UID:GID # optional: replace with your own user/group or uid/gid
    volumes:
      - /var/cache/ntfy:/var/cache/ntfy
      - /etc/ntfy:/etc/ntfy
    ports:
      - 3002:80
    healthcheck: # optional: remember to adapt the host:port to your environment
        test: ["CMD-SHELL", "wget -q --tries=1 http://localhost:80/v1/health -O - | grep -Eo '\"healthy\"\\s*:\\s*true' || exit 1"]
        interval: 60s
        timeout: 10s
        retries: 3
        start_period: 40s
    restart: unless-stopped
```

## Vaultwarden

```bash
version: '3'

services:
  vaultwarden:
    image: vaultwarden/server:latest
    container_name: vaultwarden
    restart: unless-stopped
    ports:
      - 3003:80  # Needed for the ACME HTTP-01 challenge.
      - 3004:443
    environment:
      DOMAIN: "https://vault.domain.com"  # Your domain; vaultwarden needs to know it's https to work properly with attachments
      LOG_FILE: "/data/vaultwarden.log"
      LOG_LEVEL: "warn"
      EXTENDED_LOGGING: "true"
      SHOW_PASSWORD_HINT: "false"
      SIGNUPS_ALLOWED: "false"
      # SENDS_ALLOWED: "true"
      LOGIN_RATELIMIT_MAX_BURST: 10
      LOGIN_RATELIMIT_SECONDS: 60
      ADMIN_RATELIMIT_MAX_BURST: 10
      ADMIN_RATELIMIT_SECONDS: 60
      ADMIN_TOKEN: "YourReallyStrongAdminTokenHere"
      EMERGENCY_ACCESS_ALLOWED: "true"
      SIGNUPS_VERIFY: "false"
      SMTP_HOST: "mail.infomaniak.com"
      SMTP_FROM: "mail@ik.me"
      SMTP_SECURITY: "starttls"
      SMTP_PORT: 587
      SMTP_USERNAME: "mail@ik.me"
      SMTP_PASSWORD: "YourReallyStrongPassword"
    volumes:
      - ./vw-data:/data
```

Come da wiki ([https://github.com/dani-garcia/vaultwarden/wiki/Enabling-admin-page](https://github.com/dani-garcia/vaultwarden/wiki/Enabling-admin-page)), la pagina Admin, e' accessibile al seguente indirizzo: [https://vault.domain.com/admin]. A questo punto, bisognera' inserire il token.

Per la configurazione SMTP: [https://github.com/dani-garcia/vaultwarden/wiki/SMTP-configuration](https://github.com/dani-garcia/vaultwarden/wiki/SMTP-configuration)

I parametri per Infomaniak: [https://www.infomaniak.com/en/support/faq/2023/authenticated-mail-smtp-from-a-website](https://www.infomaniak.com/en/support/faq/2023/authenticated-mail-smtp-from-a-website)

Per le password, sarebbe meglio impostare un file `.env` con le varie variabili.

Per altre info:

- [https://github.com/dani-garcia/vaultwarden/wiki/Using-Docker-Compose](https://github.com/dani-garcia/vaultwarden/wiki/Using-Docker-Compose)
- [https://www.techaddressed.com/tutorials/vaultwarden-docker-compose/](https://www.techaddressed.com/tutorials/vaultwarden-docker-compose/)
- [https://www.howtoforge.com/how-to-install-vaultwarden-on-ubuntu-22-04/](https://www.howtoforge.com/how-to-install-vaultwarden-on-ubuntu-22-04/)

## Snikket

```bash
 > cat /etc/snikket/docker-compose.yml
version: "3.3"

services:
  snikket_proxy:
    container_name: snikket-proxy
    image: snikket/snikket-web-proxy:stable
    env_file: snikket.conf
    network_mode: host
    volumes:
      - snikket_data:/snikket
      - acme_challenges:/var/www/html/.well-known/acme-challenge
    restart: "unless-stopped"
  snikket_certs:
    container_name: snikket-certs
    image: snikket/snikket-cert-manager:stable
    env_file: snikket.conf
    volumes:
      - snikket_data:/snikket
      - acme_challenges:/var/www/.well-known/acme-challenge
    restart: "unless-stopped"
  snikket_portal:
    container_name: snikket-portal
    image: snikket/snikket-web-portal:stable
    network_mode: host
    env_file: snikket.conf
    restart: "unless-stopped"

  snikket_server:
    container_name: snikket
    image: snikket/snikket-server:stable
    network_mode: host
    volumes:
      - snikket_data:/snikket
    env_file: snikket.conf
    restart: "unless-stopped"

volumes:
  acme_challenges:
  snikket_data:
```

E il file di configurazione:

```bash
 > cat /etc/snikket/snikket.conf
# The primary domain of your Snikket instance
SNIKKET_DOMAIN=chat.domain.com
# An email address where the admin can be contacted
# (also used to register your Let's Encrypt account to obtain certificates)
SNIKKET_ADMIN_EMAIL=mail@mail.com
SNIKKET_TWEAK_HTTP_PORT=5080
SNIKKET_TWEAK_HTTPS_PORT=5443
```

## Collegamenti

- [https://caddy.community/t/caddy-reverse-proxy-nextcloud-collabora-vaultwarden-with-local-https/12052](https://caddy.community/t/caddy-reverse-proxy-nextcloud-collabora-vaultwarden-with-local-https/12052)
- [https://codeberg.org/frnmst/ftutorials/src/commit/d3707cbffa3640f5b80a8cc4a6aea69209d8391d/docs/content/server/includes/home/jobs/scripts/by-user/root/docker/nextcloud/docker-compose.yml#L149](https://codeberg.org/frnmst/ftutorials/src/commit/d3707cbffa3640f5b80a8cc4a6aea69209d8391d/docs/content/server/includes/home/jobs/scripts/by-user/root/docker/nextcloud/docker-compose.yml#L149)
- [https://docs.nextcloud.com/server/25/admin_manual/office/example-docker.html](https://docs.nextcloud.com/server/25/admin_manual/office/example-docker.html)
- [https://techoverflow.net/2021/08/19/how-to-run-collabora-office-for-nextcloud-using-docker-compose/](https://techoverflow.net/2021/08/19/how-to-run-collabora-office-for-nextcloud-using-docker-compose/)
- [https://caddy.community/t/example-collabora-code/8224](https://caddy.community/t/example-collabora-code/8224)
- [https://techoverflow.net/2021/08/19/how-to-run-collabora-office-for-nextcloud-using-docker-compose/](https://techoverflow.net/2021/08/19/how-to-run-collabora-office-for-nextcloud-using-docker-compose/)
- [https://github.com/louislam/uptime-kuma/wiki/Reverse-Proxy#caddy](https://github.com/louislam/uptime-kuma/wiki/Reverse-Proxy#caddy)
- [https://bobadin.icu/posts/guida-uptime-kuma/](https://bobadin.icu/posts/guida-uptime-kuma/)

# Database

## Installazione PostgreSQL

```bash
sudo apt install postgresql
service postgresql start
service postgresql status
```

### Console di PostgreSQL

```bash
\l lista database
\d lista tabelle
\du lista utenti
```

### Configurazione per accesso da remoto

Il server PostgreSQL è configurabile mediante tre file:

- postgresql.conf
- pg_hba.conf
- pg_ident.conf

che si trovano in `/etc/postgresql/x.y/main/`
Per applicare le modifiche va fatto il *restart* del server.

Per consentire a tutti gli indirizzi IP di connettersi al server PostgreSQL, si devono modificare i file di configurazione precedenti.

#### Configurare postgresql.conf

Aprire il file e apportare alcune modifiche per consentire la connessione remota.

```bash
sudo nano /etc/postgresql/xx/main/postgresql.conf 
```

Nel file cercare "listen_addresses" e aggiungere la seguente riga:

```bash
#listen_addresses = 'localhost'
listen_address = '*'
```

Ciò cambia l'indirizzo di ascolto da localhost a "*", che consentirà a qualsiasi indirizzo IP di connettersi al server del database. Oppure se si desidera specificare alcuni IP particolari, è possibile digitarli con spazi tra ciascun indirizzo IP.

#### Configurare pg_hba.conf

Per consentire ad utenti particolari di essere collegati al database, bisogna apportare modifiche al file *pg_hba.conf* . Questo file sarà disponibile nella stessa directory di cui sopra.

```bash
sudo nano /etc/postgresql/xx/main/pg_hba.conf 
```

Nel file aggiungere le seguenti righe:

```bash
# TYPE  DATABASE  USER  ADDRESS     METHOD
host    all       all   0.0.0.0/0   md5
host    all       all     :/0       md5
```

Salvare il file di configurazione. Ora, riavviare il database eseguendo il comando indicato di seguito:

```bash
service postgresql restart 
```

Infine, aprire la porta **5432** nel firewall col comando:

```bash
sudo ufw allow 5432 
```

Altre info: [https://noviello.it/come-configurare-postgresql-per-consentire-le-connessioni-remote/](https://noviello.it/come-configurare-postgresql-per-consentire-le-connessioni-remote/)

## User

### Create user

```bash
sudo su postgres
psql

CREATE USER <name> SUPERUSER CREATEDB;
```

Se l'utente esistesse già:

```bash
ALTER USER <name> WITH PASSWORD <password>;
```

Altre info: [https://phoenixnap.com/kb/postgres-create-user](https://phoenixnap.com/kb/postgres-create-user)

### Drop user

```bash
DROP USER <name>;
```

## Database

### Create Database

```bash
CREATE DATABASE <database_name>
WITH
   [OWNER =  role_name]
   [ALLOW_CONNECTIONS = true | false]
```

Altre info: [https://www.postgresql.org/docs/12/sql-createdatabase.html](https://www.postgresql.org/docs/12/sql-createdatabase.html)

### Backup manuale

```bash
pg_dump <database_name> > <file.sql>
```

### Restore

Da shell:

```bash
psql <database_name> < <file.sql>
```

Altre info: [https://www.tecmint.com/backup-and-restore-postgresql-database/](https://www.tecmint.com/backup-and-restore-postgresql-database/)

### Auto Backup PostgreSQL

```bash
$ mkdir -p ~/databases
$ crontab -e
0 0 * * *  pg_dump  -U postgres <db_name> > ~/databases/<db_name.sql>
```

L'automatismo partirà ogni giorno alle 24.00

## Estensione VSCodium

[https://github.com/cweijan/vscode-database-client](https://github.com/cweijan/vscode-database-client)

ID: cweijan.vscode-mysql-client

## Collegamenti

- [https://noviello.it/come-installare-postgresql-su-debian-10/](https://noviello.it/come-installare-postgresql-su-debian-10/)
